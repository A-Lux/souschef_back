<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/', function () {
        $data = \App\Review::all();
    return view('landing',['data'=>$data]);
}
);

//Route::get('/', 'MainController@show');

Route::get('/about', 'AboutController@show');

Route::get('/home', 'HomeController@index')
     ->middleware('auth')
;;

Route::get('/menu', 'MenuController@index');

Route::post('/address/{id}', 'HomeController@editAddress')
     ->middleware('auth')
;;;

Route::post('/address', 'HomeController@createAddress')
     ->middleware('auth')
;;;

Route::get('/address/{id}/delete', 'HomeController@deleteAddress')
     ->middleware('auth')
;;;

Route::get('/contacts', 'MainController@contacts');

Route::get(
    '/dish', function () {
    return view('dish');
}
);

Route::get('/subscribe', 'MainController@questions');

Route::get('/agreement', 'MainController@agreement');

Route::get('/week/{id}', 'MenuController@show');

Route::get('/dishes/{week}/{category}/{dish}', 'DishesController@show');

Route::get('/cart', 'CartController@show');

Route::get('/choose/{week}', 'MainController@choose');

Route::get(
    '/promo', function () {
    return view('landing');
}
);

Route::post('/store', 'OrderController@store')
     ->middleware('auth')
;

Route::post('/order', 'CartController@checkout')
     ->middleware('auth', 'web')
;

Route::group(
    ['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get(
        '/menu', function () {
        return view('admin.admin-menu-builder');
    }
    );

    Route::get(
        '/menu-iframe', function () {
        return view('admin.menu-builder');
    }
    );
}
);

Auth::routes();

Route::post('/check', 'Auth\LoginController@checkCode');

Route::post('/profile', 'HomeController@edit')
     ->middleware('auth')
;

Route::get(
    '/compress', function () {
    $images = \App\Dish::all();
    foreach ($images as $i) {
        $str = str_replace('.png','.png.webp', $i->images);
        echo($str);
//        $i->update(['images' => $str]);
//        echo(str_replace('.png','.png.webp', $i->images));
    }
});

