<?php

use App\Category;
use App\Core\Menu\DishHashTag;
use App\Core\Menu\HashTag;
use App\Core\Menu\WeekCategory;
use App\Dish;
use App\Subscribe;
use App\UserAddress;
use App\Week;
use App\WeekCategoryDish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'address', 'namespace' => 'Api'], function () {
    Route::post('/{id}', 'HomeController@editAddressApi')->middleware('auth:api');
    Route::post('/', 'HomeController@createAddressApi')->middleware('auth:api');
    Route::delete('/{id}', 'HomeController@deleteAddressApi')->middleware('auth:api');
    Route::get('/', 'HomeController@getAddressApi')->middleware('auth:api');
    // function (Request $request)
    // {
    //     print_r($request->headers->all());
    // });
    //
});



//Route::get('/menu', 'MenuController@index');

Route::group(['prefix' => 'orders', 'middleware' => 'auth:api'], function () {
    Route::post('/create', 'Api\OrderController@store');
    Route::get('/', 'HomeController@incompleteOrders');
    Route::get('/history', 'HomeController@history');
});

Route::namespace('Api')->group(function () {

    Route::post('/set/token', function (Request $request)
    {
        $request->validate([
            'token' => 'required'
        ]);
        Auth::user()->update([
            'token' => $request->token
        ]);

    })->middleware('auth:api');

    Route::get('/questions', 'HomeController@questions');

    Route::get('/random/user', 'Auth\LoginController@random');

    Route::get('/menus', 'MenuController@mobileIndex');

    Route::get('/menu', 'MenuController@index')->name('menu');

    Route::get('/dish/{id}', 'DishesController@show');

    Route::group(['prefix' => 'auth'], function () {
        Route::post('/city', 'UserController@createAddressOnlyCity')->middleware('auth:api');
        Route::get('/phone', 'Auth\LoginController@userLogin');
        Route::post('/code', 'Auth\LoginController@checkCode');
    });

    Route::resource('/weeks', 'WeeksController');
    Route::get('/weeks/{week}/sync/{category}', 'WeeksController@sync');
    Route::delete('/weeks/{week}/sync/{category}', 'WeeksController@syncDelete');

    Route::resource('/categories', 'CategoriesController');

    Route::resource('/dishes', 'DishesController');
    Route::get('/dishes/sync/{week}/{category}/{dish}', 'DishesController@sync');
    Route::delete('/dishes/sync/{week}/{category}/{dish}', 'DishesController@syncDelete');

    // Route::post('/address/{id}', 'HomeController@editAddress')->middleware('auth');;;
    // Route::post('/address', 'HomeController@createAddress')->middleware('auth');;;
    // Route::get('/address/{id}/delete', 'HomeController@deleteAddress')->middleware('auth');;;

    Route::resource('/hash-tags', 'HashTagsController');
    Route::resource('/ingredients', 'IngredientsController');
    Route::resource('/instruments', 'InstrumentsController');

    Route::get('/test', function () {
        return ['name' => 'qwe'];
    });

    Route::post('/pay', 'PaymentController@pay');

    Route::group(['prefix' => 'profile', 'middleware' => 'auth:api'], function () {
        Route::get('/', function () {
            $response = Auth::user();
            $response->address = UserAddress::where('user_id', Auth::user()->id)->first();
            return response($response, 200);
        });
        Route::post('/', function (Request $request) {
            $request->validate([
                'name' => ['required'],
                'email' => ['email']
            ]);
            Auth::user()->update([
                'name' => $request->name,
                'email' => $request->email
            ]);
            return response([], 200);
        });
        Route::get('/phone', function ()
        {
            return response([
                'phone' => setting('site.phone')
            ], 200);
        });
        Route::get('/subscribe', function ()
        {
            $data = Subscribe::where('user_id', Auth::user()->id)->first();
            if(empty($data)){
                return null;
            }
            $order = json_decode($data->order);
            $response = [];

            $week = Week::first();
            $week->categories = Category::whereIn('id', WeekCategory::where(
                    'week_id', $week->id)
                    ->select('category_id')
                    ->get())
                ->get();

                foreach ($week->categories as $category) {
                    $category->dishes = Dish::whereIn('id', WeekCategoryDish::where('category_id', $category->id)
                        ->where('week_id', $week->id)
                        ->select('dish_id')
                        ->orderBy('id', 'desc')
                        ->get()
                    )
                    ->get();
                    foreach ($category->dishes as $dish) {
                        $dish->hashTags = HashTag::find(DishHashTag::where('dish_id', $dish->id)->select('hash_tag_id')->get());
                    }
                }
            return response($week, 200);
        });
        Route::post('/subscribe', function (Request $request)
        {
            $request->validate([
                'dishes' => ['required','array'],
                'sum' => ['required', 'numeric'],
                'persons' => ['required', 'numeric'],
                'week' => ['required', 'numeric'],
                'category' => ['required', 'numeric'],
                'address_id' => ['required', 'exists:user_addresses,id'],
                // 'subscribe' => ['required', 'boolean']
            ]);
            $order = json_encode($request->all());
            Subscribe::where('user_id', Auth::user()->id)->first()->update([
                'order' => $order
            ]);
            return response([], 200);
        });

        Route::delete('/subscribe', function (Request $request)
        {

            $order = json_decode(Subscribe::where('user_id', Auth::user()->id)->first()->order);
            Subscribe::where('user_id', Auth::user()->id)->first()->update([
                'order' => $order->active
            ]);
            return response([], 200);
        });
    });

    Route::group(['prefix' => 'static', 'middleware' => 'auth:api'], function () {
         Route::get('/agreement', function () {
             return response([
                'text' => setting('profil.agreement')
             ], 200);
         });
         Route::get('/payment', function () {
            return response([
                'text' => setting('profil.payment')
            ], 200);
         });
    });
});

Route::post('post3ds', function(Request $request){

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,"https://api.cloudpayments.ru/payments/cards/post3ds");
	curl_setopt($ch, CURLOPT_POST, 1);
	$post = [
		'TransactionId' => $request->MD,
		'PaRes' => $request->PaRes
	];
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_USERPWD, 'pk_033af4dd51a25f5da0745ebf874fd' . ":" . 'e4417a08fa2f6a0eeb6ca2be9ced99dc');

	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($post));

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec($ch);

	curl_close ($ch);

	return redirect('https://souschef.kz');
});
