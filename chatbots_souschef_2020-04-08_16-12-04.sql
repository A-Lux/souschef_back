-- MySQL dump 10.14  Distrib 5.5.64-MariaDB, for Linux (x86_64)
--
-- Host: srv-pleskdb47.ps.kz    Database: chatbots_souschef
-- ------------------------------------------------------
-- Server version	10.2.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `chatbots_souschef`
--


--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'TEST',NULL,NULL),(2,'Test 2','2020-04-07 10:20:07','2020-04-07 10:20:07');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_dishes`
--

DROP TABLE IF EXISTS `category_dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_dishes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  `dish_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_dishes_category_id_foreign` (`category_id`),
  KEY `category_dishes_dish_id_foreign` (`dish_id`),
  CONSTRAINT `category_dishes_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `category_dishes_dish_id_foreign` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_dishes`
--

LOCK TABLES `category_dishes` WRITE;
/*!40000 ALTER TABLE `category_dishes` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_prices`
--

DROP TABLE IF EXISTS `category_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_prices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  `dishes_count` int(11) DEFAULT NULL,
  `person_count` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_prices_category_id_foreign` (`category_id`),
  CONSTRAINT `category_prices_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_prices`
--

LOCK TABLES `category_prices` WRITE;
/*!40000 ALTER TABLE `category_prices` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codes`
--

DROP TABLE IF EXISTS `codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codes`
--

LOCK TABLES `codes` WRITE;
/*!40000 ALTER TABLE `codes` DISABLE KEYS */;
INSERT INTO `codes` VALUES (6,'9593',4,1,'2020-04-07 06:02:16','2020-04-07 06:02:16'),(7,'3123',5,1,'2020-04-08 01:13:02','2020-04-08 01:13:02'),(8,'0455',5,1,'2020-04-08 01:13:04','2020-04-08 01:13:04'),(9,'8265',5,1,'2020-04-08 01:13:04','2020-04-08 01:13:04'),(10,'1168',5,1,'2020-04-08 01:13:05','2020-04-08 01:13:05'),(11,'3540',5,1,'2020-04-08 01:13:05','2020-04-08 01:13:05'),(12,'1923',5,1,'2020-04-08 01:13:05','2020-04-08 01:13:05'),(13,'4167',5,1,'2020-04-08 01:13:05','2020-04-08 01:13:05'),(14,'5333',5,1,'2020-04-08 01:13:05','2020-04-08 01:13:05'),(15,'9581',5,1,'2020-04-08 01:13:06','2020-04-08 01:13:06'),(16,'3718',6,1,'2020-04-08 01:31:09','2020-04-08 01:31:09'),(17,'3115',5,1,'2020-04-08 01:33:23','2020-04-08 01:33:23');
/*!40000 ALTER TABLE `codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `components`
--

DROP TABLE IF EXISTS `components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components`
--

LOCK TABLES `components` WRITE;
/*!40000 ALTER TABLE `components` DISABLE KEYS */;
/*!40000 ALTER TABLE `components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Имя',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Пароль',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Токен восстановления',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Дата создания',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Дата обновления',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Аватар',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Роль',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Имя',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Дата создания',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Дата обновления',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Имя',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Дата создания',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Дата обновления',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Отображаемое имя',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Роль',1,1,1,1,1,1,NULL,9),(22,5,'id','text','Id',1,0,0,0,0,0,'{}',1),(23,5,'name','text','Name',0,1,1,1,1,1,'{}',2),(24,5,'images','multiple_images','Images',0,1,1,1,1,1,'{}',3),(25,5,'shelf_life','number','Shelf Life',0,1,1,1,1,1,'{}',4),(26,5,'weight','number','Weight',0,1,1,1,1,1,'{}',5),(27,5,'cooking_time','number','Cooking Time',0,1,1,1,1,1,'{}',6),(28,5,'cooking_difficulty','number','Cooking Difficulty',0,1,1,1,1,1,'{}',7),(29,5,'energy_value','number','Energy Value',0,1,1,1,1,1,'{}',8),(30,5,'protein','number','Protein',0,1,1,1,1,1,'{}',9),(31,5,'fat','number','Fat',0,1,1,1,1,1,'{}',10),(32,5,'carbohydrates','number','Carbohydrates',0,1,1,1,1,1,'{}',11),(33,5,'description','text','Description',0,1,1,1,1,1,'{}',12),(34,5,'recipe','text','Recipe',0,1,1,1,1,1,'{}',13),(35,5,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',14),(36,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',15),(37,5,'dish_belongstomany_ingredient_relationship','relationship','ingredients',0,1,1,1,1,1,'{\"model\":\"App\\\\Ingredient\",\"table\":\"ingredients\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"dish_ingredients\",\"pivot\":\"1\",\"taggable\":\"0\"}',16),(38,6,'id','text','Id',1,0,0,0,0,0,'{}',1),(39,6,'photo','image','Photo',0,1,1,1,1,1,'{}',2),(40,6,'name','text','Name',0,1,1,1,1,1,'{}',3),(41,6,'shelf_time','text','Shelf Time',0,1,1,1,1,1,'{}',4),(42,6,'storage_condition','text','Storage Condition',0,1,1,1,1,1,'{}',5),(43,6,'manufacturer','text','Manufacturer',0,1,1,1,1,1,'{}',6),(44,6,'energy_value','number','Energy Value',0,1,1,1,1,1,'{}',7),(45,6,'protein','number','Protein',0,1,1,1,1,1,'{}',8),(46,6,'fat','number','Fat',0,1,1,1,1,1,'{}',9),(47,6,'carbohydrates','number','Carbohydrates',0,1,1,1,1,1,'{}',10),(48,6,'manufacturer_country_id','number','Manufacturer Country Id',1,1,1,1,1,1,'{}',11),(49,6,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',12),(50,6,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',13),(55,9,'id','text','Id',1,0,0,0,0,0,'{}',1),(57,9,'name','text','Name',0,1,1,1,1,1,'{}',3),(58,9,'social','image','Social',0,1,1,1,1,1,'{}',4),(59,9,'position','text','Position',0,1,1,1,1,1,'{}',5),(60,9,'image','image','Image',0,1,1,1,1,1,'{}',6),(61,9,'stars','number','Stars',0,1,1,1,1,1,'{\"min\":0,\"max\":5}',7),(62,9,'text','text','Text',0,1,1,1,1,1,'{}',8),(63,9,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',9),(64,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',10),(65,9,'avatar','image','Avatar',0,1,1,1,1,1,'{}',2),(66,11,'id','text','Id',1,0,0,0,0,0,'{}',1),(67,11,'name','text','Name',0,1,1,1,1,1,'{}',2),(68,11,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(69,11,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(70,13,'id','text','Id',1,0,0,0,0,0,'{}',1),(71,13,'name','text','Name',0,1,1,1,1,1,'{}',2),(72,13,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(73,13,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(74,5,'dish_belongstomany_hash_tag_relationship','relationship','hash_tags',0,1,1,1,1,1,'{\"model\":\"App\\\\HashTag\",\"table\":\"hash_tags\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"dish_hash_tags\",\"pivot\":\"1\",\"taggable\":\"on\"}',17);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `display_name_singular` varchar(255) NOT NULL,
  `display_name_plural` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `model_name` varchar(255) DEFAULT NULL,
  `policy_name` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','Пользователь','Пользователи','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(2,'menus','menus','Меню','Меню','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(3,'roles','roles','Роль','Роли','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(5,'dishes','dishes','Dish','Dishes',NULL,'App\\Dish',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-03-23 23:32:10','2020-04-07 21:42:00'),(6,'ingredients','ingredients','Ingredient','Ingredients',NULL,'App\\Ingredient',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2020-03-24 00:06:19','2020-03-24 00:06:19'),(9,'index_bottom_slides','index-bottom-slides','Index Bottom Slide','Index Bottom Slides',NULL,'App\\IndexBottomSlide',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2020-03-26 12:23:22','2020-04-03 05:05:59'),(11,'categories','categories','Category','Categories',NULL,'App\\Category',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2020-04-07 10:19:43','2020-04-07 10:19:43'),(13,'hash_tags','hash-tags','Hash Tag','Hash Tags',NULL,'App\\HashTag',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2020-04-07 21:39:13','2020-04-07 21:39:13');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_hash_tags`
--

DROP TABLE IF EXISTS `dish_hash_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_hash_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) unsigned DEFAULT NULL,
  `hash_tag_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dish_hash_tags_dish_id_foreign` (`dish_id`),
  KEY `dish_hash_tags_hash_tag_id_foreign` (`hash_tag_id`),
  CONSTRAINT `dish_hash_tags_dish_id_foreign` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dish_hash_tags_hash_tag_id_foreign` FOREIGN KEY (`hash_tag_id`) REFERENCES `hash_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_hash_tags`
--

LOCK TABLES `dish_hash_tags` WRITE;
/*!40000 ALTER TABLE `dish_hash_tags` DISABLE KEYS */;
INSERT INTO `dish_hash_tags` VALUES (1,2,1,NULL,NULL),(2,2,2,NULL,NULL),(3,2,3,NULL,NULL),(4,1,1,NULL,NULL);
/*!40000 ALTER TABLE `dish_hash_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_ingredients`
--

DROP TABLE IF EXISTS `dish_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_ingredients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `count` int(11) DEFAULT NULL,
  `dish_id` bigint(20) unsigned DEFAULT NULL,
  `ingredient_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dish_ingredients_dish_id_foreign` (`dish_id`),
  KEY `dish_ingredients_ingredient_id_foreign` (`ingredient_id`),
  CONSTRAINT `dish_ingredients_dish_id_foreign` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dish_ingredients_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_ingredients`
--

LOCK TABLES `dish_ingredients` WRITE;
/*!40000 ALTER TABLE `dish_ingredients` DISABLE KEYS */;
INSERT INTO `dish_ingredients` VALUES (1,NULL,1,1,NULL,NULL),(2,NULL,2,1,NULL,NULL);
/*!40000 ALTER TABLE `dish_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_instruments`
--

DROP TABLE IF EXISTS `dish_instruments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_instruments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) unsigned DEFAULT NULL,
  `instrument_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dish_instruments_dish_id_foreign` (`dish_id`),
  KEY `dish_instruments_instrument_id_foreign` (`instrument_id`),
  CONSTRAINT `dish_instruments_dish_id_foreign` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dish_instruments_instrument_id_foreign` FOREIGN KEY (`instrument_id`) REFERENCES `instruments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_instruments`
--

LOCK TABLES `dish_instruments` WRITE;
/*!40000 ALTER TABLE `dish_instruments` DISABLE KEYS */;
/*!40000 ALTER TABLE `dish_instruments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes`
--

DROP TABLE IF EXISTS `dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `images` text DEFAULT NULL,
  `shelf_life` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `cooking_time` int(11) DEFAULT NULL,
  `cooking_difficulty` int(11) DEFAULT NULL,
  `energy_value` int(11) DEFAULT NULL,
  `protein` int(11) DEFAULT NULL,
  `fat` int(11) DEFAULT NULL,
  `carbohydrates` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `recipe` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes`
--

LOCK TABLES `dishes` WRITE;
/*!40000 ALTER TABLE `dishes` DISABLE KEYS */;
INSERT INTO `dishes` VALUES (1,'TEST','[\"dishes\\/April2020\\/4umBUqAvkg5K9lND0Hii.png\",\"dishes\\/April2020\\/Pv7OjRWV5TI6yXHjh9uq.png\",\"dishes\\/April2020\\/zsL7nK5KzAyxuJy3MjJU.png\"]',5,5,10,37,444,444,444,444,'Описание блюда','Рецепт',NULL,'2020-04-07 12:14:04'),(2,'Test 2','[\"dishes\\/April2020\\/YBUvLzFSIwBwJIYxhdVM.png\"]',1,2,3,4,5,6,7,8,'Описание блюда #2','Рецепт блюда #2','2020-04-07 21:16:01','2020-04-07 21:16:01');
/*!40000 ALTER TABLE `dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hash_tags`
--

DROP TABLE IF EXISTS `hash_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hash_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hash_tags`
--

LOCK TABLES `hash_tags` WRITE;
/*!40000 ALTER TABLE `hash_tags` DISABLE KEYS */;
INSERT INTO `hash_tags` VALUES (1,'Hash tag 1','2020-04-07 21:42:32','2020-04-07 21:42:32'),(2,'Hash tag 2','2020-04-07 21:42:45','2020-04-07 21:42:45'),(3,'Hash tag 3','2020-04-07 21:43:00','2020-04-07 21:43:00');
/*!40000 ALTER TABLE `hash_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `index_bottom_slides`
--

DROP TABLE IF EXISTS `index_bottom_slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `index_bottom_slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `avatar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stars` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `index_bottom_slides`
--

LOCK TABLES `index_bottom_slides` WRITE;
/*!40000 ALTER TABLE `index_bottom_slides` DISABLE KEYS */;
INSERT INTO `index_bottom_slides` VALUES (1,'index-bottom-slides/March2020/cb3dg9plUlHJtwByTlMQ.png','Test','index-bottom-slides/March2020/O2j0fGkwTOUBX4xv7NQw.png','CEO','index-bottom-slides/March2020/USsntSmmpTbn1Y8Ot28S.jpg',5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse metus enim, lobortis ac tempus eget, viverra nec leo. Duis vitae massa id arcu laoreet semper. Sed vel accumsan tellus. Pellentesque tempus tincidunt libero ut sodales. Ut sit amet purus tincidunt, aliquam urna id, varius ipsum. Nulla quis accumsan leo, at lobortis nibh. Curabitur justo elit, sodales sed purus hendrerit, aliquet blandit metus. Donec tincidunt vitae dolor in ullamcorper. Vivamus vel enim ut ipsum molestie pulvinar ac id libero. Curabitur leo nibh, posuere congue ipsum nec, tempor tempus magna. Nam hendrerit viverra placerat. Suspendisse potenti. Nam scelerisque sollicitudin dignissim. Donec et auctor nunc, at mollis lorem.','2020-03-26 12:29:00','2020-04-03 05:05:18'),(2,'index-bottom-slides/April2020/FGCL7TjbnJ2mfmRc111n.png',NULL,'index-bottom-slides/April2020/WV4095cseF1JiODaPHII.png','2',NULL,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse metus enim, lobortis ac tempus eget, viverra nec leo. Duis vitae massa id arcu laoreet semper. Sed vel accumsan tellus. Pellentesque tempus tincidunt libero ut sodales. Ut sit amet purus tincidunt, aliquam urna id, varius ipsum. Nulla quis accumsan leo, at lobortis nibh. Curabitur justo elit, sodales sed purus hendrerit, aliquet blandit metus. Donec tincidunt vitae dolor in ullamcorper. Vivamus vel enim ut ipsum molestie pulvinar ac id libero. Curabitur leo nibh, posuere congue ipsum nec, tempor tempus magna. Nam hendrerit viverra placerat. Suspendisse potenti. Nam scelerisque sollicitudin dignissim. Donec et auctor nunc, at mollis lorem.','2020-04-03 05:07:13','2020-04-03 05:07:13');
/*!40000 ALTER TABLE `index_bottom_slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `shelf_time` varchar(255) DEFAULT NULL,
  `storage_condition` varchar(255) DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `energy_value` int(11) DEFAULT NULL,
  `protein` int(11) DEFAULT NULL,
  `fat` int(11) DEFAULT NULL,
  `carbohydrates` int(11) DEFAULT NULL,
  `manufacturer_country_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients`
--

LOCK TABLES `ingredients` WRITE;
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` VALUES (1,NULL,'Test',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2020-03-24 00:06:52','2020-03-24 00:06:52');
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instruments`
--

DROP TABLE IF EXISTS `instruments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instruments`
--

LOCK TABLES `instruments` WRITE;
/*!40000 ALTER TABLE `instruments` DISABLE KEYS */;
/*!40000 ALTER TABLE `instruments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_blocks`
--

DROP TABLE IF EXISTS `main_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_blocks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_blocks`
--

LOCK TABLES `main_blocks` WRITE;
/*!40000 ALTER TABLE `main_blocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `target` varchar(255) NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `parameters` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Панель управления','','_self','voyager-boat',NULL,NULL,1,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.dashboard',NULL),(2,1,'Медиа','','_self','voyager-images',NULL,NULL,5,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.media.index',NULL),(3,1,'Пользователи','','_self','voyager-person',NULL,NULL,3,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.users.index',NULL),(4,1,'Роли','','_self','voyager-lock',NULL,NULL,2,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.roles.index',NULL),(5,1,'Инструменты','','_self','voyager-tools',NULL,NULL,9,'2020-03-02 02:03:02','2020-03-02 02:03:02',NULL,NULL),(6,1,'Конструктор Меню','','_self','voyager-list',NULL,5,10,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.menus.index',NULL),(7,1,'База данных','','_self','voyager-data',NULL,5,11,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,12,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.bread.index',NULL),(10,1,'Настройки','','_self','voyager-settings',NULL,NULL,14,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,NULL,13,'2020-03-02 02:03:02','2020-03-02 02:03:02','voyager.hooks',NULL),(12,1,'Menu','/admin/menu','_self',NULL,'#000000',NULL,15,'2020-03-02 02:06:42','2020-03-02 02:06:42',NULL,''),(13,1,'Dishes','','_self',NULL,NULL,NULL,16,'2020-03-23 23:32:10','2020-03-23 23:32:10','voyager.dishes.index',NULL),(14,1,'Ingredients','','_self',NULL,NULL,NULL,17,'2020-03-24 00:06:19','2020-03-24 00:06:19','voyager.ingredients.index',NULL),(15,2,'Главная','/','_self',NULL,'#000000',NULL,18,'2020-03-24 15:03:25','2020-03-24 15:03:25',NULL,''),(16,2,'КАК ЭТО РАБОТАЕТ?','/about','_self',NULL,'#000000',NULL,19,'2020-03-24 15:03:51','2020-03-24 15:03:51',NULL,''),(17,2,'МЕНЮ','/menu','_self',NULL,'#000000',NULL,20,'2020-03-24 15:04:14','2020-03-24 15:04:14',NULL,''),(18,2,'КОНТАКТЫ','/contacts','_self',NULL,'#000000',NULL,21,'2020-03-24 15:04:35','2020-03-24 15:04:35',NULL,''),(19,2,'Аккаунт','/home','_self',NULL,'#000000',NULL,22,'2020-03-24 15:05:03','2020-03-24 15:05:03',NULL,''),(21,1,'Index Bottom Slides','','_self',NULL,NULL,NULL,23,'2020-03-26 12:23:22','2020-03-26 12:23:22','voyager.index-bottom-slides.index',NULL),(22,1,'Categories','','_self',NULL,NULL,NULL,24,'2020-04-07 10:19:43','2020-04-07 10:19:43','voyager.categories.index',NULL),(23,1,'Hash Tags','','_self',NULL,NULL,NULL,25,'2020-04-07 21:39:14','2020-04-07 21:39:14','voyager.hash-tags.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2020-03-02 02:03:02','2020-03-02 02:03:02'),(2,'header','2020-03-24 15:02:35','2020-03-24 15:02:35'),(3,'footer','2020-03-26 14:50:14','2020-03-26 14:50:14');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metas`
--

DROP TABLE IF EXISTS `metas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metas`
--

LOCK TABLES `metas` WRITE;
/*!40000 ALTER TABLE `metas` DISABLE KEYS */;
/*!40000 ALTER TABLE `metas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2019_08_19_000000_create_failed_jobs_table',1),(24,'2020_02_11_091137_create_weeks_table',1),(25,'2020_02_11_091145_create_categories_table',1),(26,'2020_02_11_091207_create_week_categories_table',1),(27,'2020_02_11_091238_create_week_category_prices_table',1),(28,'2020_02_11_091244_create_category_prices_table',1),(29,'2020_02_11_092843_create_dishes_table',1),(30,'2020_02_11_092848_create_category_dishes_table',1),(31,'2020_02_11_093209_create_hash_tags_table',1),(32,'2020_02_11_093214_create_dish_hash_tags_table',1),(33,'2020_02_11_093415_create_ingredients_table',1),(34,'2020_02_11_093425_create_dish_ingredients_table',1),(35,'2020_02_11_093521_create_instruments_table',1),(36,'2020_02_11_093531_create_dish_instruments_table',1),(37,'2020_02_19_081822_create_sliders_table',1),(38,'2020_02_19_081829_create_slides_table',1),(39,'2020_02_19_081846_create_main_blocks_table',1),(40,'2020_02_19_090710_create_pages_table',1),(41,'2020_02_19_090717_create_metas_table',1),(42,'2020_02_19_090723_create_page_metas_table',1),(43,'2020_02_19_090737_create_components_table',1),(44,'2020_02_19_091029_create_page_components_table',1),(45,'2020_02_19_091142_create_url_pages_table',1),(46,'2020_02_19_091209_create_contacts_table',1),(47,'2020_02_19_091222_create_questions_table',1),(48,'2020_02_20_135544_create_cities_table',1),(49,'2020_02_20_143329_create_week_category_dishes_table',1),(50,'2020_02_24_104223_week_category_order',1),(51,'2020_02_26_050110_week_category_dishes_order',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_components`
--

DROP TABLE IF EXISTS `page_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_components` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_components`
--

LOCK TABLES `page_components` WRITE;
/*!40000 ALTER TABLE `page_components` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_metas`
--

DROP TABLE IF EXISTS `page_metas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_metas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_metas`
--

LOCK TABLES `page_metas` WRITE;
/*!40000 ALTER TABLE `page_metas` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_metas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(2,'browse_bread',NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(3,'browse_database',NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(4,'browse_media',NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(5,'browse_compass',NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(6,'browse_menus','menus','2020-03-02 02:03:02','2020-03-02 02:03:02'),(7,'read_menus','menus','2020-03-02 02:03:02','2020-03-02 02:03:02'),(8,'edit_menus','menus','2020-03-02 02:03:02','2020-03-02 02:03:02'),(9,'add_menus','menus','2020-03-02 02:03:02','2020-03-02 02:03:02'),(10,'delete_menus','menus','2020-03-02 02:03:02','2020-03-02 02:03:02'),(11,'browse_roles','roles','2020-03-02 02:03:02','2020-03-02 02:03:02'),(12,'read_roles','roles','2020-03-02 02:03:02','2020-03-02 02:03:02'),(13,'edit_roles','roles','2020-03-02 02:03:02','2020-03-02 02:03:02'),(14,'add_roles','roles','2020-03-02 02:03:02','2020-03-02 02:03:02'),(15,'delete_roles','roles','2020-03-02 02:03:02','2020-03-02 02:03:02'),(16,'browse_users','users','2020-03-02 02:03:02','2020-03-02 02:03:02'),(17,'read_users','users','2020-03-02 02:03:02','2020-03-02 02:03:02'),(18,'edit_users','users','2020-03-02 02:03:02','2020-03-02 02:03:02'),(19,'add_users','users','2020-03-02 02:03:02','2020-03-02 02:03:02'),(20,'delete_users','users','2020-03-02 02:03:02','2020-03-02 02:03:02'),(21,'browse_settings','settings','2020-03-02 02:03:02','2020-03-02 02:03:02'),(22,'read_settings','settings','2020-03-02 02:03:02','2020-03-02 02:03:02'),(23,'edit_settings','settings','2020-03-02 02:03:02','2020-03-02 02:03:02'),(24,'add_settings','settings','2020-03-02 02:03:02','2020-03-02 02:03:02'),(25,'delete_settings','settings','2020-03-02 02:03:02','2020-03-02 02:03:02'),(26,'browse_hooks',NULL,'2020-03-02 02:03:02','2020-03-02 02:03:02'),(27,'browse_dishes','dishes','2020-03-23 23:32:10','2020-03-23 23:32:10'),(28,'read_dishes','dishes','2020-03-23 23:32:10','2020-03-23 23:32:10'),(29,'edit_dishes','dishes','2020-03-23 23:32:10','2020-03-23 23:32:10'),(30,'add_dishes','dishes','2020-03-23 23:32:10','2020-03-23 23:32:10'),(31,'delete_dishes','dishes','2020-03-23 23:32:10','2020-03-23 23:32:10'),(32,'browse_ingredients','ingredients','2020-03-24 00:06:19','2020-03-24 00:06:19'),(33,'read_ingredients','ingredients','2020-03-24 00:06:19','2020-03-24 00:06:19'),(34,'edit_ingredients','ingredients','2020-03-24 00:06:19','2020-03-24 00:06:19'),(35,'add_ingredients','ingredients','2020-03-24 00:06:19','2020-03-24 00:06:19'),(36,'delete_ingredients','ingredients','2020-03-24 00:06:19','2020-03-24 00:06:19'),(42,'browse_index_bottom_slides','index_bottom_slides','2020-03-26 12:23:22','2020-03-26 12:23:22'),(43,'read_index_bottom_slides','index_bottom_slides','2020-03-26 12:23:22','2020-03-26 12:23:22'),(44,'edit_index_bottom_slides','index_bottom_slides','2020-03-26 12:23:22','2020-03-26 12:23:22'),(45,'add_index_bottom_slides','index_bottom_slides','2020-03-26 12:23:22','2020-03-26 12:23:22'),(46,'delete_index_bottom_slides','index_bottom_slides','2020-03-26 12:23:22','2020-03-26 12:23:22'),(47,'browse_categories','categories','2020-04-07 10:19:43','2020-04-07 10:19:43'),(48,'read_categories','categories','2020-04-07 10:19:43','2020-04-07 10:19:43'),(49,'edit_categories','categories','2020-04-07 10:19:43','2020-04-07 10:19:43'),(50,'add_categories','categories','2020-04-07 10:19:43','2020-04-07 10:19:43'),(51,'delete_categories','categories','2020-04-07 10:19:43','2020-04-07 10:19:43'),(52,'browse_hash_tags','hash_tags','2020-04-07 21:39:13','2020-04-07 21:39:13'),(53,'read_hash_tags','hash_tags','2020-04-07 21:39:13','2020-04-07 21:39:13'),(54,'edit_hash_tags','hash_tags','2020-04-07 21:39:13','2020-04-07 21:39:13'),(55,'add_hash_tags','hash_tags','2020-04-07 21:39:13','2020-04-07 21:39:13'),(56,'delete_hash_tags','hash_tags','2020-04-07 21:39:13','2020-04-07 21:39:13');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Администратор','2020-03-02 02:03:02','2020-03-02 02:03:02'),(2,'user','Обычный Пользователь','2020-03-02 02:03:02','2020-03-02 02:03:02');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `value` text DEFAULT NULL,
  `details` text DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Название Сайта','Название Сайта','','text',1,'Site'),(2,'site.description','Описание Сайта','Описание Сайта','','text',2,'Site'),(3,'site.logo','Логотип Сайта','settings/March2020/J40sftSOJQFRvz5e2XOj.png','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Фоновое Изображение для Админки','','','image',5,'Admin'),(6,'admin.title','Название Админки','Voyager','','text',1,'Admin'),(7,'admin.description','Описание Админки','Добро пожаловать в Voyager. Пропавшую Админку для Laravel','','text',2,'Admin'),(8,'admin.loader','Загрузчик Админки','','','image',3,'Admin'),(9,'admin.icon_image','Иконка Админки','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (используется для панели администратора)',NULL,'','text',1,'Admin'),(11,'main.background_one','Фон первого блока','settings/March2020/WVyBhRiC7xd7wIxyP2UE.jpg',NULL,'image',7,'Main'),(12,'main.heading_one','Заголовок первого блока','3 вида наборов',NULL,'text',6,'Main'),(13,'main.text_one','Текст первого блока','Получайте коробку с продуктами и рецептами для ужинов каждую неделю и готовьте дома. Пусть каждый ужин будет маленьким праздником!',NULL,'text_area',8,'Main'),(14,'main.button_text_one','Текст на кнопке в первом блоке','НАЧНИТЕ ГОТОВИТЬ СЕЙЧАС',NULL,'text',9,'Main'),(15,'main.button_link_one','Ссылка на кнопке в первом блоке','/menu',NULL,'text',10,'Main'),(16,'main.background_two','Фон второго блока','settings/March2020/UfQDmIMVTgJaKdacx5Jr.jpg',NULL,'image',19,'Main'),(17,'main.heading_two','Заголовок второго блока','30+ РЕЦЕПТОВ ЕЖЕНЕДЕЛЬНО',NULL,'text',20,'Main'),(18,'main.text_two','Текст второго блока','Выбирайте вкуснейшие рецепты от шеф-поваров и диетологов',NULL,'text_area',21,'Main'),(19,'main.button_text_two','Текст на кнопке во втором блоке','СМОТРЕТЬ МЕНЮ',NULL,'text',22,'Main'),(20,'main.button_link_two','Ссылка на кнопке во втором блоке','/menu',NULL,'text',23,'Main'),(21,'main.square_one_title','Название в первом квадрате','Выбирайте меню',NULL,'text',11,'Main'),(22,'main.square_one_text','Текст в первом квадрате','Каждую неделю 30+ блюд от шеф-поваров и диетологов',NULL,'text',12,'Main'),(23,'main.square_two_title','Название во втором квадрате','Свежие продукты',NULL,'text',13,'Main'),(24,'main.square_two_text','Текст во втором квадрате','Расфасованные ингредиенты с пошаговыми фоторецептами',NULL,'text',14,'Main'),(25,'main.square_three_title','Название в третьем квадрате','Блюда за 15-40 минут',NULL,'text',15,'Main'),(26,'main.square_three_text','Текст в третьем квадрате','Шедевры с любовью! Готовить - одно удовольствие!',NULL,'text',16,'Main'),(27,'main.square_four_title','Название в четвёртом квадрате','Никаких отходов',NULL,'text',17,'Main'),(28,'main.square_four_text','Текст в четвёртом квадрате','Скажите \"нет\" испорченным продуктам и загрязнению природы',NULL,'text',18,'Main'),(29,'main.background_three','Фон третьего блока','settings/March2020/aL1d0vd07qgNk7wKKe1F.jpg',NULL,'image',24,'Main'),(30,'main.heading_three','Заголовок третьего блока','КАЧЕСТВЕННЫЕ ИНГРИДИЕНТЫ',NULL,'text',25,'Main'),(31,'main.text_three','Текст третьего блока','Готовьте с лучшими ингредиентами от наших партнеров и поставщиков.',NULL,'text_area',26,'Main'),(32,'main.button_text_three','Текст на кнопке в третьем блоке','ПОСТАВЩИКИ',NULL,'text',27,'Main'),(33,'main.button_link_three','Ссылка на кнопке в третьем блоке','/menu',NULL,'text',28,'Main'),(34,'main.background_four','Фон четвёртого блока','settings/March2020/VAut7V68F9S4Db8P57P4.jpg',NULL,'image',29,'Main'),(35,'main.heading_four','Заголовок четвёртого блока','МЕНЮ РАЗРАБОТАНО ШЕФ-ПОВАРАМИ И УТВЕРЖДЕНО ДИЕТОЛОГАМИ',NULL,'text',30,'Main'),(36,'main.text_four','Текст четвёртого блока','Наша команда талантливых поваров и диетологов создает еженедельное меню, полное рецептов здорового ужина.',NULL,'text_area',31,'Main'),(37,'main.button_text_four','Текст на кнопке в четвёртом блоке','НАША КОМАНДА',NULL,'text',32,'Main'),(38,'main.button_link_four','Ссылка на кнопке в четвёртом блоке','/menu',NULL,'text',33,'Main'),(39,'main.image_reviews','Изображение в блоке с отзывами','settings/March2020/gPK9xbBPkUj6JS2FSy6i.png',NULL,'image',34,'Main'),(40,'main.title_reviews','Заголовок блока с отзывами','Отзывы Клиентов',NULL,'text',35,'Main'),(41,'main.subtitle_reviews','Подзаголовок блока с отзывами','What our clients say',NULL,'text',36,'Main'),(42,'main.image_one','Первое изображение снизу','settings/March2020/K1UMcQ8eD1a8BW3B3MxT.png',NULL,'image',37,'Main'),(43,'main.image_two','Второе изображение снизу','settings/March2020/YkTHSIcuVGC6PFc71oAC.png',NULL,'image',38,'Main'),(44,'main.image_three','Третье изображение снизу','settings/March2020/A1iR2QifxOV3ur5CcWTa.png',NULL,'image',39,'Main'),(45,'site.copyright','Копирайт','COPYRIGHT © 2020 | МАГАЗИН РЕЦЕПТОВ',NULL,'text',40,'Site'),(46,'profil.payment','Условия доставки и оплаты','<h1 style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 20px; text-align: center;\">Пользовательское соглашение (договор о присоединении)</h1>\r\n<div class=\"terms-title\" style=\"box-sizing: border-box; font-size: 16px;\">\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; text-align: center; font-size: 15px;\">Редакция № 18 от 16 мая 2019 г. г. Москва</p>\r\n</div>\r\n<h2 style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 18px;\">1. Общие положения и предмет Соглашения.</h2>\r\n<div class=\"terms-text\" style=\"box-sizing: border-box; font-size: 16px;\">\r\n<div class=\"sub-terms\" style=\"box-sizing: border-box; display: flex; flex-direction: column; padding: 0px 1rem;\">\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.1. Продавец предлагает Покупателю возможность заказа, оплаты и доставки Товаров через Интернет-магазин, по телефону или через иные средства связи.</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.2. Предметом настоящего Соглашения являются условия заключения с Покупателем договора дистанционной купли-продажи Товаров и их доставки в соответствии с условиями Соглашения и за цену, установленную Продавцом на Товары в Интернет-магазине, а также порядок оплаты и приемки Товаров Покупателем в соответствии с условиями настоящего Соглашения и Условиями доставки.</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.3. Покупатель, оформляя заказ на Товары через Интернет-магазин либо посредством телефонной связи, принимает все условия настоящего Соглашения (без полных или частичных изъятий). В случае несогласия с условиями Соглашения полностью либо в его части, Покупатель обязан отказаться от покупки Товаров.</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.4. Условия Соглашения могут быть изменены Продавцом без какого-либо специального уведомления Покупателя. Действие новой редакции Соглашения начинается по истечении 3 (трех) дней с момента ее размещения на соответствующей интернет-странице.</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.5. Настоящее Соглашение является публичной офертой. Размещение заказа на Товар Покупателем любым способом или намерения заказать Товар, участие Покупателя в рекламных акциях Продавца, а также регистрация Покупателя в Интернет-магазине, являются акцептом оферты, что означает заключение договора дистанционной купли-продажи на условиях, изложенных в настоящем Соглашении. Договор считается заключенным с момента подтверждения оформленного Покупателем заказа Товаров, принятия Покупателем участия в рекламной акции Продавца путем направления Продавцу контактных данных в любом виде, или регистрации в Интернет-магазине. Продавец обязуется выдать Покупателю кассовый и/или товарный чек, либо иной документ подтверждающий факт оплаты, а право собственности на заказанный Покупателем Товар переходит к нему в момент получения Товара, но не ранее момента полной оплаты Товара Покупателем.</p>\r\n</div>\r\n</div>',NULL,'rich_text_box',41,'Профиль'),(47,'profil.agreement','Пользовательское соглашение','<div class=\"terms-title\" style=\"box-sizing: border-box; color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px;\">\r\n<h1 style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 20px; text-align: center;\">Пользовательское соглашение (договор о присоединении)</h1>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; text-align: center; font-size: 15px;\">Редакция № 18 от 16 мая 2019 г. г. Москва</p>\r\n</div>\r\n<div class=\"terms-text\" style=\"box-sizing: border-box; color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px;\">\r\n<h2 style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 18px;\">1. Общие положения и предмет Соглашения.</h2>\r\n<div class=\"sub-terms\" style=\"box-sizing: border-box; display: flex; flex-direction: column; padding: 0px 1rem;\">\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.1. Продавец предлагает Покупателю возможность заказа, оплаты и доставки Товаров через Интернет-магазин, по телефону или через иные средства связи.</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.2. Предметом настоящего Соглашения являются условия заключения с Покупателем договора дистанционной купли-продажи Товаров и их доставки в соответствии с условиями Соглашения и за цену, установленную Продавцом на Товары в Интернет-магазине, а также порядок оплаты и приемки Товаров Покупателем в соответствии с условиями настоящего Соглашения и Условиями доставки.</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.3. Покупатель, оформляя заказ на Товары через Интернет-магазин либо посредством телефонной связи, принимает все условия настоящего Соглашения (без полных или частичных изъятий). В случае несогласия с условиями Соглашения полностью либо в его части, Покупатель обязан отказаться от покупки Товаров.</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.4. Условия Соглашения могут быть изменены Продавцом без какого-либо специального уведомления Покупателя. Действие новой редакции Соглашения начинается по истечении 3 (трех) дней с момента ее размещения на соответствующей интернет-странице.</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; font-size: 14px; color: #333333;\">2.5. Настоящее Соглашение является публичной офертой. Размещение заказа на Товар Покупателем любым способом или намерения заказать Товар, участие Покупателя в рекламных акциях Продавца, а также регистрация Покупателя в Интернет-магазине, являются акцептом оферты, что означает заключение договора дистанционной купли-продажи на условиях, изложенных в настоящем Соглашении. Договор считается заключенным с момента подтверждения оформленного Покупателем заказа Товаров, принятия Покупателем участия в рекламной акции Продавца путем направления Продавцу контактных данных в любом виде, или регистрации в Интернет-магазине. Продавец обязуется выдать Покупателю кассовый и/или товарный чек, либо иной документ подтверждающий факт оплаты, а право собственности на заказанный Покупателем Товар переходит к нему в момент получения Товара, но не ранее момента полной оплаты Товара Покупателем.</p>\r\n</div>\r\n</div>',NULL,'rich_text_box',42,'Профиль');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,'Клиенты на главной','2020-03-26 04:29:01','2020-03-26 04:29:01');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slides` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slider_id` bigint(20) unsigned DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `slides_slider_id_foreign` (`slider_id`),
  CONSTRAINT `slides_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slides`
--

LOCK TABLES `slides` WRITE;
/*!40000 ALTER TABLE `slides` DISABLE KEYS */;
/*!40000 ALTER TABLE `slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL,
  `column_name` varchar(255) NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `url_pages`
--

DROP TABLE IF EXISTS `url_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `url_pages`
--

LOCK TABLES `url_pages` WRITE;
/*!40000 ALTER TABLE `url_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `url_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `settings` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','your@email.com',NULL,'users/default.png',NULL,'$2y$10$xFwpJ7XXb6ZfxUVngkzZJ.2Q97qDv1L6G90nZitJZTNofqEnHunjS','dYQCmYgIttFMh4X7wy7QFvamsz6WbpPeXcqIRoOm9Mucp7rMlQqKkyXHjIzw',NULL,'2020-03-02 02:04:23','2020-03-23 15:49:18'),(4,2,'','','77015455575','users/default.png',NULL,NULL,'qbHfsSol9Nw1Y0iithIrmmxk7TWKboheYdHHh8zDQp0e4qFWjM68oByMTHPx',NULL,'2020-04-07 01:02:16','2020-04-07 01:02:16'),(5,2,'','','+77762027700','users/default.png',NULL,NULL,'VV9uWxCHCz5RUFmN2WlqGR4KUVP6GPq7aT1mSx0ShMV3eVbPLEtCQGK4N40c',NULL,'2020-04-08 01:13:02','2020-04-08 01:13:02'),(6,2,'','','87762027700','users/default.png',NULL,NULL,NULL,NULL,'2020-04-08 01:31:09','2020-04-08 01:31:09');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `week_categories`
--

DROP TABLE IF EXISTS `week_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `week_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `week_id` bigint(20) unsigned DEFAULT NULL,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  `is_hit` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `week_categories_week_id_foreign` (`week_id`),
  KEY `week_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `week_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `week_categories_week_id_foreign` FOREIGN KEY (`week_id`) REFERENCES `weeks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `week_categories`
--

LOCK TABLES `week_categories` WRITE;
/*!40000 ALTER TABLE `week_categories` DISABLE KEYS */;
INSERT INTO `week_categories` VALUES (3,6,1,NULL,NULL,NULL,1),(6,6,2,NULL,NULL,NULL,2);
/*!40000 ALTER TABLE `week_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `week_category_dishes`
--

DROP TABLE IF EXISTS `week_category_dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `week_category_dishes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `week_id` bigint(20) unsigned DEFAULT NULL,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  `dish_id` bigint(20) unsigned DEFAULT NULL,
  `city_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `week_category_dishes_week_id_foreign` (`week_id`),
  KEY `week_category_dishes_category_id_foreign` (`category_id`),
  KEY `week_category_dishes_dish_id_foreign` (`dish_id`),
  KEY `week_category_dishes_city_id_foreign` (`city_id`),
  CONSTRAINT `week_category_dishes_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `week_category_dishes_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `week_category_dishes_dish_id_foreign` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `week_category_dishes_week_id_foreign` FOREIGN KEY (`week_id`) REFERENCES `weeks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `week_category_dishes`
--

LOCK TABLES `week_category_dishes` WRITE;
/*!40000 ALTER TABLE `week_category_dishes` DISABLE KEYS */;
INSERT INTO `week_category_dishes` VALUES (3,6,1,1,NULL,'2020-04-07 10:52:01','2020-04-07 10:52:01',1),(4,6,2,2,NULL,'2020-04-07 21:16:21','2020-04-07 21:16:21',1);
/*!40000 ALTER TABLE `week_category_dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `week_category_prices`
--

DROP TABLE IF EXISTS `week_category_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `week_category_prices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `week_category_id` bigint(20) unsigned DEFAULT NULL,
  `dishes_count` int(11) DEFAULT NULL,
  `person_count` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `week_category_prices_week_category_id_foreign` (`week_category_id`),
  CONSTRAINT `week_category_prices_week_category_id_foreign` FOREIGN KEY (`week_category_id`) REFERENCES `week_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `week_category_prices`
--

LOCK TABLES `week_category_prices` WRITE;
/*!40000 ALTER TABLE `week_category_prices` DISABLE KEYS */;
/*!40000 ALTER TABLE `week_category_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weeks`
--

DROP TABLE IF EXISTS `weeks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weeks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `start_at` timestamp NULL DEFAULT NULL,
  `end_at` timestamp NULL DEFAULT NULL,
  `hidden_if_expired` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weeks`
--

LOCK TABLES `weeks` WRITE;
/*!40000 ALTER TABLE `weeks` DISABLE KEYS */;
INSERT INTO `weeks` VALUES (4,'09.03 - 15.03','2020-03-08 18:00:00','2020-03-15 17:59:59',NULL,'2020-04-07 07:41:01','2020-04-07 07:41:01'),(5,'16.03 - 22.03','2020-03-15 18:00:00','2020-03-22 17:59:59',NULL,'2020-04-07 10:05:42','2020-04-07 10:05:42'),(6,'23.03 - 29.03','2020-03-22 18:00:00','2020-03-29 17:59:59',NULL,'2020-04-07 10:05:44','2020-04-07 10:05:44');
/*!40000 ALTER TABLE `weeks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-08 16:12:18

