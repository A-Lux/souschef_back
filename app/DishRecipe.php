<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DishRecipe extends Model
{
    protected $fillable = [
        'image',
        'text',
        'order',
        'dish_id',
    ];
}