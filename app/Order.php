<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['sum', 'user_id', 'address_id', 'subscribe', 'delivery_time', 'comment'];
}
