<?php

namespace App\Http\Controllers;

use App\Category;
use App\Core\Menu\DishHashTag;
use App\Core\Menu\HashTag;
use App\Core\Menu\WeekCategory;
use App\Dish;
use App\Week;
use App\WeekCategoryDish;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class MenuController extends Controller
{
    public function index(Request $request)
    {
        $weeks = \App\Week::orderBy('id', 'desc')
                          ->get()
        ;

        foreach ($weeks as $week) {
            $week->categories = Category::whereIn(
                'id', WeekCategory::where('week_id', $week->id)
                                  ->select('category_id')
                                  ->get()
            )
                                        ->get()
            ;

            foreach ($week->categories as $category) {
                $category->dishes = Dish::whereIn(
                    'id',
                    WeekCategoryDish::where('category_id', $category->id)
                                    ->where('week_id', $week->id)
                                    ->select('dish_id')
                                    ->orderBy('id', 'desc')
                                    ->get()
                )
                                        ->get()
                ;
                foreach ($category->dishes as $dish) {
                    $dish->hashTags = HashTag::find(
                        DishHashTag::where('dish_id', $dish->id)
                                   ->select('hash_tag_id')
                                   ->get()
                    );
                }
            }
        }

        return view('menu', ['weeks' => $weeks]);
    }

    public function show($id)
    {
        $current = \App\Week::where('id', $id)
                            ->orderBy('id', 'desc')
                            ->first()
        ;
        $weeks   = \App\Week::where('id', '<>', $id)
                            ->orderBy('id', 'desc')
                            ->get()
        ;
        $weeks->prepend($current);
        foreach ($weeks as $week) {
            $week->categories = Category::whereIn(
                'id', WeekCategory::where('week_id', $week->id)
                                  ->select('category_id')
                                  ->get()
            )
                                        ->get()
            ;

            foreach ($week->categories as $category) {
                $category->dishes = Dish::whereIn(
                    'id',
                    WeekCategoryDish::where('category_id', $category->id)
                                    ->where('week_id', $week->id)
                                    ->where('week_id', $week->id)
                                    ->select('dish_id')
                                    ->orderBy('id', 'desc')
                                    ->get()
                )
                                        ->get()
                ;
                foreach ($category->dishes as $dish) {
                    $dish->hashTags = HashTag::find(
                        DishHashTag::where('dish_id', $dish->id)
                                   ->select('hash_tag_id')
                                   ->get()
                    );
                    $dish->images   = !empty(json_decode($dish->images)) ? Voyager::image(
                        json_decode($dish->images)[0]
                    ) : '';
                    $dish->checked  = 0;
                }
            }
        }

        return view('category', ['weeks' => $weeks]);
    }
}
