<?php

namespace App\Http\Controllers;

use App\Category as AppCategory;
use App\Core\Menu\Dish;
use App\Core\Menu\Week;
use App\WeekCategoryDish;
use App\Core\Menu\Category;
use App\Core\Menu\DishIngredient;
use App\DishRecipe;
use App\Filters\DishFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DishResource;
use App\Ingredient;

class DishesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, DishFilter $filter)
    {
        $dishes = DishResource::collection(Dish::filter($filter)->get());

        return $dishes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Core\Menu\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function show($week, $category, $dish_id)
    {
        $dish = Dish::find($dish_id);

        $weekCatDish = WeekCategoryDish::where('week_id', $week)
            ->where('category_id', $category)
            ->where('dish_id', $dish_id)
            ->first();

        $next = WeekCategoryDish::where('week_id', $weekCatDish->week_id)
            ->where('category_id', $category)
            ->where('order', $weekCatDish->order + 1)
            ->first();
        if ($next) {
            $next = asset('/') .
                '/dishes/' .
                $week .
                '/' .
                $category .
                '/' .
                $next->dish_id;
        }

        $prev = WeekCategoryDish::where('week_id', $weekCatDish->week_id)
            ->where('category_id', $category)
            ->where('order', $weekCatDish->order - 1)
            ->first();

        if ($prev) {
            $prev = asset('/') .
                '/dishes/' .
                $week .
                '/' .
                $category .
                '/' .
                $prev->dish_id;
        }

        return view('dish', [
            'dish' => $dish,
            'recipeSlides' => DishRecipe::where('dish_id', $dish_id)->get(),
            'category' => AppCategory::find($category),
            'next' => $next,
            'prev' => $prev,
            'current' => $weekCatDish->order,
            'ingridients' => Ingredient::whereIn(
                'id',
                DishIngredient::where(
                    'dish_id',
                    $dish->id
                )
                    ->select('ingredient_id')
                    ->get()
            )
                ->where('home', 0)
                ->get(),
            'home' => Ingredient::whereIn(
                'id',
                DishIngredient::where(
                    'dish_id',
                    $dish->id
                )
                    ->select('ingredient_id')
                    ->get()
            )->where('home', 1)
                ->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Core\Menu\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dish $dish)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Core\Menu\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dish $dish)
    {
        //
    }

    public function sync(Week $week, Category $category, Dish $dish)
    {
        $week_id = $week->id;
        $category_id = $category->id;
        $dish_id = $dish->id;

        $last = WeekCategoryDish::selectRaw("MAX(`order`) as last_order")
            ->where('week_id', $week_id)
            ->where('category_id', $category_id)
            ->first();

        $order = $last ? $last->last_order + 1 : 1;

        $model = WeekCategoryDish::updateOrCreate(
            ['week_id' => $week_id, 'category_id' => $category_id, 'dish_id' => $dish_id],
            ['order' => $order]
        );

        return $dish;
    }

    public function syncDelete(Request $request, Week $week, Category $category, Dish $dish)
    {
        $category_id = $category->id;
        $week_id = $week->id;
        $dish_id = $dish->id;

        $model = WeekCategoryDish::where('week_id', $week_id)
            ->where('category_id', $category_id)
            ->where('dish_id', $dish_id)
            ->first();

        $model ? $model->delete() : false;

        return $dish;
    }
}