<?php

namespace App\Http\Controllers;

use App\UserAddress;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function createAddressOnlyCity(Request $request){
        UserAddress::create([
            'user_id' => $request->user(),
            'location' => $request->city
        ]);
        return response([], 200);
    }
}
