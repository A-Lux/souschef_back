<?php

namespace App\Http\Controllers;

use App\Category;
use App\Dish;
use App\Question;
use App\UserAddress;
use App\WeekCategoryDish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function show(Request $request)
    {

        if (session()->has('dishes')) {
            $dishes = Dish::whereIn('id', session()->get('dishes'))->get();
        } else {
            $dishes = [];
        }
		
		if(Auth::check()){
        	$addresses = UserAddress::where('user_id', $request->user()->id)->get();
		}
		else {
			$addresses = collect([]);
		}

        // if ($addresses->isEmpty()) {
        //     $addresses->prepend(
        //         UserAddress::create([
        //             'user_id' => $request->user()->id,
        //             'city' => 'Алматы'
        //         ])
        //     );
        // }

        $questions = Question::where('type', 0)->get();
        $persons = 2;
        if (session()->has('persons')) {
            $persons = session()->get('persons');
        }

        return view('checkout', [
            'dishes' => $dishes,
            'addresses' => $addresses,
            'persons' => $persons,
            'questions' => $questions
        ]);
    }

    public function add(Request $request)
    {
        session()->put('dishes', $request->dishes);
        return redirect('/cart');
    }

    public function remove(Request $request)
    {
        session()->forget('dishes');
        return redirect('/menu');
    }

    public function checkout(Request $request)
    {
        $request->validate([
            'category' => ['required', 'numeric', 'exists:categories,id'],
            'week' =>  ['required', 'numeric', 'exists:weeks,id'],
            'dishes' => ['required', 'array'],
            'dishesAmount' => ['required', 'numeric'],
            'persons' => ['required', 'numeric']
        ]);
        // $dish_ids = WeekCategoryDish::where('week_id', $request->week)
        //     ->where('category_id', $request->category)
        //     ->select('dish_id')
        //     ->get()
        //     ->toArray();

        $dishes = Dish::whereIn('id', $request->dishes)->get();
        $addresses = UserAddress::where('user_id', Auth::user()->id)->get();
        $questions = Question::where('type', 0)->get();

        if ($addresses->isEmpty()) {
            $addresses->prepend(UserAddress::create([
                'user_id' => Auth::user()->id
            ]));
        }
        session()->put('total', $dishes->sum('price'));
        session()->put('dishes', $request->dishes);
        // $price = Category::find($request->category)->price * $request->persons * $request->dishesAmount;

        $personsNumber = [
            '2' => 'two',
            '4' => 'four'
        ];

        $dishesNumber = [
            '3' => 'three',
            '5' => 'five'
        ];

        $priceColumn = 'price_' . $personsNumber[$request->persons] . '_' . $dishesNumber[$request->dishesAmount];
        $price = Category::find($request->category)->{$priceColumn};
        session()->put('price', $price);
        session()->put('persons', $request->persons);
        $questions = Question::where('type', 0)->get();
        // return view('checkout', ['dishes' => $dishes, 'addresses' => $addresses, 'persons' => $request->personAmount]);
				
        return redirect('/cart');
    }
}
