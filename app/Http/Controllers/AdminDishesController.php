<?php

namespace App\Http\Controllers;

use App\DishRecipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AdminDishesController extends VoyagerBaseController
{
    public function update(Request $request, $id)
    {
        foreach ($request->recipeSlides as $key => $value) {
            $check = DishRecipe::where('dish_id', $id)
                ->where('order', $key)
                ->exists();
            $data = [];
            if($check){
                if(isset($value['image'])){
                    if($value['image'] != ""){
                        $data['image'] = Storage::put('public/recipes/' . $id, $value['image']);
                    }
                }
                $data['text'] = $value['text'];
                DishRecipe::where('dish_id', $id)
                    ->where('order', $key)
                    ->update($data);
            }
            else{
                $data = $value;
                $data['dish_id'] = $id;
                $data['order'] = $key;
                if(isset($value['image'])){
                    $data['image'] = Storage::put('public/recipes/' . $id, $value['image']);
                }
                DishRecipe::create($data);
            }
        }
        unset($request->recipeSlides);
        return parent::update($request, $id);
    }
}