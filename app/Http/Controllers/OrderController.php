<?php
namespace App\Http\Controllers;

use App\Order;
use App\OrderDish;
use Illuminate\Http\Request;
use Exception;
use DB;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response(Order::where('user_id', $request->user()->id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'dishes'     => 'array',
                'sum'        => 'numeric',
                'persons'    => 'numeric',
                'week'       => 'numeric',
                'category'   => 'numeric',
                'address_id' => 'exists:user_addresses,id',
                'subscribe'  => 'boolean',
            ]
        );
        $order  = Order::create(
            [
                'sum'           => $request->sum,
                'address_id'    => $request->address_id,
                'user_id'       => $request->user()->id,
                'subscribe'     => $request->subscribe,
                'delivery_time' => $request->delivery_time ? $request->delivery_time : '',
                'comment'       => $request->comment,
            ]
        );
        $dishes = [];
        foreach ($request->dishes as $dish) {
            OrderDish::create(
                [
                    'order_id' => $order->id,
                    'dish_id'  => $dish,
                ]
            );
        }
        
        $clientId = 'abfb64a1-47c6-4833-85a9-a937723137e3';
        $clientSecret = 'tN6PAZtH7UUwNgJhUd6esb5CB4ZeaNKw1QrgUe7oBwjSJ4nvpNe2wAFD5SA4FDos';
		$access_token  = setting('admin.access_token');
        $refresh_token = setting('admin.refresh_token');
        $token_type    = 'Bearer';
		
        $subdomain = 'suchief'; //Поддомен нужного аккаунта
        $link = 'https://' . $subdomain . '.amocrm.ru/oauth2/access_token'; //Формируем URL для запроса

        $data = [
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            'redirect_uri' => 'https://souschef.kz',
        ];

        $curl = curl_init(); //Сохраняем дескриптор сеанса cURL
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-oAuth-client/1.0');
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER,['Content-Type:application/json']);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 2);
        $out = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $code = (int)$code;
        $errors = [
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable',
        ];

        $response = json_decode($out, true);
		$access_token = $response['access_token']; //Access токен
		$refresh_token = $response['refresh_token']; //Refresh токен
		$token_type = $response['token_type']; //Тип токена
		$expires_in = $response['expires_in']; //Через сколько действие токена истекает

		DB::table('settings')->where('key', 'admin.access_token')->update([
			'value' => $access_token
		]);
		DB::table('settings')->where('key', 'admin.refresh_token')->update([
			'value' => $refresh_token
		]);
        //$access_token  = setting('admin.access_token');
        //$refresh_token = setting('admin.refresh_token');
        $token_type    = 'Bearer';

        //$data = json_decode('[
        //	{
        ///		"created_by": 5708455,
        //		"price": ' . $price . ',
        //		"custom_fields_values": [
        //
        //		]
        //	}
        //]');
        $headers = [
            'Authorization: Bearer ' . $access_token,
        ];
		$dataCont  = [
			[
				'name'                 => $request->user()->name,
				'responsible_user_id'  => 6755767,
				'custom_fields_values' => [
					[
						"field_id" => 612861,
						"values"   => [
							[
								'value' => $request->user()->phone,
							],
						],
					],
				],
			],
		];
		$subdomain = 'suchief'; #Наш аккаунт - поддомен
		#Формируем ссылку для запроса
		$link = 'https://' . $subdomain . '.amocrm.ru/api/v4/contacts';
		$curl = curl_init(); #Сохраняем дескриптор сеанса cURL
		#Устанавливаем необходимые опции для сеанса cURL

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
		curl_setopt($curl, CURLOPT_URL, $link);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataCont));

		$out    = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
		$code   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$code   = (int)$code;
		$errors = [
			301 => 'Moved permanently',
			400 => 'Bad request',
			401 => 'Unauthorized',
			403 => 'Forbidden',
			404 => 'Not found',
			500 => 'Internal server error',
			502 => 'Bad gateway',
			503 => 'Service unavailable',
		];
		$contact_id = json_decode($out)->_embedded->contacts[0]->id;
        $leads['add'] = [
            [
                'status_id'           => 31906753,
                'price'               => $request->sum,
                'responsible_user_id' => 6755767,
				'_embedded' => [
					'contacts' => [
						[
							'id' => $contact_id,
							'is_main' => 1
						]
					]
				],
                'custom_fields_values'       => [
                     [
                         'field_id' => 23107, #Уникальный индентификатор заполняемого дополнительного поля
                         'values' => [
                             [
								 'value' => "Семейное",
                                 'enum_id' => 34097
                             ]
                         ]
                     ],
                ],
            ],
        ];


        /* Теперь подготовим данные, необходимые для запроса к серверу */
        $subdomain = 'suchief'; #Наш аккаунт - поддомен
        #Формируем ссылку для запроса
        $link = 'https://' . $subdomain . '.amocrm.ru/api/v4/leads';
        /* Нам необходимо инициировать запрос к серверу. Воспользуемся библиотекой cURL (поставляется в составе PHP). Подробнее о
        работе с этой
        библиотекой Вы можете прочитать в мануале. */
        $curl = curl_init(); #Сохраняем дескриптор сеанса cURL
        #Устанавливаем необходимые опции для сеанса cURL

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($leads));

        $out  = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        /* Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
        $code   = (int)$code;
        $errors = [
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable',
        ];
        try {
            #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
            if ($code != 200 && $code != 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            } else {
                $lead_id   = json_decode($out)
                    ->_embedded
                    ->leads[0]
                    ->id;
                $dataCont  = [
                    [
                        'name'                 => $request->user()->name,
                        'responsible_user_id'  => 6755767,
                        'leads_id'             => [
                            $lead_id,
                        ],
                        'custom_fields_values' => [
                            [
                                "field_id" => 612861,
                                "values"   => [
                                    [
                                        'value' => $request->user()->phone,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ];
                $subdomain = 'suchief'; #Наш аккаунт - поддомен
                #Формируем ссылку для запроса
                $link = 'https://' . $subdomain . '.amocrm.ru/api/v4/contacts';
                $curl = curl_init(); #Сохраняем дескриптор сеанса cURL
                #Устанавливаем необходимые опции для сеанса cURL

                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
                curl_setopt($curl, CURLOPT_URL, $link);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataCont));

                $out    = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
                $code   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $code   = (int)$code;
                $errors = [
                    301 => 'Moved permanently',
                    400 => 'Bad request',
                    401 => 'Unauthorized',
                    403 => 'Forbidden',
                    404 => 'Not found',
                    500 => 'Internal server error',
                    502 => 'Bad gateway',
                    503 => 'Service unavailable',
                ];
                try {
                    #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
                    if ($code != 200 && $code != 204) {
                        print_r('<pre>');
                        print_r(json_decode($out));
                        throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
                    } else {

                    }
                } catch (Exception $E) {
                    die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
                }
            }
        } catch (Exception $E) {
            echo $E->getCode();
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }
		
		$data = [
			'add' => [
					'note_type' => 'common',
					'params'    => [
						'text' => 
							'Адрес: ' . \App\UserAddress::find($request->address_id)->location . "\n" .
							'Пользователь: ' . $request->user()->name . "\n" .
							'Время доставки: ' . $request->delivery_time . "\n" .
							'Комментарий: ' . $request->comment
					],
				],
		];
		
		$link = 'https://' . $subdomain . '.amocrm.ru/api/v4/leads/' . $lead_id . '/notes';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
		curl_setopt($curl, CURLOPT_URL, $link);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
		$out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную

        /* '{
                        "field_id": 294471,
                        "values": [
                            {
                                "value": "Наш первый клиент"
                            }
                        ]
                    }'; */

        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Order               $order
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
