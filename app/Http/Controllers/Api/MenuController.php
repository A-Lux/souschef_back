<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Core\Menu\DishHashTag;
use App\Core\Menu\HashTag;
use App\Core\Menu\Week;
use App\Core\Menu\WeekCategory;
use App\Dish;
use App\Filters\WeekFilter;
use App\WeekCategoryDish;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\WeekResource;
use App\Http\Resources\WeekCollection;

class MenuController extends Controller
{
    public function index(Request $request)
    {

        $models = WeekResource::collection(Week::orderBy('id', 'desc')->get());

        return $models;

    }

    public function mobileIndex()
    {
        $weeks = \App\Week::orderBy('id', 'desc')->get();

        foreach ($weeks as $week) {
            $week->categories = Category::whereIn('id', WeekCategory::where('week_id', $week->id)->select('category_id')->get())->get();

            foreach ($week->categories as $category) {
                $category->dishes = Dish::whereIn('id', WeekCategoryDish::where('category_id', $category->id)
                    ->where('week_id', $week->id)
                    ->select('dish_id')
                    ->orderBy('id', 'desc')
                    ->get()
                )
                    ->get();
                foreach ($category->dishes as $dish) {
                    $dish->hashTags = HashTag::find(DishHashTag::where('dish_id', $dish->id)->select('hash_tag_id')->get());
                }
            }
        }

        return response($weeks, 200);
    }
}
