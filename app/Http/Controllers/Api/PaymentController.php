<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function pay(Request $request)
    {
        $request->validate(
            [
                'cryptogram' => 'required',
                'amount'     => 'required',
                'name'       => 'required',
            ]
        );

        $client = new \GuzzleHttp\Client(
            [
                'auth' => [
                    'pk_033af4dd51a25f5da0745ebf874fd',
                    'e4417a08fa2f6a0eeb6ca2be9ced99dc',
                ],
            ]
        );

        $url = 'https://api.cloudpayments.ru/payments/cards/charge';

        $guzzle = $client->request(
            'POST',
            $url, ['form_params' => [
                      'CardCryptogramPacket' => $request->cryptogram,
                      'Amount'               => $request->amount,
                      'name'                 => $request->name,
                      'Currency'             => 'KZT',
                  ]]
        );
		
		$body = json_decode($guzzle->getBody()->getContents());
		
		// Implicitly cast the body to a string and echo it
		//echo $body;
		// Explicitly cast the body to a string
		//$stringBody = (string) $guzzle;
		// Read 10 bytes from the body
		//$tenBytes = $guzzle->read(10);
		// Read the remaining contents of the body as a string
		//$remainingBytes = $guzzle->getContents();


//        $guzzle = $client->request(
//            'GET',
//            'https://www.google.com'
//        );

////        dd($guzzle);
//

       

//        dd($data->Model, $data->Success);
		if($body->Model->ThreeDsCallbackId && $body->Model->AcsUrl && !$body->Success) {
			return response(
				['success' => $body->Success, 'url' => $body->Model], 302
			);
		}
        return response(
            ['success' => $body->Success, 'message' => $body->Model->CardHolderMessage], 200
        );
    }
}
