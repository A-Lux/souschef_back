<?php

namespace App\Http\Controllers\Api\Auth;

use App\Code;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Traits\CodeGenerator;
use App\Traits\GenerateAndSendCode;
use App\Traits\SendSMS;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mobizon\MobizonApi;

class LoginController extends Controller
{
    use GenerateAndSendCode;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function checkCode(Request $request){
        $request->validate([
            'code' => ['required'],
        ]);

        $code = Code::where('code', $request->code)
            ->where('valid', 1)
            ->first();
        $user = User::find($code->user_id);
		return response(['token' => $user->createToken('Laravel Password Grant Client')->accessToken], 200);

        if($code){
            $code->update(['valid' => 0]);
            $user = User::find($code->user_id);
            return response(['token' => $user->createToken('Laravel Password Grant Client')->accessToken], 200);
        }

        return response([], 403);
    }

    public function random()
    {
        $user = User::all()->random();
        return response($user->createToken('Laravel Password Grant Client')->accessToken, 200);
    }
}
