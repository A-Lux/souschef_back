<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function pay(Request $request)
    {
        $request->validate(
            [
                'cryptogram' => 'required',
                'amount'     => 'required',
                'name'       => 'required',
            ]
        );

        $client = new \GuzzleHttp\Client();
        $url    = 'https://api.cloudpayments.ru/payments/cards/charge';

        $myBody['CardCryptogramPacket'] = $request->cryptogram;
        $myBody['Amount']               = $request->amount;
        $myBody['name']                 = $request->name;
        $myBody['Currency']             = 'KZT';


        $request = $client->post(
            $url,
            [
                'body' => $myBody,
                'auth' => [
                    'pk_033af4dd51a25f5da0745ebf874fd',
                    'e4417a08fa2f6a0eeb6ca2be9ced99dc',
                ],
            ]
        );

        $response = $request->send();


        dd($response);
//        return response('OK', 200);
    }
}
