<?php

namespace App\Http\Controllers;

use App\Category;
use App\Dish;
use App\Order;
use App\OrderDish;
use App\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $addresses = UserAddress::where('user_id', $request->user()->id)->get();
        $orders = Order::whereNull('completed_at')
            ->where('user_id', $request->user()->id)
            ->get();
        foreach ($orders as $order) {
            $order->dishes = Dish::whereIn('id',
                    OrderDish::where('order_id', $order->id)
                        ->select('dish_id')
                        ->get()
                )
                ->get();
            $order->image = !empty(json_decode(Dish::find(
                OrderDish::where('order_id', $order->id)->first() ?
                    OrderDish::inRandomOrder()->where('order_id', $order->id)->first()
                    ->dish_id : ''
            ))) ? Voyager::image(json_decode(Dish::find(
                OrderDish::inRandomOrder()->where('order_id', $order->id)->first()
                    ->dish_id
            )->images)[0]) : 0;

            $order->category = Category::find($order->category_id);
        }
        return view('home', compact('addresses', 'orders'));
    }

    public function editAddress(Request $request, $id)
    {
        UserAddress::find($id)->update([
            "location" => $request->location,
            "apartment" => $request->apartment,
            "segment" => $request->segment,
            "floor" => $request->floor,
            "intercom" => $request->intercom,
            "name" => $request->name,
            "phone" => $request->phone,
            "comment" => $request->comment,
        ]);
        if ($request->ajax()) {
            return response([], 200);
        }
        return back();
    }

    public function createAddress(Request $request)
    {
        UserAddress::create([
            "user_id" => Auth::user()->id,
            "location" => $request->location,
            "apartment" => $request->apartment,
            "segment" => $request->segment,
            "floor" => $request->floor,
            "intercom" => $request->intercom,
            "name" => $request->name,
            "phone" => $request->phone,
            "comment" => $request->comment,
        ]);
        if ($request->ajax()) {
            return response([], 200);
        }
        return back();
    }

    public function deleteAddress(Request $request, $id)
    {
        UserAddress::find($id)->delete();
        if ($request->ajax()) {
            return response([], 200);
        }
        return back();
    }

    public function edit(Request $request)
    {
        Auth::user()->update([
            'name' => $request->name
        ]);
        return redirect('/home');
    }

    public function getAddressApi(Request $request)
    {
        $addresses = UserAddress::where('user_id', $request->user()->id)->get();

        //        $orders->
        return response([
            'addresses' => $addresses,
        ], 200);
    }

    public function editAddressApi(Request $request, $id)
    {
        UserAddress::find($id)->update([
            "location" => $request->location,
            "apartment" => $request->apartment,
            "segment" => $request->segment,
            "floor" => $request->floor,
            "intercom" => $request->intercom,
            "name" => $request->name,
            "phone" => $request->phone,
            "comment" => $request->comment,
        ]);
        return response([], 200);
    }

    public function createAddressApi(Request $request)
    {
        UserAddress::create([
            "user_id" => Auth::user()->id,
            "location" => $request->location,
            "apartment" => $request->apartment,
            "segment" => $request->segment,
            "floor" => $request->floor,
            "intercom" => $request->intercom,
            "name" => $request->name,
            "phone" => $request->phone,
            "comment" => $request->comment,
        ]);
        return response([], 200);
    }

    public function deleteAddressApi(Request $request, $id)
    {
        UserAddress::find($id)->delete();
        return response([], 200);
    }

    public function incompleteOrders(Request $request)
    {
        $orders = Order::where('user_id', $request->user()->id)
            ->whereNull('completed_at')->get();
        foreach ($orders as $order) {

            $dishes = Dish::whereIn(
                'id',
                OrderDish::where('order_id', $order->id)
                    ->select('dish_id')
                    ->get()
            )
                ->get();

            $order->dishes = $dishes;

            $order->category = Category::find($order->category_id);
        }
        return response(['orders' => $orders], 200);
    }

    public function history(Request $request)
    {
        $orders = Order::where('user_id', $request->user()->id)
            ->get();
        foreach ($orders as $order) {

            $dishes = Dish::whereIn(
                'id',
                OrderDish::where('order_id', $order->id)
                    ->select('dish_id')
                    ->get()
            )
                ->get();

            $order->dishes = $dishes;

            $order->category = Category::find($order->category_id);
        }
        return response(['orders' => $orders], 200);
    }
}