<?php

namespace App\Http\Controllers;

use App\Category;
use App\IndexBottomSlide;
use App\Question;
use App\Week;
use App\WeekCategoryDish;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function show()
    {
        $slides = IndexBottomSlide::all();
        return view('index', compact('slides'));
    }

    public function questions()
    {
        $questions = Question::orderBy('position')->where('type', 0)->get();
        return view('subscribe', compact('questions'));
    }

    public function contacts()
    {
        $questions = Question::orderBy('position')->where('type', 1)->get();
        return view('contacts', compact('questions'));
    }

    public function choose($week)
    {
        $week_categories = WeekCategoryDish::where(
            'week_id',
            $week
        )
            ->select('category_id')
            ->get();
        return view('choose', [
            'categories' =>
            Category::whereIn('id', $week_categories)
                ->orderBy('position')
                ->get(),
            'week' => $week
        ]);
    }

    public function agreement()
    {
        return view('agreement');
    }
}
