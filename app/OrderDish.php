<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDish extends Model
{
    protected $fillable = ['order_id', 'dish_id'];
}
