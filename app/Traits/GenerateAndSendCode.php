<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait GenerateAndSendCode
{
    use CodeGenerator, SendSMS;

    public function userLogin(Request $request){
        $request->validate([
            'phone' => ['required']
        ]);
        $code = $this->generate($request->phone);

        if($this->sendCode($code, $request->phone)){
            $responce = ['status' => 'success'];
            if($code['first']){
                $responce['first'] = $code['first'];
            }
            return response($responce, 200)->cookie(
                'phone', $request->phone, 360
            );
        }
        else{
            return response(['status' => 'error'], 500);
        }
    }
}
