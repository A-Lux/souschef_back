<?php

namespace App\Traits;

use Carbon\Carbon;
use Mobizon\MobizonApi;

trait SendSMS
{
    public function sendCode($code, $phone)
    {
        //return 1;
//        $api = new MobizonApi(env('MOBIZONE_KEY'), 'api.mobizon.kz');
////        $alphaname = 'SousChef';
//        if ($api->call('message',
//            'sendSMSMessage',
//            array(
//                'recipient' => $phone,
//                'text' => '<#>Ваш код: ' . $code['code']. "\nИстекает " . Carbon::parse($code['expire_at'])->toDateTimeString() . "\nd8bpIZONhS+",
////                'from' => 'SousChef',
//                //Optional, if you don't have registered alphaname, just skip this param and your message will be sent with our free common alphaname.
//            ))
//        ) {
//            $messageId = $api->getData('messageId');
//
//
//            if ($messageId) {
//                $messageStatuses = $api->call(
//                    'message',
//                    'getSMSStatus',
//                    array(
//                        'ids' => array($messageId, '13394', '11345', '4393')
//                    ),
//                    array(),
//                    true
//                );
//
//                if ($api->hasData()) {
//                    $results = [];
//                    foreach ($api->getData() as $messageInfo) {
//                        $results[] = 'Message # ' . $messageInfo->id . " status: " . $messageInfo->status;
//                    }
//                }
//            }
//            return 1;
//        } else {
//            return 0;
//        }
        $text = '<#>Ваш код: ' . $code['code'] . " Истекает " . Carbon::parse($code['expire_at'])
                                                                      ->toDateTimeString() . " d8bpIZONhS+";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://smsc.kz/sys/send.php");
        curl_setopt($ch, CURLOPT_POST, 1);
// In real life you should use something like:
        curl_setopt(
            $ch, CURLOPT_POSTFIELDS, http_build_query(
                   [
                       'mes' => $text,
                       'phones' => $phone,
                       'login' => 'souschef',
                       'psw' => 'souschef1993'
                   ]
               )
        );

// Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        return 1;
    }
}
