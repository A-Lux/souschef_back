<?php

namespace App\Traits;

use App\Code;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait CodeGenerator
{
    public function generate($phone){
        $code = mt_rand(0,9) . mt_rand(0,9) . mt_rand(0,9) . mt_rand(0,9);

        while (!is_null(Code::where('code', $code)
            ->where('valid', 1)
            ->first()))
        {
            $code = mt_rand(0,9) . mt_rand(0,9) . mt_rand(0,9) . mt_rand(0,9);
        }

        $user = User::where('phone', $phone)->first();
        $new = false;
        if(!$user){
            $new = true;
            $user = User::create([
                'name' => '',
                'email' => '',
                'phone'=> $phone
            ]);
        }

        $created = Code::create([
            'code' => $code,
            'user_id' => $user->id
        ]);

        return [
            'first' => $new,
            'code' => $code,
            'expire_at' => $created->created_at->addHours(6)->toDateTimeString()
        ];
    }
}