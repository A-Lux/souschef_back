@extends('layout')

@section('content')
    <div class="account">
        <div class="container">
            <div class="account-inner password-inner">
                <div class="titleDiv">
                    <h1>Мой аккаунт</h1>
                    <p>Забыли свой пароль? Укажите свой Email или имя пользователя. Ссылку на создание нового пароля вы получите по электронной почте.</p>
                </div>
                <div class="account-link">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="account-title">
                                <h3>Имя пользователя или электронная почта</h3>
                            </div>
                            <input type="text">
                        </div>
                    </div>
                </div>
                <button class="button-item">Сбросить пароль</button>
            </div>
        </div>
    </div>
@endsection
