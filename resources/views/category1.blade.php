@extends('layout')

@section('content')
    <div class="menu-inner">
        <div class="container menu-inner-container">
            <script>
                var weeks = {};
                var totalPrice=0;
                var totalPriceSubscription=0;
            </script>
            <div class="slider-nav">
                @foreach($weeks as $week)
                    <div class="menu-slide" id="menu{{ $week->id }}">
                        <script>
                            weeks['{{ $week->id }}'] = {};
                        </script>
                        <h1>Меню на {{ $week->name }}</h1>
{{--                        <p>Доставка этого меню доступна только с 1 по 4 марта </p>--}}
                        <div class="menu-inner-tabs">
                            <ul class="nav nav-pills mb-3 dishes-nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a href="/choose" class="choose-link">Что выбрать?</a>
                                </li>
                                @foreach($week->categories as $category)
                                    <script>
                                    </script>
                                    <li class="nav-item">
                                        <a onclick="$('#change').attr('category', {{ $category->id }}); $('#catCurrent').val({{ $category->id }})"
                                           class="nav-link @if($loop->first) active @endif" id="pill-link{{ $week->id }}cat{{ $category->id }}"
                                           data-toggle="pill"
                                           href="#pill{{ $week->id }}cat{{ $category->id }}"
                                           role="tab"
                                           aria-controls="pill{{ $week->id }}cat{{ $category->id }}"
                                           aria-selected="true">
                                            {{ $category->name }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <form name="checkout" method="post" action="/order">
                                @csrf
                                <div class="menu-tabs-bottom">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-6 col-md-5">
                                                <div class="tabs-left">
                                                    <select
{{--                                                        class="dishes-quantity"--}}
                                                            name="dishesAmount" class="dishes-quantitys" 
                                                            id='dishSelect{{ $week->id }}'
                                                            onchange="selectDish{{ $week->id }}()">
                                                        <option value="3">3 блюда</option>
                                                        <option value="4">4 блюда</option>
                                                        <option selected value="5">5 блюд</option>
                                                        <option value="6">6 блюд</option>
                                                        <option value="7">7 блюд</option>
                                                    </select>
                                                    <select
{{--                                                            class="quantity-person"--}}
                                                            name="personAmount" class="dishes-quantitys">
                                                        <option value="1">1 персона</option>
                                                        <option selected value="2">2 персоны</option>
                                                        <option value="4">4 персоны</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-7">
                                                <div class="tabs-right">
                                                    <div class="total-price">
                                                        <span>0</span>
                                                    </div>
                                                    <div class="expand-icon">
                                                        <span class="expand-btn"> <img src="/image/expand.svg" alt=""></span>
                                                        <div class="expand-wrapper">
                                                            <p>Цена по подписке. Вы можете не оформлять подписку. В таком случае скидка по подписке
                                                                не применяется.</p>
                                                            <div class="priceInfo">
                                                                <span><span id="dishCategoryInfo{{ $week->id }}">{{ $week->id }}">Семейное меню</span> <span id="dishAmountInfo{{ $week->id }}">3</span> блюда</span>
                                                                <span id="totalPriceWithSubscription">0 Т</span>
                                                            </div>
                                                            <div class="priceInfo-total">
                                                                <span>Итого</span>
                                                                <span id="totalPrice{{ $week->id }}">0 ₽</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="dish-button">
                                                        <input id="catCurrent" type="hidden" name="category" value="{{ $weeks[0]->categories[0]->id }}">
                                                        <input id="catCurrent" type="hidden" name="week" value="{{ $week->id }}">
                                                        <button id="change"
                                                                type="button"
                                                                category="{{ $weeks[0]->categories[0]->id }}"
                                                                onclick="$('#comments{{ $week->id }}cat'+$(this).attr('category')).modal('show')">
                                                            <span>
                                                                ИЗМЕНИТЬ СОСТАВ
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <div class="button-loader">
                                                        <button type="submit" id="submitButton{{ $week->id }}cat{{ $category->id }}" disabled>ОФОРМИТЬ ЗАКАЗ</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="menu-tabs-mobile">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xl-6 col-12">
                                            <div class="tabs-right">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="expand-icon">
                                                            <div class="button-loader">
                                                                <a href="checkout.php">ЗАКАЗАТЬ за 5 481 ₽</a>
                                                            </div>
                                                            <a class="btn" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                                <img src="/image/expand.svg" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="dish-button">
                                                            <button> <span>ИЗМЕНИТЬ СОСТАВ</span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-12">
                                            <div class="tabs-left">
                                                <select class="dishes-quantity">
                                                    <option value="">3 блюда</option>
                                                    <option value="">4 блюда</option>
                                                    <option value="">5 блюд</option>
                                                    <option value="">6 блюд</option>
                                                    <option value="">7 блюд</option>
                                                </select>
                                                <select class="quantity-person">
                                                    <option value="">1 персоны</option>
                                                    <option value="">2 персоны</option>
                                                    <option value="">4 персоны</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="collapse" id="collapseExample">
                                                <div class="card card-body">
                                                    <div class="expand-wrapper">
                                                        <p>Цена по подписке. Вы можете не оформлять подписку. В таком случае скидка по подписке
                                                            не применяется.</p>
                                                        <div class="priceInfo">
                                                            <span>Семейное меню 3 блюда</span>
                                                            <span>4 390 ₽</span>
                                                        </div>
                                                        <div class="scissors">
                                                            <span>Ножницы</span>
                                                            <div class="delete-scissors">
                                                                <span>0 ₽</span>
                                                                <span class="close-panel"></span>
                                                            </div>
                                                        </div>
                                                        <div class="priceInfo-total">
                                                            <span>Итого</span>
                                                            <span>3 951 ₽</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide-content">
                            <div class="tab-content" id="pills-tabContent">
                                @foreach($week->categories as $category)
                                    <div class="tab-pane fade show @if($loop->first) active @endif" id="pill{{ $week->id }}cat{{ $category->id }}" role="tabpanel" aria-labelledby="pill{{ $week->id }}cat{{ $category->id }}">
                                    <div class="dishes-list">
                                        <div class="container container-tabs">
                                                <div class="row">
                                                @foreach($category->dishes as $dish)
                                                    <div class="col-xl-4 col-md-6">
                                                        <div class="item-wrapper">
                                                        <a href="/dishes/{{ $category->id }}/{{ $dish->id }}">
                                                                <div class="dishes-image">
                                                                    <img src="{{ !empty(json_decode($dish->images)) ? Voyager::image(json_decode($dish->images)[0]) : '' }}" alt="">
                                                                    <div class="menu-hashtags">
                                                                        @foreach($dish->hashTags as $hashtag)
                                                                            <span>#{{ $hashtag->name }}</span>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div class="about-dishes">
                                                                <div class="dishes-title">
                                                                    <h1>{{ $dish->name }}</h1>
                                                                </div>
                                                                <div class="dishes-date">
                                                                    <span>Готовить на <b>{{ $dish->shelf_life }} день</b></span>
{{--                                                                    <span><img src="/image/check.svg" alt=""></span>--}}
                                                                <!-- <div class="dishes-check"> -->
                                                                <label class="container-check">
                                                                    <input type="checkbox" id="dish{{ $dish->id }}" class='checked-dish' >
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                                <!-- </div> -->
                                                                </div>
                                                                <div class="dishes-icon">
                                                                    <div class="dish-icon">
                                                                        <p><img src="/image/cook.svg" alt="">{{ $dish->weight }} г</p>
                                                                    </div>
                                                                    <div class="dish-icon">
                                                                        <p><img src="/image/time.svg" alt="">{{ $dish->cooking_time }} мин</p>
                                                                    </div>
                                                                    <div class="dish-icon">
                                                                        <p><img src="/image/shef.svg" alt="">{{ $dish->cooking_difficulty }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <script>
                        document.getElementById('dish{{ $dish->id }}').onclick = function() {
                        if(document.getElementById('dish{{ $dish->id }}').checked) {
                            window.localStorage.setItem('dish{{ $dish->id }}', "true");
                            window.localStorage.getItem('{{ $dish->id }}')
                            
                        } else {
                            window.localStorage.setItem('dish{{ $dish->id }}', "false");
                            // window.localStorage.getItem('dishId')
                            window.localStorage.getItem('{{ $dish->id }}')
                        }
                        }
                        
// if (localStorage.getItem('dish{{ $dish->id }}') == "true") {
//   document.getElementById("dish{{ $dish->id }}").setAttribute('checked','checked');
// }
                        function selectDish{{ $week->id }}(){
                        var option = document.getElementById('dishSelect{{ $week->id }}').value-1;
                        var menuSlide = document.getElementById('menu{{ $week->id }}')
                        var tabs = menuSlide.querySelectorAll('.tab-pane');
                        var submitBtn = document.getElementById('submitButton{{ $week->id }}cat{{ $category->id }}');
                        
                        for (let tab of tabs) {
                           var formInput = tab.querySelectorAll('.checked-dish');
                           var checkBox = document.getElementsByClassName('checked-dish');
                            formInput.forEach((input, index) => {
                                if (index <= option) {
                                    input.setAttribute('checked', true);
                                    submitBtn.removeAttribute('disabled');
                                    let checked = tab.querySelectorAll('.checked-dish');
                                    var empty = [].filter.call( checked, function( el ) {
                                    return el.checked
                                    
                                    });
                                    console.log(empty)
                                //     var indexDish = []
                                //     for (let dishes of empty){
                                //         indexs = $(this).attr('id')
                                //         indexs = indexDish
                                //     }
                                //     console.log(indexDish)
                                //     localStorage.setItem('checked', JSON.stringify(empty))
                                //     empty = JSON.parse(localStorage.getItem('empty'))
                                // }
                                }
                                else{
                                    input.removeAttribute('checked');
                                    submitBtn.setAttribute('disabled', true);
                                }
                                
                        })
                        

                        }
                        }
                        </script>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @foreach($weeks as $week)
        @foreach($week->categories as $category)
            <!-- <div class="modal" id="comments{{ $week->id }}cat{{ $category->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content edit-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Изменить продукты</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            @foreach($category->dishes as $dish)
                                <span id="commentName{{ $dish->id }}">{{ $dish->name }}</span>
                                <input type="text" name="comments[{{ $dish->id }}][comment]"/>
                                <input type="hidden" name="comments[{{ $dish->id }}][dish]" value="{{ $dish->id }}" />
                                <br />
                            @endforeach
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Сохранить</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div> -->
        @endforeach
    @endforeach
 <script>
window.onload = function() {
    selectDish{{ $weeks[0]->id }}();
localStorage.getItem('dishId');
// for(var i=0, len=localStorage.length; i<len; i++) {
//     var key = localStorage.key(i);
//     var value = localStorage[key];
//     var checki= document.getElementById(key);
//     if(key.checked){
//         console.log(checki)
//         checki.setAttribute('checked', true)
//     }
//     else{
//         checki.removeAttribute('checked')
//     }
//     localStorage.getItem(key, value)
// }
};
 </script>
@endsection