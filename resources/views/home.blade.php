@extends('layout')

@section('content')
<div class="profile-page">
    <div class="container profile-container">
        <div class="row">
            <div class="col-12 col-xl-4">
                <div class="profile-info-left">
                    <div class="profile-list">
                        <div class="profile-info">
                            <div class="row">
                                <div class="col-xl-9">
                                    <div class="profile-title">
                                        <h1>{{ Auth::user()->name }}</h1>
                                    </div>

                                </div>
                                <div class="col-xl-3">
                                    <div class="profile-edit">
                                        <a href="" class="btn adress-button" data-toggle="modal"
                                            data-target="#profileEdit">
                                            <img src="/image/profile-edit.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="">{{ Auth::user()->phone }}</a>
                        <p>{{ $addresses->isEmpty() ? '' : $addresses[0]->location }}.</p>
                    </div>
                    <div class="list-group profile-list-group" id="list-tab" role="tablist">
                        <div class="tabs-title">
                            <h1>Ваши заказы</h1>
                        </div>
                        <a class="list-group-item profile-group-item list-group-item-action active"
                            id="list-orders-list" data-toggle="list" href="#list-orders" role="tab"
                            aria-controls="orders">Ближайшие заказы <i class="fa fa-chevron-left"
                                aria-hidden="true"></i></a>
                        <div class="tabs-title">
                            <h1>Настройки подписки</h1>
                        </div>
                        {{--                            <a class="list-group-item profile-group-item list-group-item-action" id="list-card-list" data-toggle="list" href="#list-card" role="tab" aria-controls="card">Мои карты<i class="fa fa-chevron-left" aria-hidden="true"></i></a>--}}
                        <a class="list-group-item profile-group-item list-group-item-action" id="list-adress-list"
                            data-toggle="list" href="#list-adress" role="tab" aria-controls="adress">Мои адреса <i
                                class="fa fa-chevron-left" aria-hidden="true"></i></a>
                        <div class="tabs-title">
                            <h1>Полезная информация</h1>
                        </div>
                        <a class="list-group-item profile-group-item list-group-item-action" id="list-terms-list"
                            data-toggle="list" href="#list-terms" role="tab" aria-controls="terms">Условия доставки и
                            оплаты<i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                        <a class="list-group-item profile-group-item list-group-item-action" id="list-agreement-list"
                            data-toggle="list" href="#list-agreement" role="tab"
                            aria-controls="agreement">Пользовательское соглашение <i class="fa fa-chevron-left"
                                aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-8">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-orders" role="tabpanel"
                        aria-labelledby="list-orders-list">
                        @php
                        $half = ceil($orders->count() / 2);
                        $chunks = $orders->chunk($half);
                        if(!isset($chunks[0])){
                        $chunks[0] = [];
                        }
                        if(!isset($chunks[0])){
                        $chunks[1] = [];
                        }
                        @endphp
                        <div class="orders-slider">
                            @foreach($chunks[0] as $order)
                            <div class="order-slide">
                                <div class="orderTimeDiv">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="order-date">
                                                <p>{{ $order->created_at }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="order-time">
                                                <span>{{ $order->delivery_time }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="orders-item">
                                    <div class="order-image">
                                        <img src="{{ isset($order->image) ? $order->image : '' }}" alt="">
                                    </div>
                                    <div class="order-info">
                                        <h1>{{ isset($order->category) ? $order->category->name : '' }}</h1>
                                    </div>
                                    <div class="order-cost">
                                        <span>{{ $order->sum }} ₸</span>
                                    </div>
                                </div>
                                <div class="text-overflow">
                                    <div class="read-overflow">
                                        <ul>
                                            @foreach($order->dishes as $dish)
                                            <li>
                                                {{ $dish->name }}
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="spoiler-icons">
                                        <input type="button" class="show-dishes" value="Посмотреть блюда">
                                        <span class="spoiler-arrow"></span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="orders-slider-bottom">
                            @isset($chunks[1])

                            @foreach($chunks[1] as $order)
                            <div class="order-slide">
                                <div class="orderTimeDiv">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="order-date">
                                                <p>{{ $order->created_at }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="order-time">
                                                <span>{{ $order->delivery_time }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="orders-item">
                                    <div class="order-image">
                                        <img src="{{ isset($order->image) ? $order->image : '' }}" alt="">
                                    </div>
                                    <div class="order-info">
                                        <h1>{{ isset($order->category) ? $order->category->name : '' }}</h1>
                                    </div>
                                    <div class="order-cost">
                                        <span>{{ $order->sum }} ₸</span>
                                    </div>
                                </div>
                                <div class="text-overflow">
                                    <div class="read-overflow">
                                        <ul>
                                            @foreach($order->dishes as $dish)
                                            <li>
                                                {{ $dish->name }}
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="spoiler-icons">
                                        <input type="button" class="show-dishes" value="Посмотреть блюда">
                                        <span class="spoiler-arrow"></span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endisset
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-history" role="tabpanel" aria-labelledby="list-history-list">

                    </div>
                    <div class="tab-pane fade" id="list-card" role="tabpanel" aria-labelledby="list-card-list">
                        <div class="bank-cards">
                            <h1>Банковские карты</h1>
                            <a href="" class="btn adress-button" data-toggle="modal" data-target="#exampleModalCenter">+
                                Добавить карту</a>
                            <div class='modal fade adress-modal' id="exampleModalCenter" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="new-adress">
                                            <h4 class="modal-title">Добавление новой карты</h4>
                                            <p>В целях безопасности мы не храним данные вашей банковской карты</p>
                                            <button class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card-info-body">
                                                <div class="payment-method">
                                                    <div class="card-left">
                                                        <input type="text" class="payment-input"
                                                            placeholder="НОМЕР КАРТЫ">
                                                        <input type="text" class="payment-input"
                                                            placeholder="ИМЯ ВЛАДЕЛЬЦА">
                                                        <div class="payment-info">
                                                            <div class="row">
                                                                <div class="col-xl-6">
                                                                    <div class="control">
                                                                        <input type="text" class="payment-input"
                                                                            placeholder="ММ/ГГ">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-6">
                                                                    <div class="control-image">
                                                                        <img src="../dist/image/pay.png" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-right">
                                                        <div class="card-stripe"></div>
                                                        <div class="card-info">
                                                            <input class="payment-input" type="text"
                                                                placeholder="EMAIL(ДЛЯ ОТПРАВКИ ЧЕКА)">
                                                            <div class="card-info-bottom">
                                                                <input class="payment-input" type="text"
                                                                    placeholder="CVC/CVV">
                                                                <span class="help-icon"> ? </span>
                                                                <div class="tooltip-card">
                                                                    <p> <b>Код</b> проверки подлинности карты (или
                                                                        <b>код CVC*</b>) - это дополнительный код,
                                                                        нанесенный на
                                                                        вашу дебетовую или кредитную карту. Для
                                                                        большинства карт (Visa, MasterCard, банковские
                                                                        карты и т. д.) это
                                                                        последние три цифры числа, напечатанного в поле
                                                                        подписи <b>на обратной стороне карты.</b></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cards-icon">
                                                    <img src="../dist/image/cards.svg" alt="">
                                                </div>
                                                <div class="payment-text">
                                                    <p>Для проверки подлинности карты мы спишем и вернем 1 рубль. Ваша
                                                        карта будет автоматически привязана к профилю, для удобства
                                                        платежей.
                                                        Вы в любой момент можете отвязать или поменять её на другую в
                                                        личном кабинете.</p>
                                                </div>
                                                <div class="add-card-btn">
                                                    <button>Добавить карту</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="security">
                                <p>Это безопасно - наш сайт защищен и постоянно мониторится</p>
                                <img src="../dist/image/cards.svg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-adress" role="tabpanel" aria-labelledby="list-adress-list">
                        <div class="adress-inner">
                            <h1>Адреса доставки</h1>
                            @php
                            $lastIteration = null;
                            @endphp
                            @foreach($addresses as $address)
                            @if($loop->last)
                            @php $lastIteration = $loop->iteration + 1 @endphp
                            @endif
                            <div class="adress-item">
                                <div class="row">
                                    <div class="col-xl-8">
                                        <div class="adress-text">
                                            <p>{{ $address->location }}</p>
                                            <p>{{ $address->phone }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-12">
                                        <div class="adress-edit">
                                            <a href="" data-map="map{{ $loop->iteration }}" class="btn adress-button"
                                                data-toggle="modal" data-target="#modal{{ $address->id }}">Изменить</a>
                                            <div class='modal fade adress-modal' id="modal{{ $address->id }}"
                                                tabindex="-1" role="dialog" aria-labelledby="modal{{ $address->id }}"
                                                aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="new-adress">
                                                            <h4 class="modal-title">Изменение адреса и получателя</h4>
                                                            <p>Укажите точку на карте или заполните поля</p>
                                                            <button class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div id='map{{ $loop->iteration }}'
                                                                style="height: 300px; width: 100%;"
                                                                data-input="adressInput{{ $address->id }}">
                                                                <!-- <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A9dc3b6f34091a35fdbe201b7b8c9dbaff62f1a993049f6a41b5390da179ef54c&amp;source=constructor"
                                                                            width="763" height="300" frameborder="0"></iframe> -->
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="new-adress-info">
                                                                <form action="/address/{{ $address->id }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="row">
                                                                        <div class="col-xl-8">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->location }}"
                                                                                    name="location"
                                                                                    id='adressInput{{ $address->id }}'
                                                                                    placeholder="Город, улица, дом">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-4">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->apartment }}"
                                                                                    name="apartment"
                                                                                    placeholder="Квартира">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-4">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->segment }}"
                                                                                    name="segment"
                                                                                    placeholder="Подъезд">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-4">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->floor }}"
                                                                                    name="floor" placeholder="Этаж">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-4">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->intercom }}"
                                                                                    name="intercom"
                                                                                    placeholder="Домофон">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->name }}"
                                                                                    name="name"
                                                                                    placeholder="Имя получателя">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->phone }}"
                                                                                    name="phone"
                                                                                    placeholder="+7 (905) 724-75-05">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-12">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->comment }}"
                                                                                    name="comment"
                                                                                    placeholder="Комментарий">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-12">
                                                                            <div class="add-adress">
                                                                                <button type="submit">Сохранить
                                                                                    изменения</button>
                                                                                <div class="delete-adress">
                                                                                    <a
                                                                                        href="/address/{{ $address->id }}/delete">Удалить
                                                                                        адрес</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach
                            <a href="" data-map="map{{ $lastIteration }}" class="btn adress-button" data-toggle="modal"
                                data-target="#modalAdress">+ Добавить новый адрес</a>
                            <div class='modal fade adress-modal' id="modalAdress" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalCenterTitle-1" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="new-adress">
                                            <h4 class="modal-title">Добавить новый адрес</h4>
                                            <p>Укажите точку на карте или заполните поля</p>
                                            <button class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div id="map{{ $lastIteration }}" style="height: 300px; width: 100%"
                                                data-input="adressInput{{ $lastIteration }}">
                                                <!-- <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A9dc3b6f34091a35fdbe201b7b8c9dbaff62f1a993049f6a41b5390da179ef54c&amp;source=constructor"
                                                            width="763" height="300" frameborder="0"></iframe> -->
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <form action="/address" method="post">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-xl-8">
                                                        <div class="new-adress-input">
                                                            <input type="text" value="" name="location"
                                                                id='adressInput{{ $lastIteration }}'
                                                                placeholder="Город, улица, дом">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <div class="new-adress-input">
                                                            <input type="text" value="" name="apartment"
                                                                placeholder="Квартира">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <div class="new-adress-input">
                                                            <input type="text" value="" name="segment"
                                                                placeholder="Подъезд">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <div class="new-adress-input">
                                                            <input type="text" value="" name="floor" placeholder="Этаж">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <div class="new-adress-input">
                                                            <input type="text" value="" name="intercom"
                                                                placeholder="Домофон">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="new-adress-input">
                                                            <input type="text" value="" name="name"
                                                                placeholder="Имя получателя">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="new-adress-input">
                                                            <input type="text" value="" name="phone"
                                                                placeholder="+7 (905) 724-75-05">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12">
                                                        <div class="new-adress-input">
                                                            <input type="text" value="" name="comment"
                                                                placeholder="Комментарий">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12">
                                                        <div class="add-adress">
                                                            <button type="submit">Сохранить изменения</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-terms" role="tabpanel" aria-labelledby="list-terms-list">
                        {!! setting('profil.payment') !!}
                    </div>
                    <div class="tab-pane fade" id="list-agreement" role="tabpanel"
                        aria-labelledby="list-agreement-list">
                        {!! setting('profil.agreement') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='modal fade adress-modal' id="profileEdit" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="new-adress">
                <h4 class="modal-title">Редактировать данные</h4>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <form action="/profile" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="new-adress-input">
                                <input type="text" value="{{ Auth::user()->phone }}" name="name"
                                    placeholder="Имя получателя">
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="new-adress-input">
                                <input disabled type="text" value="{{ Auth::user()->phone }}" name="phone"
                                    placeholder="+7 (905) 724-75-05">
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="add-adress">
                                <button type="submit">Сохранить изменения</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection