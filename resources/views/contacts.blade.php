@extends('layout')

@section('content')
<div class="contact-page">
    <div class="container">
        <div class="contact-title title">
            <h1>Контакты</h1>
        </div>
    </div>
</div>
@php
    $half = ceil($questions->count() / 2);
    $chunks = $questions->chunk($half);
@endphp
<div class="contact">
    <div class="container">
        <div class="contact-info">
            <div class="row contact-row">
                <div class="col-xl-4 col-md-4">
                    <div class="contact-link">
                        <a href="" class="contact-icons"><i class="fas fa-phone"></i></a>
                        <a href="" class="contact-item">{{ setting('site.phone') }}</a>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="contact-link">
                        <a href="" class="contact-icons"><i class="far fa-envelope"></i></a>
                        <a href="" class="contact-item">{{ setting('site.email') }}</a>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="contact-link">
                        <a href="" class="contact-icons"><i class="fas fa-map-marker-alt"></i></a>
                        <a href="" class="contact-item">{{ setting('site.address') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="questions">
    <div class="container">
        <div class="questions-title">
            <h2>Появились вопросы? Мы с радостью ответим на них! 🙂</h2>
        </div>
        <div class="questions-image">
            <img src="image/contact-2.png" alt="">
        </div>
        <div class="questions-content">
            <div class="row">
                @foreach($chunks as $questions)
                <div class="col-xl-6">
                    @foreach($questions as $question)
                        <div class="question-inner">
                        <div class="question-btn">
                            <button class="btn sub-btn" type="button" data-toggle="collapse" data-target="#collapseExample{{ $question->id }}" aria-expanded="false" aria-controls="collapseExample{{ $question->id }}">
                                {{ $question->title }}
                            </button>
                            <i class="fas fa-caret-right arrow-right"></i>
                        </div>
                        <div class="collapse" id="collapseExample{{ $question->id }}">
                            <div class="card card-body question-body">
                                {!! $question->answer !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
