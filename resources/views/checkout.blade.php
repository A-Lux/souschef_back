@extends('layout')

@section('content')
<style>
	select {
	  display: block;
	  font-size: 16px;
	  font-family: sans-serif;
	  font-weight: 700;
	  color: #444;
	  line-height: 1.3;
	  padding: .6em 1.4em .5em .8em;
	  width: 100%;
	  max-width: 100%; /* useful when width is set to anything other than 100% */
	  box-sizing: border-box;
	  margin: 0;
	  border: 1px solid #aaa;
	  box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
	  border-radius: .5em;
	  -moz-appearance: none;
	  -webkit-appearance: none;
	  appearance: none;
	  background-color: #fff;
}
/* Hide arrow icon in IE browsers */
select-css::-ms-expand {
  display: none;
}
/* Hover style */
select:hover {
  border-color: #888;
}
/* Focus style */
select:focus {
  border-color: #aaa;
  /* It'd be nice to use -webkit-focus-ring-color here but it doesn't work on box-shadow */
  box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
  box-shadow: 0 0 0 3px -moz-mac-focusring;
  color: #222;
  outline: none;
}

/* Set options to normal weight */
select option {
  font-weight:normal;
}

/* Support for rtl text, explicit support for Arabic and Hebrew */
*[dir="rtl"] select, :root:lang(ar) select, :root:lang(iw) select {
  background-position: left .7em top 50%, 0 0;
  padding: .6em .8em .5em 1.4em;
}

/* Disabled styles */
select:disabled, .select-css[aria-disabled=true] {
  color: graytext;
    linear-gradient(to bottom, #ffffff 0%,#e5e5e5 100%);
}

select:disabled:hover, .select-css[aria-disabled=true] {
  border-color: #aaa;
}
</style>
<script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
<div class="checkout-page">
    <div class="container checkout-container">
        <div class="back-choose">
            @php
                $link = '/week/' . \App\Week::orderBy('created_at', 'desc')->first()->id;
            @endphp
            <a href="{{ $link }}"> <span class="back-icon"></span> К выбору меню</a>
        </div>
        <form name="send" method="post" action="/store">
            @csrf
            @foreach($dishes as $dish)
            <input type="hidden" name="dishes[]" value="{{ $dish->id }}" />
            @endforeach
            <input type="hidden" name="sum" value="{{ session()->has('price') ? session()->get('price') : 0 }}" />
            <input type="hidden" name="persons" value="{{   $persons }}" />
            <input type="hidden" name="subscribe" value="0" />
            <input type="hidden" name="address_id" value="3" />
            <div class="checkout-panel">
                <div class="row">
                    <div class="col-xl-8">
                        <div class="checkout-left">
                            <h1>Адрес и время доставки</h1>
                            <div class="adress-form">
                                <div class="row">
                                    <div class="col-xl-8 col-md-7">
                                        <div class="adress-select">
                                            <select name="address_id" name="state" id='openmodal'>
                                                <option
													selected
													disabled
												>
													Выберите адрес
												</option>
                                                @foreach($addresses as $address)
													<option
														id="adress{{ $address->id }}"
														value="{{ $address->id }}"
													>
														{{ $address->location }}
													</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-5">
                                        <div class="adress-change">
                                            <a
											   href=""
											   id="switcher"
											   class="btn adress-button"
											   data-toggle="modal"
                                               data-target="">
												Изменить адрес получателя
											</a>
                                            @foreach($addresses as $address)
                                            <div class='modal fade adress-modal' id="modal{{ $address->id }}"
                                                tabindex="-1" role="dialog" aria-labelledby="modal{{ $address->id }}"
                                                aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="new-adress">
                                                            <h4 class="modal-title">Изменение адреса получателя</h4>
                                                            <p>Укажите точку на карте или заполните поля</p>
                                                            <button type="button" class="close"
                                                                data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div id='map{{ $address->id }}' style="height: 300px; width: 100%;" data-input="adressInput{{ $address->id }}">
                                                                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A9dc3b6f34091a35fdbe201b7b8c9dbaff62f1a993049f6a41b5390da179ef54c&amp;source=constructor"
                                                                        width="763" height="300" frameborder="0"></iframe>

                                                            </div>
                                                            <div id='coords'></div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="new-adress-info">
                                                                <form action="/address/{{ $address->id }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="row">
                                                                        <div class="col-xl-8">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->location }}"
                                                                                    id='adressInput{{ $address->id }}' name="location"
                                                                                    placeholder="Город, улица, дом">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-4">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->apartment }}"
                                                                                    name="apartment"
                                                                                    placeholder="Квартира">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-4">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->segment }}"
                                                                                    name="segment"
                                                                                    placeholder="Подъезд">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-4">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->floor }}"
                                                                                    name="floor" placeholder="Этаж">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-4">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->intercom }}"
                                                                                    name="intercom"
                                                                                    placeholder="Домофон">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->name }}"
                                                                                    name="name"
                                                                                    placeholder="Имя получателя">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->phone }}"
                                                                                    name="phone"
                                                                                    placeholder="+7 (905) 724-75-05">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-12">
                                                                            <div class="new-adress-input">
                                                                                <input type="text"
                                                                                    value="{{ $address->comment }}"
                                                                                    name="comment"
                                                                                    placeholder="Комментарий">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-12">
                                                                            <div class="add-adress">
                                                                                <button type="submit">Сохранить
                                                                                    изменения</button>
                                                                                <div class="delete-adress">
                                                                                    <a
                                                                                        href="/address/{{ $address->id }}/delete">Удалить
                                                                                        адрес</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-xl-7 col-md-7">
                                        <div class="delivery">
                                            <select>
                                                <option
													selected
													disabled
												>
													День недели
												</option>
                                                <option>
													Суббота
												</option>
                                                <option>
													Воскресенье
												</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-md-5">
                                        <div class="time-delivery">
                                            <select name="delivery_time">
                                                <option selected disabled>Интервал доставки</option>
                                                @php
                                                    $arr = [18,20];
                                                @endphp
                                                @foreach($arr as $time)
                                                    <option>
                                                        {{ $time }}:00 - {{ $time + 2 }}:00
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="info-cardowner">
                                <div class="row">
                                    {{--                                <div class="col-xl-6 col-md-6">--}}
                                    {{--                                    <input type="text" class="payment-input" placeholder="Екатерина">--}}
                                    {{--                                </div>--}}
                                    {{--                                <div class="col-xl-6 col-md-6">--}}
                                    {{--                                    <input type="text" class="payment-input" placeholder="+7 (905) 724-75-05">--}}
                                    {{--                                </div>--}}
                                    <div class="col-xl-12">
                                        <input type="text" name="comment" class="payment-input"
                                            placeholder="КОММЕНТАРИЙ К ЗАКАЗУ">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="checkout-right">
                            <div class="checkout-menu-list">
                                <div class="row">
                                    <div class="col-xl-8 col-8">
                                        <div class="dishes-list">
                                            <h1>Заказанные блюда</h1>
                                            <span>{{ $dishes ? $dishes->count() : 0 }} блюд , {{ $persons }}
                                                персоны</span>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-4">
                                        <div class="dishes-cost">
                                            <span>{{ session()->has('price') ? session()->get('price') : 0 }} тенге</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-overflow">
                                    <div class="read-overflow">
                                        <ul>
                                            @foreach($dishes as $dish)
                                            <li>
                                                {{ $dish->name }}
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="spoiler-icons">
                                        <input type="button" class="show-dishes" value="Посмотреть блюда">
                                        <span class="spoiler-arrow"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="checkout-menu-list scissors">
                                <div class="row">
                                    {{--                                <div class="col-xl-9 col-9">--}}
                                    {{--                                    <div class="dishes-list">--}}
                                    {{--                                        <h1>Ножницы</h1>--}}
                                    {{--                                        <span>Подарок</span>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </div>--}}
                                    <div class="col-xl-3 col-3">
                                        <div class="dishes-cost">
                                            <span>{{ session()->has('price') ? session()->get('price') : 0 }} тенге</span>
                                            <span class="close-panel"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {{-- <div class="checkout-menu-list">
                                <div class="price-inner">
                                    <div class="price-subscription">

                                        <div class="price-title">
                                            <div class="row">
                                                <div class="col-xl-9 col-9 pr-0">
                                                    <h1>Цена по подписке</h1>
                                                    <p>Активируйте подписку и получайте скидку 10 % на меню.
                                                        <a href="" class="btn btn-primary quest-btn" data-toggle="modal"
                                                            data-target="#exampleModalCenter">
                                                            Подробнее</a></p>
                                                </div>
                                                <div class="col-xl-3 col-3 p-0">
                                                    <span>{{  session()->has('price') ? session()->get('price') * 0.9 : 0 }}
                                                        тенге</span>
                                                </div>
                                                <div class="col-xl-12">
                                                    <p>Для активации подписки, необходимо скачать приложение</p>
                                                </div>
                                                <div class="col-xl-6">
                                                    <a href=""><img src="image/google-play.svg" alt=""></a>
                                                </div>
                                                <div class="col-xl-6">
                                                    <a href=""><img src="image/app-store.svg" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered question-modal"
                                                role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h2>Как работает подписка</h2>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @foreach($questions as $question)
                                                            <div class="question">
                                                                <span class="question-arrow arrow-left"><img
                                                                        src="/image/collapse-arrow.svg" alt=""></span>

                                                                <div class="questions">
                                                                    <a class="btn btn-primary quest-button "
                                                                        data-toggle="collapse" href="#collapse{{ $question->id }}"
                                                                        role="button" aria-expanded="false"
                                                                        aria-controls="collapse">
                                                                        {{ $question->title }}
                                                                    </a>
                                                                    <div class="collapse" id="collapse{{ $question->id }}">
                                                                        <div class="card card-body">
                                                                            {!! $question->answer !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="price">
                                <div class="price-title">
                                    <div class="row">
                                        <div class="col-xl-9 col-9 pr-0">
                                            <h1>Цена без подписки</h1>
                                        </div>
                                        <div class="col-xl-3 col-3 p-0">
                                            <span>{{ session()->has('price') ? session()->get('price') :  0 }} тенге</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="checkout-list-bottom">
                        <div class="checkout-btn">
                            <button type="button" onclick="pay();" id="issue-btn">ОФОРМИТЬ ЗАКАЗ</button>
                        </div>
                        <div class="chexbox-wrapper">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox" id="checkbox">
                                <p> Выбирая способ оплаты, вы даете согласие ИП «СуШЕФ»
                                    на <a href="agreement.php"> обработку персональных данных </a>, а также
                                    подтверждаете, что ознакомились с <a href="agreement.php"> пользовательским
                                        соглашением</a></p>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</form>
</div>
</div>
<script>
    this.pay = function () {
        var widget = new cp.CloudPayments();
        widget.charge({ // options
                publicId: 'pk_033af4dd51a25f5da0745ebf874fd', //id из личного кабинета
                description: 'Пример оплаты (деньги сниматься не будут)', //назначение
                amount:
                    {{
                        session()->get('price') ? session()->get('price') : 0
                    }}
                , //сумма
                currency: 'KZT', //валюта
                // invoiceId: '1234567', //номер заказа  (необязательно)
                // accountId: 'user@example.com', //идентификатор плательщика (необязательно)
                skin: "classic", //дизайн виджета
                // data: {
                //     myProp: 'myProp value' //произвольный набор параметров
                // }
            },
            function (options) { // success
                document.forms.send.submit();
            },
            function (reason, options) { // fail
                //действие при неуспешной оплате
            });
    };





    // console.log(options);
</script>
@endsection
