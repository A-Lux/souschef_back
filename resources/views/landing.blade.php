<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    <title>Сушеф</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=PT+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link href="landing/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>

    <section id="sec_one">
        
        <!-- desktop -->
        <div class="d-none d-lg-block">
            <nav class="navbar navbar-expand-md mt-2">
                <div class="container-fluid sec_one_mar">
                    <a class="navbar-brand scroll_menu" href="#sec_one"><img src="landing/img/logo2.svg"></a>
                   
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                        <ul class="navbar-nav mr-auto ml-5">
<!--
                            <li class="nav-item">
                                <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#sec_one">Стартовый <span class="sr-only">(current)</span></a>
                            </li>
-->
                            <li class="nav-item">
                                <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#eat_post_sec">Блюда</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#sec_five">Вопросы</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#sec_four">Отзывы</a>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link white upper nav_size nav_mar_right phone_header" href="tel:+77776868707">+7 777 686 87 07</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://wa.me/77776868707?text=Добрый день! Хотим сделать заказ" target="_blank"><button class="btn btn_white_order upper" type="submit">Заказать пробный сет</button></a>
                            </li>

                        </ul>


                    </div>
                </div>
            </nav>
        </div>
        <!-- end desktop -->
        
        <!-- mobile -->
        <div class="d-md-none d-xs-block">
            
            <nav class="navbar navbar-expand-md fixed-top navbar-light bg-white" style="border-bottom: 1px solid #7EC25A">
                <div class="container-fluid sec_one_mar">
                    <a class="navbar-brand scroll_menu"  href="#sec_one"><img src="landing/img/logo_black.png" width="50px"></a>
                     <a class=" upper" href="tel:+77776868707" style="font-size: 16px; color:#7EC25A; ">+7 777 686 87 07</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation" style="color:#fff; background: #7EC25A; outline: 0; font-size: 20px">
                        <span>≡</span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                        <ul class="navbar-nav mr-auto ml-5 text-right">
                            
                            <li class="nav-item" style="border-bottom: 1px solid #7EC25A;">
                                <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#eat_post_sec">Блюда</a>
                            </li>
                            <li class="nav-item" style="border-bottom: 1px solid #7EC25A;">
                                <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#sec_five">Вопросы</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#sec_four">Отзывы</a>
                            </li>
                        </ul>
                       

                    </div>
                </div>
            </nav>
        </div>
        <!-- end mobile -->

        <hr class="line_nav">

        <div class="container pt-140">
            <div class="row">
                <div class="col-lg-12 col-12 col-md-12 text-center">
                    <img src="landing/img/logo.png" class="img_logo">
                    <h2 class="upper white title_sec_one mt-5">{{setting('main.heading_one')}}</h2>

                    <p class="mini_text_title white mb-5">{!! setting('main.text_one') !!}</p>


                    <a href="https://wa.me/77776868707?text=Добрый день! Хотим сделать заказ" target="_blank"><button class="btn btn_white sec_one_btn my-2 my-sm-0 upper" type="submit">{!! setting('main.button_text_one') !!}</button></a>
                </div>

                <div class="col-lg-12 col-12 col-md-12 text-center">
                    <a href="#sec_two" class="scroll_menu"><img src="landing/img/arrow_down.svg" class="sec_one_arrowdown"></a>
                </div>
            </div>
        </div>

    </section>

    <section id="sec_two" class="sec_two_mar">
        <!--desktop -->
        <div class="container-fluid d-none d-lg-block">
            <div class="row text-center">
                <div class="col-lg-3 col-md-6 col-6">
                    <img src="landing/img/1_webpage.png" class="sec_two_img">
                </div>
                <div class="col-lg-3 col-md-6 col-6">
                    <img src="landing/img/2_food-delivery.png" class="sec_two_img">
                </div>
                <div class="col-lg-3 col-md-3 col-6">
                    <img src="landing/img/3_smartwatch.png" class="sec_two_img">
                </div>
                <div class="col-lg-3 col-md-6 col-6">
                    <img src="landing/img/4_calendar.png" class="sec_two_img">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 text-center">
                    <img src="landing/img/line.svg" width="80%" class="mt-5">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-6 col-12">
                    <p class="text_sec_two">{!! setting('main.square_one_title') !!}</p>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <p class="text_sec_two">{!! setting('main.square_two_title') !!}</p>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <p class="text_sec_two">{!! setting('main.square_three_title') !!}</p>

                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <p class="text_sec_two">{!! setting('main.square_four_title') !!}</p>
                </div>
            </div>
        </div>
        <!-- end Desktop -->

        <!-- tablet -->
        <div class="container-fluid d-lg-none d-none d-md-block">
            <div class="row text-center">

                <div class="col-lg-3 col-md-6 col-6">
                    <img src="landing/img/1_webpage.png" class="sec_two_img">
                    <p class="text_sec_two">Выбирайте<br>блюда из меню</p>

                </div>
                <div class="col-lg-3 col-md-6 col-6">
                    <img src="landing/img/2_food-delivery.png" class="sec_two_img">
                    <p class="text_sec_two">Получайте<br>свежие продукты</p>
                </div>

                <div class="col-md-12 mt-3"><img src="landing/img/01.svg" width="65%" class="mb-5"></div>
                <div class="col-lg-3 col-md-6 col-6">
                    <img src="landing/img/3_smartwatch.png" class="sec_two_img">
                    <p class="text_sec_two">Готовьте<br>за 15-40 минут</p>

                </div>
                <div class="col-lg-3 col-md-6 col-6">
                    <img src="landing/img/4_calendar.png" class="sec_two_img">
                    <p class="text_sec_two">Настройте<br>график доставок</p>
                </div>

                <div class="col-md-12 mt-3"><img src="landing/img/02.svg" width="65%" class="mb-5"></div>
            </div>

        </div>
        <!-- end tablet -->

        <!-- mobile-->
        <div class="container-fluid d-md-none d-xs-block">
            <div class="row text-center">

                <div class="col-12">
                    <!--                    <div class="mt-3 mb-3"><img src="landing/img/st_1.svg" width="80px"></div>-->
                    <img src="landing/img/1_webpage.png" class="sec_two_img">

                    <p class="text_sec_two mt-3">Выбирайте<br>блюда из меню</p>

                </div>
                <div class="col-12 mt-2">
                    <div class="mt-3 mb-3"><img src="landing/img/st_1.svg" width="80px"></div>
                    <img src="landing/img/2_food-delivery.png" class="sec_two_img">

                    <p class="text_sec_two mt-3">Получайте<br>свежие продукты</p>

                </div>
                <div class="col-12 mt-2">
                    <div class="mt-3 mb-3"><img src="landing/img/st_2.svg" width="80px"></div>
                    <img src="landing/img/3_smartwatch.png" class="sec_two_img">

                    <p class="text_sec_two mt-3">Готовьте<br>за 15-40 минут</p>

                </div>
                <div class="col-12 mt-2">
                    <div class="mt-3 mb-3"><img src="landing/img/st_3.svg" width="80px"></div>
                    <img src="landing/img/4_calendar.png" class="sec_two_img">

                    <p class="text_sec_two mt-3">Настройте<br>график доставок</p>
                    <div class="mt-3 mb-3"><img src="landing/img/st_4.svg" width="80px"></div>
                </div>
            </div>


        </div>
        <!-- end mobile-->
    </section>
    <div class="bg_eat_rel">
        <img src="landing/img/eat_bg_1.png" class="bg_eat_1">
    </div>
    <section id="eat_post_sec">
        <div class="container-fluid">
            <div class="row mt-5">
                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
                    <h3 class="upper title_sec_two">{{setting('main.text_two')}}</h3>
                    <p class="mini_text_title_two green mb-5">{{setting('main.heading_two')}}</p>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12" align="center">

                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active post_block">
                                            <div class="row pb-5">
                                                <div class="col-lg-12 pb-5">

{{--                                                    <img src="landing/img/eat_post.jpg" class="w-100">--}}
                                                    <img src="{{ Voyager::image(setting('main.recipe-one-image')) }}" />
                                                    <img src="" alt="">
                                                    <div class="bg_eat_rel">
                                                        <span class="blur_btn post_category">{!! setting('main.recipe-one-kitchen') !!}</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-3">
                                                    <div class="bg_eat_rel">
                                                        <p class="post_date_text">{!! setting('main.recipe-one-length') !!}</p>
                                                    </div>
                                                    <img src="landing/img/calendar.svg" class="post_calendar">
                                                </div>
                                                <div class="col-lg-7 col-9">
                                                    <p class="post_text">{!! setting('main.recipe-one-text') !!}</p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 col-12 border_l">
                                                    <p class="post_details green">{!! setting('main.recipe-one-time') !!}</p>
                                                    <p class="post_details">{!! setting('main.recipe-one-kkcal') !!}</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="carousel-item post_block">
                                            <div class="row pb-5">
                                                <div class="col-lg-12 pb-5">
{{--                                                    <img src="landing/img/eat_post.jpg" class="w-100">--}}

                                                    <img src="{{ Voyager::image(setting('main.recipe-two-image')) }}" />
                                                    <div class="bg_eat_rel">
                                                        <span class="blur_btn post_category">{!! setting('main.recipe-two-kitchen') !!}</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-3">
                                                    <div class="bg_eat_rel">
                                                        <p class="post_date_text">{!! setting('main.recipe-two-length') !!}</p>
                                                    </div>
                                                    <img src="landing/img/calendar.svg" class="post_calendar">
                                                </div>
                                                <div class="col-lg-7 col-9">
                                                    <p class="post_text">{!! setting('main.recipe-two-text') !!}</p>
                                                </div>
                                                <div class="col-lg-3 col-md-12 col-12 border_l">
                                                    <p class="post_details green">{!! setting('main.recipe-two-time')
 !!}</p>
                                                    <p class="post_details">{!! setting('main.recipe-two-kkcal') !!}</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="d-none d-lg-block">
                                        <div class="col-12 mt-5 text-center">
                                            <a class="post_control_left" href="#carouselExampleControls" role="button" data-slide="prev">
                                                <i class="controls class-fade"><img src="landing/img/left_arrow.svg"></i>
                                            </a>
                                            <a class="post_control_right" href="#carouselExampleControls" role="button" data-slide="next">
                                                <i class="controls class-active"><img src="landing/img/rigth_arrow.svg"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class=" d-lg-none d-xs-block">
                                        <div class="col-12 mt-5 text-center" style="position: relative;">
                                            <a href="#carouselExampleControls" role="button" data-slide="prev">
                                                <i class="controls class-fade"><img src="landing/img/left_arrow.svg" class="post_control_left_mob"></i>
                                            </a>
                                            <a href="#carouselExampleControls" role="button" data-slide="next">
                                                <i class="controls class-active"><img src="landing/img/rigth_arrow.svg" class="post_control_right_mob"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section>
    <div class="bg_eat_rel">
        <img src="landing/img/eat_bg_2.png" class="bg_eat_2">
    </div>

    <section id="sec_three" class="sec_two_mar">
        <div class="bg_eat_rel">
            <img src="landing/img/eat_bg_3.png" class="bg_eat_3">
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
                    <p class="mini_text_title_two green">{!! setting('main.square_one_text') !!}</p>
                    <h3 class="upper title_sec_three">{!! setting('main.square_two_text') !!}</h3>
                </div>
                <div class="col-lg-12 col-md-12 col-12 text-center mt-3">
                    <a href="https://wa.me/77776868707?text=Добрый день! Хотим сделать заказ" target="_blank"><button class="btn btn_green relative my-2 my-sm-0 upper" type="submit">Заказать со скидкой 33%</button></a>
                </div>

                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
{{--                    <img src="landing/img/box.jpg" class="box_img mt-3">--}}

                    <img src="{{ Voyager::image(setting('main.image-box')) }}"  alt=""/>

                </div>
            </div>

            <div class="row mt-5">
                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
                    <h3 class="upper title_sec_three mt-5">{!! setting('main.text-after-images') !!}</h3>
                    <p class="mini_text_title_two green">{!! setting('main.podzagolovk-after-images-text') !!}</p>
{{--                    <p class="mini_text_title_two green">Оставьте телефон, и мы предложим другие варианты<br class="d-none d-lg-block"> меню с выгодной скидкой</p>--}}
                </div>

                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
                    <div class="">


                        <a href="tel:+77776868707"><button class="bg_eat_rel btn btn_green relative my-2 my-sm-0 upper" type="submit"><img class="phone_pos" src="landing/img/phone.png">Позвоните</button></a>
                    </div>
                </div>
            </div>
        </div>

    </section>


    <section id="sec_four">
        <div class="bg_eat_rel">
            <img src="landing/img/eat_bg_4.png" class="bg_eat_4">
        </div>
        <div class="bg_eat_rel">
            <img src="landing/img/eat_bg_5.png" class="bg_eat_5">
        </div>
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
                    <h3 class="upper title_sec_three">ОТЗЫВЫ</h3>
                    <p class="mini_text_title_two green">Средняя оценка наших блюд 4,9</p>
                </div>

                <div class="col-lg-12 col-md-12 col-12 mt-5">

                    <div id="reviewcontrols" class="carousel slide mar_sec_four" data-ride="carousel">

                        <div class="carousel-inner">
                            @foreach($data as $review)
                            <div class="carousel-item active ">
                                <div class="row pb-5">
                                    <div class="col-lg-4 col-md-12 col-12" align="center">
                                        <div class="review_block">

                                            <div><img src="{{ Voyager::image($review->review_image) }}" /></div>
                                            <div align="center" class="mt-5"><img src="{{ Voyager::image($review->star_rating) }}" /></div>
                                            <p class="review_text mt-3">{!! $review->comment !!}</p>

                                            <div class="row mt-5">
                                                <div class="col-lg-2 col-2">
{{--                                                    <img src="landing/img/review_author_1.png">--}}

                                                </div>

                                                <div class="col-lg-10 col-10 text-left">
                                                    <div class="instagram_review">
                                                        <span class="review_name">{{$review->name}}</span><br>
                                                        <span class="insta_nik">{{$review->username}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
{{--                                    <div class="col-lg-4 col-md-12 col-12" align="center">--}}
{{--                                        <div class="review_block">--}}

{{--                                            <div><img src="landing/img/review_1.png" class="review_img"></div>--}}
{{--                                            <div align="left" class="mt-5"><img src="landing/img/5_stars.svg"></div>--}}
{{--                                            <p class="review_text mt-3">Не думала раньше, что смогу так сильно полюбить готовить? Оказалось, что я просто ненавидела ходить в супермаркеты и собирать блюда по рецепту? Теперь вечера с дочей проходят намного интереснее и вкуснее? Спасибо су-шефу!</p>--}}

{{--                                            <div class="row mt-5">--}}
{{--                                                <div class="col-lg-2 col-2">--}}
{{--                                                    <img src="landing/img/review_author_1.png">--}}
{{--                                                </div>--}}

{{--                                                <div class="col-lg-10 col-10 text-left">--}}
{{--                                                    <div class="instagram_review">--}}
{{--                                                        <span class="review_name">Irina Nikolaeva</span><br>--}}
{{--                                                        <span class="insta_nik">@iroka_i</span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}


{{--                                    <div class="col-lg-4 col-md-12 col-12" align="center">--}}
{{--                                        <div class="review_block_center">--}}

{{--                                            <div><img src="landing/img/review_2.png" class="review_img"></div>--}}
{{--                                            <div align="left" class="mt-5"><img src="landing/img/5_stars.svg"></div>--}}
{{--                                            <p class="review_text mt-3">Больше всего в сервисе "СуШЕФ" мне нравится свобода от принятия незначимых решений. Больше не нужно питаться рецептами из интернета и постоянно искать (а то, что их миллион, только усложняет выбор), заранее заказывать под ...</p>--}}

{{--                                            <div class="row mt-5">--}}
{{--                                                <div class="col-lg-2 col-2">--}}
{{--                                                    <img src="landing/img/review_author_2.png">--}}
{{--                                                </div>--}}

{{--                                                <div class="col-lg-10 col-10 text-left">--}}
{{--                                                    <div class="instagram_review">--}}
{{--                                                        <span class="review_name">Sultan Aida</span><br>--}}
{{--                                                        <span class="insta_nik">@sultanaida</span>--}}

{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}


{{--                                        </div>--}}
{{--                                    </div>--}}


{{--                                    <div class="col-lg-4 col-md-12 col-12" align="center">--}}
{{--                                        <div class="review_block">--}}

{{--                                            <div><img src="landing/img/review_3.png" class="review_img"></div>--}}
{{--                                            <div align="left" class="mt-5"><img src="landing/img/5_stars.svg"></div>--}}
{{--                                            <p class="review_text mt-3">Открыла для себя классный сервис по доставке продуктов с рецептами "СуШЕФ"! Это не реклама, а просто рекомендация! Хотя какая это может быть от меня реклама с моими 100 подписчиками ????? Теперь даже из меня получается отличный шеф ...</p>--}}

{{--                                            <div class="row mt-5">--}}
{{--                                                <div class="col-lg-2 col-2">--}}
{{--                                                    <img src="landing/img/review_author_1.png">--}}
{{--                                                </div>--}}

{{--                                                <div class="col-lg-10 col-10 text-left">--}}
{{--                                                    <div class="instagram_review">--}}
{{--                                                        <span class="review_name ">Irina Nikolaeva</span><br>--}}
{{--                                                        <span class="insta_nik">@iroka_i</span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}


{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}


{{--                            <div class="carousel-item">--}}
{{--                                <div class="row pb-5">--}}
{{--                                    <div class="col-lg-4 col-md-12 col-12" align="center">--}}
{{--                                        <div class="review_block">--}}

{{--                                            <div><img src="landing/img/review_2.png" class="review_img"></div>--}}
{{--                                            <div align="left" class="mt-5"><img src="landing/img/5_stars.svg"></div>--}}
{{--                                            <p class="review_text mt-3">Больше всего в сервисе "СуШЕФ" мне нравится свобода от принятия незначимых решений. Больше не нужно питаться рецептами из интернета и постоянно искать (а то, что их миллион, только усложняет выбор), заранее заказывать под ...</p>--}}

{{--                                            <div class="row mt-5">--}}
{{--                                                <div class="col-lg-2 col-2">--}}
{{--                                                    <img src="landing/img/review_author_2.png">--}}
{{--                                                </div>--}}

{{--                                                <div class="col-lg-10 col-10 text-left">--}}
{{--                                                    <div class="instagram_review">--}}
{{--                                                        <span class="review_name">Sultan Aida</span><br>--}}
{{--                                                        <span class="insta_nik">@sultanaida</span>--}}

{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}


{{--                                        </div>--}}
{{--                                    </div>--}}





{{--                                    <div class="col-lg-4 col-md-12 col-12" align="center">--}}
{{--                                        <div class="review_block_center">--}}

{{--                                            <div><img src="landing/img/review_3.png" class="review_img"></div>--}}
{{--                                            <div align="left" class="mt-5"><img src="landing/img/5_stars.svg"></div>--}}
{{--                                            <p class="review_text mt-3">Открыла для себя классный сервис по доставке продуктов с рецептами "СуШЕФ"! Это не реклама, а просто рекомендация! Хотя какая это может быть от меня реклама с моими 100 подписчиками ????? Теперь даже из меня получается отличный шеф ...</p>--}}

{{--                                            <div class="row mt-5">--}}
{{--                                                <div class="col-lg-2 col-2">--}}
{{--                                                    <img src="landing/img/review_author_1.png">--}}
{{--                                                </div>--}}

{{--                                                <div class="col-lg-10 col-10 text-left">--}}
{{--                                                    <div class="instagram_review">--}}
{{--                                                        <span class="review_name ">Irina Nikolaeva</span><br>--}}
{{--                                                        <span class="insta_nik">@iroka_i</span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}


{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-lg-4 col-md-12 col-12" align="center">--}}
{{--                                        <div class="review_block">--}}

{{--                                            <div><img src="landing/img/review_1.png" class="review_img"></div>--}}
{{--                                            <div align="left" class="mt-5"><img src="landing/img/5_stars.svg"></div>--}}
{{--                                            <p class="review_text mt-3">Не думала раньше, что смогу так сильно полюбить готовить? Оказалось, что я просто ненавидела ходить в супермаркеты и собирать блюда по рецепту? Теперь вечера с дочей проходят намного интереснее и вкуснее? Спасибо су-шефу!</p>--}}

{{--                                            <div class="row mt-5">--}}
{{--                                                <div class="col-lg-2 col-2">--}}
{{--                                                    <img src="landing/img/review_author_1.png">--}}
{{--                                                </div>--}}

{{--                                                <div class="col-lg-10 col-10 text-left">--}}
{{--                                                    <div class="instagram_review">--}}
{{--                                                        <span class="review_name ">Irina Nikolaeva</span><br>--}}
{{--                                                        <span class="insta_nik">@iroka_i</span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}


{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                            </div>--}}


{{--                        </div>--}}

{{--                        <div class="row">--}}
{{--                            <div class="col-12 mt-5 d-none d-lg-block t_none">--}}
{{--                                <a class="review_control_left" href="#reviewcontrols" role="button" data-slide="prev">--}}
{{--                                    <i class="controls class-fade"><img src="landing/img/review_left_arrow.svg"></i>--}}
{{--                                </a>--}}
{{--                                <a class="review_control_right" href="#reviewcontrols" role="button" data-slide="next">--}}
{{--                                    <i class="controls class-active"><img src="landing/img/review_right_arrow.svg"></i>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}
                @endforeach
            </div>
        </div>

    </section>

    <div class="bg_eat_rel">
        <img src="landing/img/eat_bg_6.png" class="bg_eat_6">
    </div>
    <div class="bg_eat_rel">
        <img src="landing/img/eat_bg_7.png" class="bg_eat_7">
    </div>

    <section id="sec_five" class="faq_margin">
        <div class="container-fluid">
            <div class="row ">

                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
                    <h3 class="upper title_sec_three">Часто задаваемые вопросы</h3>

                </div>

                <div class="col-lg-12 col-md-12 col-12 text-center mt-5 ">

                    <div class="container center-block" style="z-index: 999!important">
                        <div class="panel-group text-left" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            {!! setting('main.question-1') !!}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <span class="text_faq">{!! setting('main.question-1-answear') !!}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            {!! setting('main.question-2') !!}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <span class="text_faq">{!! setting('main.question-2-answear') !!}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default active">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            {!! setting('main.question-3') !!}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <span class="text_faq">{!! setting('main.question-3-answear') !!}</span>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            {!! setting('main.question-4') !!}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                        <span class="text_faq">{!! setting('main.question-4-answear') !!}</span>
                                    </div>
                                </div>
                            </div>



                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFive">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                            {!! setting('main.question-5') !!}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body">
                                        <span class="text_faq">{!! setting('main.question-5-answear') !!}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </section>





    <section id="sec_seven">
        <div class="bg_eat_rel">
            <img src="landing/img/eat_bg_8.png" class="bg_eat_8">
        </div>
        <div class="bg_eat_rel">
            <img src="landing/img/eat_bg_9.png" class="bg_eat_9">
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
                    <div class="bg_apple_rel"><img src="landing/img/apple.png" class="apple"></div>
                    <span class="mr-5 price_1">{!! setting('main.price-old-after-faq') !!}</span> <span class="price_2">{!! setting('main.price-new-after-faq') !!}</span>

                    <h3 class="upper title_sec_three">{!! setting('main.text-after-price') !!}</h3>

                </div>

                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">
                    <a href="https://wa.me/77776868707?text=Добрый день! Хотим сделать заказ"><button class="btn btn_green relative my-2 my-sm-0 upper" type="submit">Заказать в 1 клик</button></a>
                </div>

            </div>
        </div>


<!--
         <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 text-center mt-5">

                   
                    <div id="apps_sec" class="bg_apps_rel d-none d-lg-block">
                        <img src="landing/img/bookmark.png" class="bookmark_top">
                        <div class="row pt-5">
                            <img src="landing/img/green_1.png" class="green_block">
                            <img src="landing/img/green_2.png" class="green_block_2">
                            <div class="col-lg-6 col-md-6 pt-5 pb-2 text-left ">
                                <p align="left" class="left_apps"><span class="upper apps_text">с приложнием</span><br>
                                    <span class="apps_minitext">еще удобнее</span>
                                </p>
                                <div class="pt-5 left_apps">
                                    <a href="#" target="_blank"><img src="landing/img/playmarket.svg" class="app_btns"></a>
                                    <a href="#" target="_blank"><img src="landing/img/appstore.svg" class="app_btns ml-3"></a>
                                </div>
                            </div>



                        </div>
                        <img src="landing/img/hand_smartphone.png" class="hand_smart">
                    </div>
                   
                    <div id="apps_sec" class="bg_apps_rel d-lg-none d-xs-block">
                        <img src="landing/img/bookmark.png" class="bookmark_top">
                        <div class="row pt-5">
                            <img src="landing/img/green_1.png" class="green_block">
                            <img src="landing/img/green_2.png" class="green_block_2">
                            <div class="col-12 pt-5 pb-2 text-left">
                                <span class="upper apps_text">с приложнием</span><br>
                                <span class="apps_minitext">еще удобнее</span>
                            </div>
                            <div class="col-12 pb-5 text-left ">
                                <a href="#" target="_blank"><img src="landing/img/playmarket.svg" width="150px"></a>
                                <a href="#" target="_blank"><img src="landing/img/appstore.svg" width="150px"></a>
                            </div>

                            <div class="col-12 text-right"><img src="landing/img/hand_smartphone.png" class="hand_smart"></div>
                        </div>
                    </div>
                 

                </div>
            </div>
        </div>
-->
    </section>

    <section id="sec_six">
        <div class="col-lg-12 col-12 col-md-12 text-center">
            <a href="#sec_one" class="scroll_menu"><img src="landing/img/footer_arrow.svg" class="sec_six_arrowup"></a>
        </div>
        
      <div class="d-none d-lg-block">
        <nav class="navbar navbar-expand-md">
            <div class="container-fluid sec_one_mar" align="center">
                <a class="navbar-brand scroll_menu" href="#sec_one"><img src="landing/img/logo2.svg"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto ml-5">
                        <li class="nav-item">
                            <a class="nav-link white upper nav_size nav_mar_right phone_header" href="tel:+77776868707">+7 777 686 87 07</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav_size nav_mar_left upper" href="mailto:souschef.kz@yandex.ru">souschef.kz@yandex.ru</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a href="https://www.instagram.com/sous_chef.kz/" target="_blank"><img src="landing/img/inst.svg" class="btn btn_soc"></a>

                        </li>
                        <li class="nav-item">
                            <a href="https://wa.me/77776868707?text=Добрый день! Хотим сделать заказ" target="_blank"><img src="landing/img/wpp.svg" class="btn btn_soc"></a>

                        </li>
                        <li class="nav-item">
                            <a href="https://wa.me/77776868707?text=Добрый день! Хотим сделать заказ" target="_blank"><button class="btn btn_white relative  my-sm-0 upper zakaz_footer" type="submit">Заказать пробный сет</button></a>
                        </li>

                    </ul>


                </div>
            </div>
        </nav>
           <hr class="line_nav">
          </div>
        
       
        <div class="d-lg-none d-xs-block">
          <div class="container">
        <div class="row">
        <div class="col text-center mt-5">
                <a class="navbar-brand ml-3 scroll_menu" href="#sec_one"><img src="landing/img/logo2.svg"></a>
            
                <a class="nav-link white upper  phone_header" href="tel:+77776868707">+7 777 686 87 07</a> 
                <a class="nav-link white  upper" href="mailto:souschef.kz@yandex.ru">souschef.kz@yandex.ru</a>
                <a href="https://www.instagram.com/sous_chef.kz/" target="_blank"><img src="landing/img/inst.svg" class="btn btn_soc"></a>
                   
                <a href="https://wa.me/77776868707?text=Добрый день! Хотим сделать заказ" target="_blank"><img src="landing/img/wpp.svg" class="btn btn_soc"></a>
            <hr class="line_nav">
                <span class="nav-link upper white mt-3 pb-2" href="#">2021. сушеф — МАГАЗИН РЕЦЕПТОВ</span>
                <span class="nav-link  nav_mar_left  white" >Разработано в: <a href="https://www.a-lux.kz/" target="_blank"><img src="landing/img/a-lux.png"></a></span>
                </div>    
        </div>
        </div>
            </div>
        
        <div class="d-none d-lg-block">
        <nav class="navbar navbar-expand-md">
            <div class="container-fluid sec_one_mar" align="center">


                <div class="collapse navbar-collapse" id="navbarsExampleDefault">

                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <span class="nav-link nav_size nav_mar_left upper white">2021. сушеф — МАГАЗИН РЕЦЕПТОВ</span>
                            <span class="nav-link  nav_mar_left  white" >Разработано в: <a href="https://www.a-lux.kz/" target="_blank"><img src="landing/img/a-lux.png"></a></span>
                        </li>

                    </ul>

                    <ul class="navbar-nav ml-auto ml-5">
<!--
                        <li class="nav-item">
                            <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#sec_one">Стартовый <span class="sr-only">(current)</span></a>
                        </li>
-->
                        <li class="nav-item">
                            <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#eat_post_sec">Блюда</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#sec_five">Вопросы</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav_size nav_mar_left upper scroll_menu" href="#sec_four">Отзывы</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        </div>

    </section>


    <script>
        $('.panel-collapse').on('show.bs.collapse', function() {
            $(this).siblings('.panel-heading').addClass('active');
        });

        $('.panel-collapse').on('hide.bs.collapse', function() {
            $(this).siblings('.panel-heading').removeClass('active');
        });
    </script>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>



    <script type="text/javascript">
        $(function() {
            $('.scroll_menu').click(function() {
                var target = $(this).attr('href');
                $('html, body').animate({
                    scrollTop: $(target).offset().top
                }, 1000); //800 - длительность скроллинга в мс
                return false;
            });
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body></html>
