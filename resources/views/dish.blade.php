@extends('layout')

@section('content')
    <div class="dish-top">
        <div class="container">
            <div class="static-top">
                <div class="menu-tabs-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-8 col-md-5 d-flex align-items-center">
                                <div class="tabs-left">
                                    <div class="back-menu">
                                        <a href="{{ url()->previous() }}">Назад в меню</a>
                                    </div>
                                    {{-- <div class="dish-top-select">
                                        <select id="persons" class="dishes-quantity">
                                            <option value="3" selected>3 блюда</option>
                                            <option value="4">4 блюда</option>
                                            <option value="5">5 блюд</option>
                                            <option value="6">6 блюд</option>
                                            <option value="7">7 блюд</option>
                                        </select>
                                        <select id="quantity" class="quantity-person">
                                            <option value="1">1 персоны</option>
                                            <option value="2" selected>2 персоны</option>
                                            <option value="4">4 персоны</option>
                                        </select>
                                        <script>
                                            document.getElementById('persons').value = localStorage.getItem('persons');
                                            document.getElementById('quantity').value = localStorage.getItem('dishes');
                                        </script>
                                    </div> --}}
                                </div>
                            </div>
                            {{-- <div class="col-xl-4 col-md-7">
                            <div class="tabs-right">
                                <div class="total-price">
                                    <span>3 231 ₽</span>
                                </div>
                                <div class="expand-icon">
                                   <span class="expand-btn">
                                       <img src="/image/expand.svg" alt=""></span>
                                    <div class="expand-wrapper">
                                        <p>Цена по подписке. Вы можете не оформлять подписку. В таком случае скидка по подписке
                                        не применяется.</p>
                                        <div class="priceInfo">
                                            <span>Семейное меню 3 блюда</span>
                                            <span>4 390 ₽</span>
                                        </div>
                                        <div class="scissors">
                                            <span>Ножницы</span>
                                           <div class="delete-scissors">
                                            <span>0 ₽</span>
                                            <span class="close-panel"></span>
                                           </div>
                                        </div>
                                        <div class="priceInfo-total">
                                            <span>Итого</span>
                                            <span>3 951 ₽</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-loader">
                                    <a href="checkout.php">ОФОРМИТЬ ЗАКАЗ</a>
                                </div>
                            </div>
                        </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dish-content">
        <div class="container dish-container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="dish-image"
                         style="background-image: url({{ isset(json_decode($dish->images)[0]) ? Voyager::image(json_decode($dish->images)[0]) : '' }});">

                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="dish-info">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                                <li class="breadcrumb-item breadcrumb-link"><a href="#">{{ $category->name }}</a></li>
                                <li class="breadcrumb-item breadcrumb-link breadcrumb-dish active" aria-current="page">
                                    {{ $dish->name }}</li>
                            </ol>
                        </nav>
                        <div class="dish-title">
                            <a href="{{ $prev }}" class="prev-arrow"><img src="/image/menu-prev.svg" alt=""></a>
                            <h1>{{ $category->name }}. Блюдо {{ $current }}</h1>
                            <a href="{{ $next }}" class="next-arrow"><img src="/image/menu-next.svg" alt=""></a>
                        </div>
                        <div class="dish-name">
                            <h1>{{ $dish->name }}</h1>
                        </div>
                        <div class="about-dish">
                            <span class="dish-dates"></span>
                            <div class="about-dish-right">
                                <span><img src="/image/cook.svg" alt="">{{ $dish->weight }} г</span>
                                <span><img src="/image/time.svg" alt="">{{ $dish->cooking_time }} мин</span>
                                <span><img src="/image/time.svg" alt=""><b>{{ $dish->cooking_difficulty }}</b></span>
                            </div>
                        </div>
                        <div class="dish-value">
                        <span>
                            На 100 г блюда: {{ $dish->energy_value }} кКал,
                            {{ $dish->protein }} | {{ $dish->protein }} | {{ $dish->fat }} БЖУ
                        </span>
                        </div>
                        <div class="dish-mobile-image">

                        </div>
                        <div class="dish-comment-wrapper">
                            <div class='row'>
                                <div class="col-xl-4 col-4">
                                    <div class="dish-comment-image">
                                    </div>
                                </div>
                                <div class='col-xl-7 col-8'>
                                    <div class='dish-comment'>
                                        <p class='p-comment'>{{ $dish->comment }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='share'>
                            <div class="share-text">
                                <p> Поделитесь рецептом: </p>
                            </div>
                            <div class="share-social">
                                <a href=""><i class="fab fa-facebook-f"></i></a>
                                <a href=""><i class="fab fa-vk"></i></a>
                                <a href=""><i class="fab fa-odnoklassniki"></i></a>
                                <a href=""><i class="fab fa-whatsapp"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="dish-nav">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mobile-nav-link" id="we-will-bring-tab" data-toggle="pill"
                                   href="#we-will-bring" role="tab" aria-controls="we-will-bring" aria-selected="false">Мы
                                    привезем</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link active mobile-nav-link" id="pills-recipe-tab" data-toggle="pill"
                                    href="#pills-recipe" role="tab" aria-controls="pills-recipe"
                                    aria-selected="false">Рецепт</a>
                            </li>--}}
                            <li class="nav-item">
                                <a class="nav-link mobile-nav-link" id="pills-home-tab" data-toggle="pill"
                                   href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="false">Должно
                                    быть дома</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div style="display: flex;
    max-width: 100%;
    justify-content: center;">

        <div class="dish-tabs" style="width: 1260px;">

            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade" id="we-will-bring" role="tabpanel" aria-labelledby="we-will-bring-tab">
                    <div class="container">
                        <div class="row dish-companents">
                            <div class="container">


                                <div class="col-xl-3 col-6 col-md-3">
                                    <div class="products">
                                        {{--                                        <div class="products-image"--}}
                                        {{--                                             style="background-image: url('{{ Voyager::image($ingridient->photo) }}')">--}}

                                    </div>
                                    <div class="about-product">
                                        <p>{{ $dish->description }}</p>
                                        {{--                                            <span>{{ $ingridient->weight}}</span>--}}
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="we-will-bring-tab">
                    <div class="container">
                        <div class="row dish-companents">
                            <div class="container">


                                <div class="col-xl-3 col-6 col-md-3">
                                    <div class="products">
                                        {{--                                        <div class="products-image"--}}
                                        {{--                                             style="background-image: url('{{ Voyager::image($ingridient->photo) }}')">--}}

                                    </div>
                                    <div class="about-product">
                                        <p>{{ $dish->home_ingridients }}</p>
                                        {{--                                            <span>{{ $ingridient->weight}}</span>--}}
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

{{--            --}}
{{--            <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">--}}
{{--                <div class="container">--}}
{{--                    <div class="row dish-companents">--}}

{{--                        <div class="col-xl-3 col-6 col-md-3">--}}
{{--                            <div class="products">--}}
{{--                                --}}{{--                            <div class="products-image"--}}
{{--                                --}}{{--                                 style="background-image: url('{{ Voyager::image($home_ingridient->photo) }}')">--}}

{{--                            </div>--}}
{{--                            <div class="about-product">--}}
{{--                                <p>{{ $dish->home_ingridients }}</p>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}





            {{-- <div class="tab-pane fade show active" id="pills-recipe" role="tabpanel" aria-labelledby="pills-recipe-tab">
                <div class="container">
                    <div class="row dish-companents">
                        <div class="col-xl-12">
                            {{-- <div class="dish-recipe-inner">
                                {{-- <div class="dishRecipe-action">
                                <a href=""><img src="/image/dish6.svg" alt="">Распечатать</a>
                            </div> --}}
            {{-- <div class="dishRecipe-action">
                <a href="{{ asset('/') }}storage/{{ $dish->recipe }}">
                    <img src="/image/dish-5.png" alt="" />
                    Открыть на весь экран
                </a>
            </div>
        </div> --}}
            {{--            <img style="width: 100%" src="{{ asset('/') }}storage/{{ $dish->recipe }}"--}}
            {{--                 class='desktop-dish'/>--}}
            {{-- <div style="
                display: grid;
                grid-template-columns: repeat(3, 1fr);
                grid-column-gap: 10px;
                grid-row-gap: 1em;"
            >
                @foreach($recipeSlides as $slide)
                <div>
                    <div>
                        <div>
                            <img
                                src="{{ asset('/') }}storage/{{ str_replace('public/', '', $slide->image) }}"
                                style="width: 100%; max-height: 100%"
                                alt="">

                            <p>
                                {{ $slide->text }}
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div> --}}


            <div class="dishes-slider-mobile">

                @foreach($recipeSlides as $slide)
                    <div class="dishes-slide">
                        @if($slide->image != null)
                            <img src="{{ asset('/') }}storage/{{ str_replace('public/', '', $slide->image) }}"
                                 style="    height: 170px;
    width: 100%;
    object-fit: contain;" alt="">
                        @endif
                        <p>{{ $slide->text }}</p>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

    {{-- <embed
        src="{{ asset('/') }}storage/{{
            json_decode($dish->recipe)
                ?
                json_decode($dish->recipe)[0]->download_link : ''
            }}" style="width: 100%; height: 1000px"> --}}

    {{--    </div>--}}
    {{--    </div>--}}
    {{--    </div>--}}
    <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <div class="container">
            <div class="row dish-companents">

                <div class="col-xl-3 col-6 col-md-3">
                    <div class="products">
                        {{--                            <div class="products-image"--}}
                        {{--                                 style="background-image: url('{{ Voyager::image($home_ingridient->photo) }}')">--}}

                    </div>
                    <div class="about-product">
                        <p>{{ $dish->home_ingridients }}</p>

                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
    </div>
    </div>


@endsection
