@extends('layout')

@section('content')
<div class="questions subscribe">
    <div class="container">
        <div class="questions-title">
            <h2>Как работает подписка?</h2>
        </div>
        <div class="questions-image">
            <img src="image/contact-2.png" alt="">
        </div> 
        @php
            $half = ceil($questions->count() / 2);
            $chunks = $questions->chunk($half);
        @endphp
        <div class="questions-content">
            <div class="row">
                @foreach($chunks as $questions)
                <div class="col-xl-6">
                    @foreach($questions as $question)
                        <div class="question-inner">
                        <div class="question-btn">
                            <button class="btn sub-btn" type="button" data-toggle="collapse" data-target="#collapseExample{{ $question->id }}" aria-expanded="false" aria-controls="collapseExample{{ $question->id }}">
                                {{ $question->title }}
                            </button>
                            <i class="fas fa-caret-right arrow-right"></i>
                        </div>
                        <div class="collapse" id="collapseExample{{ $question->id }}">
                            <div class="card card-body question-body">
                                {!! $question->answer !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection