@extends('layout')

@section('content')
    @php
        //TODO: Arrow top: 50% to Arrows top 7%;
    @endphp
    <div class="menu-page">
        <div class="container">
            <div class="slider-nav">
                @foreach($weeks as $week)
                    <div class="menu-slide">
                        <h1>Меню на {{ $week->name }}</h1>
                        <p>Доставка этого меню доступна только с 1 по 4 марта </p>
                        <div class="menu-tabs">
                            <div class="container">
                                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                    @foreach($week->categories as $category)
                                        <li class="nav-item">
                                            <a class="nav-link @if($loop->first) active @endif" id="pill-link{{ $week->id }}cat{{ $category->id }}" data-toggle="pill" href="#pill{{ $week->id }}cat{{ $category->id }}" role="tab" aria-controls="pill{{ $week->id }}cat{{ $category->id }}" aria-selected="true">
                                                {{ $category->name }}
                                                @if($category->hit)<p>ХИТ</p>@endif
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="slide-content">
                            <div class="slider-content">
                                <div class="tab-content" id="pills-tabContent">
                                    @foreach($week->categories as $category)
                                        <div class="tab-pane fade show @if($loop->first) active @endif" id="pill{{ $week->id }}cat{{ $category->id }}" role="tabpanel" aria-labelledby="pill{{ $week->id }}cat{{ $category->id }}">
                                        <div class="original">
                                            <div class="container">
                                                <div class="original-slider">
                                                    @foreach($category->dishes as $dish)
                                                        <div class="original-slide">
                                                            <div class="menu-slide-image">
                                                                <img src="{{ !empty(json_decode($dish->images)) ? Voyager::image(json_decode($dish->images)[0]) : '' }}" alt="">
                                                                <div class="menu-hashtags">
                                                                    @foreach($dish->hashTags as $hashtag)
                                                                        <span>#{{ $hashtag->name }}</span>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            <div class="menu-content">
                                                                <div class="row">
                                                                    <div class="col-xl-2 col-md-2">
                                                                        <div class="time-cook">

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-10  col-md-10 col-12">
                                                                        <div class="dish-title">
                                                                            <h1>{{ $dish->name }}</h1>
                                                                            <div class="time-cook-mob">
                                                                            </div>
                                                                            <div class="dish-info">
                                                                                <div class="dish-icons">
                                                                                    <div class="dish-icon">
                                                                                        <p><img class="lazy" data-src="/image/cook.svg" alt=""> {{ $dish->weight }} г</p>
                                                                                    </div>
                                                                                    <div class="dish-icon">
                                                                                        <p><img class="lazy" data-src="/image/time.svg" alt=""> {{ $dish->cooking_time }} мин</p>
                                                                                    </div>
                                                                                    <div class="dish-icon dish-time">
                                                                                        <p><img class="lazy"  data-src=src="/image/shef.svg" alt=""> {{ $dish->cooking_difficulty }}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="dish-value dish-icon">
                                                                                    <p>На 100 г блюда: {{ $dish->energy_value }} кКал, {{ $dish->protein }} | {{ $dish->fat }} | {{ $dish->carbohydrates }} БЖУ
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="menu-buttons">
                                                <a href="/week/{{ $week->id }}" class="watch">Смотреть меню</a>
                                                <a href="/choose" class="compare">Сравнить меню</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
