@extends('layout')

@section('content')
    <div class="compain-page">
    <div class="what-choose">
        <h1>Что выбрать?</h1>
    </div>
    <div class="container">
        <div class="compain-list">
            <table>
                <thead>
                <tr id="choose-head" class="choose-head">
                    <th >&nbsp;</th>
                    @foreach ($categories as $category)
                        <th class="choose-title">
                            {{ $category->name }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                    <tr class="actions-row">
                        <td class="actions-col">&nbsp;</td>
                        @foreach ($categories as $category)
                            <td class="actions-col">
                                <a href="/week/{{ $week }}?category={{ $category->id }}" class="choose-button">
                                    Выбрать
                                </a>
                            </td>
                        @endforeach
                    </tr>
                @foreach ( config('categories')[0] as $key => $value)
                
                    <tr class="choose-border">
                        <td class="choose-value">
                            {{ config('categories')[0][$key] }}
                        </td>
                        @foreach ( $categories as $category)
                            <td class="choose-price">
                                @if($category->{$key} == 1)
                                    <div class="text-center">
                                        <i class="fas fa-check"></i>
                                    </div>
                                @else
                                    {{ $category->{$key} }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach

                <tr class="choose-row"
                >
                    <td
                    >Ингредиенты</td>
                    <td class="">&nbsp;</td>
                    <td class="">&nbsp;</td>
                    <td class="">&nbsp;</td>
                    <td class="">&nbsp;</td>
                </tr>
                @foreach ( config('categories')[1] as $key => $value)
                    <tr class="choose-border">
                        <td class="choose-value">
                            {{ config('categories')[1][$key] }}
                        </td>
                        @foreach ( $categories as $category)
                            <td class="choose-price">
                                @if($category->{$key} == 1)
                                    <div class="text-center">
                                        <i class="fas fa-check"></i>
                                    </div>
                                @else
                                    {{ $category->{$key} }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                    <tr class="choose-row">
                        <td>Рецептура</td>
                        <td class="choose-price">&nbsp;</td>
                        <td class="choose-price">&nbsp;</td>
                        <td class="choose-price">&nbsp;</td>
                        <td class="choose-price">&nbsp;</td>
                    </tr>
                @foreach ( config('categories')[2] as $key => $value)
                    <tr class="choose-border">
                        <td class="choose-value">
                            {{ config('categories')[2][$key] }}
                        </td>
                        @foreach ( $categories as $category)
                            <td class="choose-price">
                                @if($category->{$key} == 1)
                                    <div class="text-center">
                                        <i class="fas fa-check"></i>
                                    </div>
                                @else
                                    {{ $category->{$key} }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="compain-list-mobile">
        <h2>Выберите типы меню</h2>
        <div class="select-div">
            <select id="left">
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <select id="right">
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        @foreach ( config('categories')[0] as $key => $value)
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>{{ config('categories')[0][$key] }}</h1>
                </div>
                <div class="row" id="zero[{{ $key }}]">
                    <div class="col-6">
                        <div class="choose-mobile-col" id="leftZero">

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col" id="rightZero">
                            
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        
        <div class="choose-mobile-name">
            <h2>Ингредиенты</h2>
        </div>
        @foreach ( config('categories')[1] as $key => $value)
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>{{ config('categories')[1][$key] }}</h1>
                </div>
                <div class="row" id="first[{{ $key }}]">
                    <div class="col-6">
                        <div class="choose-mobile-col" id="leftFirst">

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col" id="rightFirst">
                            
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="choose-mobile-name">
            <h2>Рецептура</h2>
        </div>
        @foreach ( config('categories')[2] as $key => $value)
            <div class="choose-mobile-row">
                <div class="choose-mobile-title">
                    <h1>{{ config('categories')[2][$key] }}</h1>
                </div>
                <div class="row" id="second[{{ $key }}]">
                    <div class="col-6">
                        <div class="choose-mobile-col" id="leftSecond">

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="choose-mobile-col" id="rightSecond">
                            
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
</div>

@php
    $iteratorArray = [
        0 => 'Zero',
        1 => 'First',
        2 => 'Second'
    ];
    $i = 0;
@endphp
@foreach(config('categories') as $part)
    @foreach ( $categories as $category)
        <template id="part{{ $iteratorArray[$i] }}Category[{{ $category->id }}]">
            @foreach ( $part as $key => $value)
                <p>@if($category->{$key} == 1)
                    <div class="text-center">
                        <i class="fas fa-check"></i>
                    </div>
                @else
                    @if($category->{$key} == 0)
                    <div class="text-center">
                        <i class="fas fa-times"></i>
                    </div>
                    @else
                        {{ $category->{$key} }}
                    @endif
                @endif</p>
            @endforeach
        </template>
    @endforeach
    @php
        $i = $i+1;
    @endphp
@endforeach

<script>
    function setCategoryLeft(e) {
        document.getElementById('leftZero').innerHTML = document.getElementById('partZeroCategory[' + e.target.value + ']').innerHTML;
        document.getElementById('leftFirst').innerHTML = document.getElementById('partFirstCategory[' + e.target.value + ']').innerHTML;
        document.getElementById('leftSecond').innerHTML = document.getElementById('partSecondCategory[' + e.target.value + ']').innerHTML;
    }
    function setCategoryRight(e) {
        document.getElementById('rightZero').innerHTML = document.getElementById('partZeroCategory[' + e.target.value + ']').innerHTML;
        document.getElementById('rightFirst').innerHTML = document.getElementById('partFirstCategory[' + e.target.value + ']').innerHTML;
        document.getElementById('rightSecond').innerHTML = document.getElementById('partSecondCategory[' + e.target.value + ']').innerHTML; 
    }
    setCategoryLeft({
        target: {
            value: {{ $categories[0]->id }}
        }
    });
    setCategoryRight({
        target: {
            value: {{ $categories[0]->id }}
        }
    });
    document.getElementById('left').addEventListener("change", setCategoryLeft);
    document.getElementById('right').addEventListener("change", setCategoryRight);
</script>
@endsection
