@extends('layout')

@section('content')

<div class="agreement">
    <div class="container">
        {!! setting('profil.agreement') !!}
    </div>
</div>

@endsection