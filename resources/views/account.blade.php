@extends('layout')

@section('content')
<div class="account">
    <div class="container">
        <div class="account-inner titleDiv">
            <h1>Мой аккаунт</h1>
            <h2>Авторизация</h2>
            <div class="account-form">
                <div class="account-link">
                    <div class="account-title">
                        <h3>Имя пользователя или email</h3>
                        <span>*</span>
                    </div>
                    <input type="text">
                </div>
                <div class="account-link">
                    <div class="account-title">
                        <h3>Пароль </h3>
                        <span>*</span>
                    </div>
                    <input type="text">
                </div>
                <div class="account-check">
                    <input type="checkbox">
                    <h3>Запомнить меня</h3>
                </div>
                <div class="account-button">
                    <button class="button-item">Войти</button>
                    <a href="">Забыли свой пароль?</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
