@extends('layout')

@section('content')
<div class="about">
    <div class="container">
        <div class="row about-row">
            <div class="col-xl-9">
                <div class="about-content title">
                    <h1>{{ setting('kak-eto-rabotaet.title') }}</h1>
                    <span>{{ setting('kak-eto-rabotaet.text-under-title') }}</span>
                    <h2>{{ setting('kak-eto-rabotaet.text-above-first-image') }}</h2>
                    <img src="{{ Voyager::image(setting('kak-eto-rabotaet.first-image')) }}" alt="">
                    {{ setting('kak-eto-rabotaet.text-under-image') }}
                </div>
                <div class="about-content-inner">
                    <div class="row">
                        <div class="col-xl-5">
                            <div class="content-image">
                                <img src="{{ Voyager::image(setting('kak-eto-rabotaet.first-image-left')) }}" alt="">
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="content-text">
                                {!! setting('kak-eto-rabotaet.first-right-text') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-content">
                    <img src="{{ Voyager::image(setting('kak-eto-rabotaet.second-image')) }}" alt="">
                    {{ setting('kak-eto-rabotaet.text-under-second-image') }}
                </div>
                <div class="about-content-inner">
                    <div class="row">
                        <div class="col-xl-5">
                            <div class="content-image">
                                <img src="{{ Voyager::image(setting('kak-eto-rabotaet.second-image-left')) }}" alt="">
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="content-text">
                                {!! setting('kak-eto-rabotaet.second-right-text') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-content">
                    <img src="{{ Voyager::image(setting('kak-eto-rabotaet.third-image')) }}" alt="">
                    {{ setting('kak-eto-rabotaet.text-under-third-image') }}
                </div>
                <div class="about-content-inner">
                    <div class="row">
                        <div class="col-xl-5">
                            <div class="content-image">
                                <img src="{{ Voyager::image(setting('kak-eto-rabotaet.third-image-left')) }}" alt="">
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="content-text">
                                {!! setting('kak-eto-rabotaet.third-right-text') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-content">
                    <img src="{{ Voyager::image(setting('kak-eto-rabotaet.forth-image')) }}" alt="">
                    {!! setting('kak-eto-rabotaet.bottom-text') !!}
                </div>
            </div>
            <div class="col-xl-12">
                <div class="about-video">
                    <iframe width="1180" height="664" src="{{ setting('kak-eto-rabotaet.video') }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
