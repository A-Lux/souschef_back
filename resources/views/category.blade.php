@extends('layout')

@section('content')
@php
    $week_fields = ['id', 'name'];
    $categories_fields = [
        'id', 
        'name', 
        // 'price'
        'price_two_three',
        'price_two_five',
        'price_four_three',
        'price_four_five'
    ];
    $dish_fields = [
        'id',
        'name',
        'shelf_life',
        'weight',
        'cooking_time',
        'cooking_difficulty',
        'images',
        'price',
        'checked'
    ];
@endphp
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" integrity="sha256-T/f7Sju1ZfNNfBh7skWn0idlCBcI3RwdLSS4/I7NQKQ=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="/js/vue/dish.js"></script>
<script src="/js/vue/category.js"></script>
<script src="/js/vue/categoryPill.js"></script>
<script src="/js/vue/weeks.js"></script>
<div class="menu-inner">
    <div class="container menu-inner-container">
        <div id="app">
            <form name="checkout" method="post" action="/order">
                @csrf
            </form>
            <weeks @save="save" :weeks='weeks' :current='currentWeek'></week>
            
        </div>
    </div>
</div>
<script>
    window.onload = function () {
        // var url = window.location.path.split('/');
        // var lastSegment = url.pop() || url.pop();  // handle potential trailing slash

        var app = new Vue({
            el: '#app',
            data() {
                return {
                    currentWeek: 0,
                    weeks: [
                        @foreach ($weeks as $week)
                        {
                            @foreach ($week->toArray() as $key => $value)
                                    @if(in_array($key , $week_fields))  
                                            {{ $key }}: "{{ $value }}",
                                    @endif
                            @endforeach
                            @if($week->categories)
                                categories: [
                                    @foreach ($week->categories as $category)
                                    {
                                        @foreach ($category->toArray() as $key => $value)
                                            @if(in_array($key , $categories_fields))  
                                                    {{ $key }}: "{{ $value }}",
                                            @endif
                                        @endforeach
                                        @if($category->dishes)
                                            dishes: [
                                                @foreach ($category->dishes as $dish)
                                                {
                                                    @foreach ($dish->toArray() as $key => $value)
                                                        @if(in_array($key , $dish_fields))  
                                                        
                                                            {{ $key }}: @if(gettype($value) != "integer")"@endif{{ $value }}@if(gettype($value) != "integer")"@endif,
                                                        @endif
                                                    @endforeach
                                                },
                                                @endforeach
                                            ]
                                        @endif
                                    },
                                    @endforeach
                                ]
                            @endif
                        },
                        @endforeach
                    ]
                }
            },
            created(){
                // this.currentWeek = this.weeks.findIndex(el => el.id == parseInt(lastSegment));
                let state = JSON.parse(localStorage.getItem('state'));
                this.weeks
                    .forEach(week => {
                        let state_week = state.find(el => el.id == week.id);
                        if(state_week){
                            week.categories.forEach(category => {
                                let state_category = state_week.categories.find(el => el.id == category.id);
                                if(state_category){
                                    category.dishes.forEach(dish =>{
                                        let state_dish = state_category.dishes.find(el => el.id == dish.id)
                                        if(state_dish){
                                            dish.checked = state_dish.checked;
                                        }
                                    })
                                }
                            })
                        }
                    });
            },
            components:{
                'weeks': weeks,
                'category-pill': categoryPill,
            },
            methods: {
                setNext(){
                    if(this.currentWeek == this.weeks.length - 1){
                        this.currentWeek == 0;
                    }
                    else{
                        this.currentWeek += 1;
                    }   
                },
                setPrev(){
                    if(this.currentWeek == 0){
                        this.currentWeek == this.weeks.length - 1;
                    }
                    else{
                        this.currentWeek -= 1;
                    }                
                },
                save(){
                    localStorage.setItem('state', JSON.stringify(this.weeks));
                }
            },
        });
        if($('#app').length){
            $('.slider-nav').on('init', function(event, slick){
                $('.slick-prev').off();
                $('.slick-next').off();
                $('.slick-prev').on('click', function () {
                    app.setPrev();
                });
                $('.slick-next').on('click', function () {
                    app.setNext();
                });
            });
            $('.slider-nav').slick({
                slidesToShow: 0.999,
                slidesToScroll: 1,
                swipe: false,
                asNavFor: '.slide-content',
            });
        }
    }
    
    
</script>

@endsection