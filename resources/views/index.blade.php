@extends('layout')

@section('content')
<!--    --><?php //include('modal-basket.php');?>
<!--    --><?php //include('whatsapp-modal.php');?>
<div class="about-recipes lazy" style="background-image: url({{ asset('/storage/'.setting('main.background_one')) }});">
    <div class="container">
        <div class="row about-recipes-row ">
            <div class="col-xl-6 col-12 col-md-6">
                <div class="about-recipes-text">
                    <h1>{{ setting('main.heading_one') }}</h1>
                    <p>{{ setting('main.text_one') }}</p>
                    <a href="{{ setting('main.button_link_one') }}">
						<i class="fas fa-shopping-cart"></i>
						{{ setting('main.button_text_one') }}
					</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="advantage">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-md-6 col-6" data-aos="fade-right" data-aos-duration="1000">
                <div class="advantage-link">
                    <div class="advantage-icon">
                        <a href=""><i class="fas fa-vote-yea"></i></a>
                    </div>
                    <div class="advantage-text">
                        <h2>{{ setting('main.square_one_title') }}</h2>
                        <p>{{ setting('main.square_one_text') }}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6" data-aos="fade-down" data-aos-duration="1100">
                <div class="advantage-link">
                    <div class="advantage-icon">
                        <a href=""><i class="fas fa-truck-moving"></i></a>
                    </div>
                    <div class="advantage-text">
                        <h2>{{ setting('main.square_two_title') }}</h2>
                        <p>{{ setting('main.square_two_text') }}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6" data-aos="fade-up" data-aos-duration="1150">
                <div class="advantage-link">
                    <div class="advantage-icon">
                        <a href=""><i class="far fa-clock"></i></a>
                    </div>
                    <div class="advantage-text">
                        <h2>{{ setting('main.square_three_title') }}</h2>
                        <p>{{ setting('main.square_three_text') }}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6" data-aos="fade-left" data-aos-duration="1200">
                <div class="advantage-link">
                    <div class="advantage-icon">
                        <a href=""><i class="fas fa-recycle"></i></a>
                    </div>
                    <div class="advantage-text">
                        <h2>{{ setting('main.square_four_title') }}</h2>
                        <p>{{ setting('main.square_four_text') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-recipes-2 lazy" style="background-image: url({{ asset('/storage/'.setting('main.background_two')) }});">
    <div class="container">
        <div class="row about-recipes-2-row">
            <div class="col-xl-4 p-0 col-6">
                <div class="about-recipes-2-text " data-aos="zoom-in" data-aos-duration="1500">
                    <h1>{{ setting('main.heading_two') }}</h1>
                    <p>{{ setting('main.text_two') }}</p>
                    <a href="{{ setting('main.button_link_two') }}"><i
                            class="far fa-gem"></i>{{ setting('main.button_text_two') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-recipes-3 lazy" style="background-image: url({{ asset('/storage/'.setting('main.background_three')) }});">
    <div class="container">
        <div class="row about-recipes-3-row">
            <div class="col-xl-4 p-0 col-7 col-md-6">
                <div class="about-recipes-3-text" data-aos="zoom-in" data-aos-duration="1500">
                    <h1>{{ setting('main.heading_three') }}</h1>
                    <p>{{ setting('main.text_three') }}</p>
                    <!-- <a href="{{ setting('main.button_link_three') }}"><i class="far fa-gem"></i>{{ setting('main.button_text_three') }}</a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-recipes-4 lazy" style="background-image: url({{ asset('/storage/'.setting('main.background_four')) }});">
    <div class="container">
        <div class="row about-recipes-4-row">
            <div class="col-xl-4 p-0 col-7 col-md-6">
                <div class="about-recipes-4-text" data-aos="zoom-in" data-aos-duration="1500">
                    <h1>{{ setting('main.heading_four') }}</h1>
                    <p>{{ setting('main.text_four') }}</p>
                    <!-- <a href="{{ setting('main.button_link_four') }}"><i class="far fa-gem"></i>{{ setting('main.button_text_four') }}</a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="review">
    <div class="container">
        <div class="review-logo-image" data-aos="zoom-in" data-aos-duration="1200">
            <img class="lazy" data-src="{{ asset('/storage/'.setting('main.image_reviews')) }}" alt="">
        </div>
        <div class="review-top">
            <h1>{{ setting('main.title_reviews') }}</h1>
            <img  src="image/contact-2.png" alt="">
        </div>
        <div class="review-slider-title">
            <h2>{{ setting('main.subtitle_reviews') }}</h2>
        </div>
        <div class="review-slider">
            @foreach($slides as $slide)
            <div class="review-slide">
                <div class="review-title">
                    <img class="lazy" data-src="/storage/{{ $slide->avatar }}" alt="">
                    <div class="review-name">
                        <h1>{{ $slide->name }}<img class="lazy" data-src="/storage/{{ $slide->social }}" /></h1>
                        <p>{{ $slide->position }}</p>
                    </div>
                </div>
                <div class="review-logo">
                    <img class="lazy" data-src="/storage/{{ $slide->image }}" alt="">
                </div>
                <div class="rating">
                    @for($i = 0; $i < $slide->stars; $i++)
                        <i class="fas fa-star"></i>
                        @endfor
                </div>
                <div class="review-comment">
                    <p class="review-p">{{ $slide->text }}</p>
                </div>
            </div>
            @endforeach
        </div>
        <div class="company-icons">
            <div class="company-icons-image" data-aos="zoom-out-left" data-aos-duration="1000">
                <img class="lazy" data-src="{{ asset('/storage/'.setting('main.image_one')) }}" alt="">
            </div>
            <div class="company-icons-image" data-aos="zoom-out-down" data-aos-duration="1100">
                <img class="lazy" data-src="{{ asset('/storage/'.setting('main.image_two')) }}" alt="">
            </div>
            <div class="company-icons-image" data-aos="zoom-out-right" data-aos-duration="1200">
                <img class="lazy" data-src="{{ asset('/storage/'.setting('main.image_three')) }}" alt="">
            </div>
        </div>
    </div>
</div>
@endsection