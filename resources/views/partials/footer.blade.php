<div class="footer" style="position: relative;">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-6 col-md-6">
            <div class="copyright">
            <p>{{ setting('site.copyright') }}</p>
            <a href="">{{ setting('site.email') }}</a>
            <a href="">{{ setting('site.phone') }}</a>
        </div>
            </div>
            <div class="col-xl-3 col-6">
            <div class="footer-link">
                <a href="/about">Как это работает?</a>
                <a href="/subscribe">Как работает подписка?</a>
                <a href="/menu">Меню</a>
                <a href="/contacts">Контакты</a>
                <a href="/home">Аккаунт</a>
                <a href="/agreement">Пользовательское соглашение</a>
            </div>
            </div>
            <div class="col-xl-3 col-md-6">
            <h1 class="social-title">Принимаем оплату картой:</h1>
            <div class="footer-icons footer-card">
            <a href=""><img class="lazy" data-src="/image/visa.png" alt=""></a>
            <a href=""><img class="lazy" data-src="/image/mastercard.png" alt=""></a>
            </div>
                <h1 class="social-title">Подписывайтесь на нас:</h1>
            <div class="footer-icons">
            <a href=""><i class="fab fa-yelp"></i></a>
            <a href=""><i class="fab fa-facebook-f"></i></a>
            <a href=""><i class="fab fa-twitter"></i></a>
            <a href=""><i class="fab fa-amazon"></i></a>
            </div>
            </div>
            <div class="col-xl-2 col-12 col-md-6">
            <div class="app-links">
                <a href=""><img class="lazy" data-src="/image/google-play.svg" alt=""></a>
                <a href=""><img class="lazy" data-src="/image/app-store.svg" alt=""></a>
            </div>
            </div>
        </div>
        
    </div>
</div>

<link rel="stylesheet preload" href="/slick/slick-theme.css">
<link rel="stylesheet preload" href="/slick/slick.css">
<link rel="stylesheet preload" href="/select2.min.css">
<link rel="stylesheet preload" href="/hc-offcanvas-nav.css">
<link rel="stylesheet preload" href="/slick/slick-theme.css">
<link rel="stylesheet preload" href="/slick/slick.css">
<link rel="stylesheet preload" href="/select2.min.css">
<link rel="stylesheet preload" href="/hc-offcanvas-nav.css">
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js" defer></script>
<script src="/js/hc-offcanvas-nav.js" defer></script>
<script src="/js/select2.min.js" defer></script>
<script src="/js/jquery.maskedinput.min.js" defer></script>
<script src="/js/bootstrap.min.js" defer></script>
<script src="/slick/slick.min.js" defer></script>
<script src="/js/main.js" defer></script>
 
<script>
    document.addEventListener("DOMContentLoaded", function() {
      var lazyloadImages = document.querySelectorAll("img.lazy");    
      var lazyloadThrottleTimeout;
      
      function lazyload () {
        if(lazyloadThrottleTimeout) {
          clearTimeout(lazyloadThrottleTimeout);
        }    
        
        lazyloadThrottleTimeout = setTimeout(function() {
            var scrollTop = window.pageYOffset;
            lazyloadImages.forEach(function(img) {
                if(img.offsetTop < (window.innerHeight + scrollTop)) {
                  img.src = img.dataset.src;
                  img.classList.remove('lazy');
                }
            });
            if(lazyloadImages.length == 0) { 
              document.removeEventListener("scroll", lazyload);
              window.removeEventListener("resize", lazyload);
              window.removeEventListener("orientationChange", lazyload);
            }
        }, 20);
      }
      
      document.addEventListener("scroll", lazyload);
      window.addEventListener("resize", lazyload);
      window.addEventListener("orientationChange", lazyload);
    });
    </script>
<script>
    $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
        $('.original-slider').slick('reinit')
    })
</script>
 <!-- <script src="https://unpkg.com/@popperjs/core@2" defer></script>
 <link rel="stylesheet" href="/aos.css" defer/>
<script src="/js/aos.js" defer></script> -->


<!-- 
<script src="https://api-maps.yandex.ru/2.1/?apikey=8350983e-1e36-4ac2-95ed-62859584bf6f&lang=ru_RU" type="text/javascript" defer></script> -->
</body>
</html>
