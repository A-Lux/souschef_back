<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Сушеф</title>
<!-- <link  href="https://fonts.googleapis.com/css?family=Lato&Merriweather&display=swap" rel="stylesheet"> -->

<link rel="stylesheet preload" href="/css/main.css">
<link rel="stylesheet preload" href="/css/bootstrap.min.css">

<link rel="stylesheet preload" href="/font/css/all.min.css">
    
</head>

<body>
    {{-- <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align: center;">
                        Дорогие друзья!
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="display: flex; flex-direction: column;">
                    <p style="margin-bottom: 0;">До 01.09.20 наш сервис временно недоступен. </p> 
                    <p style="margin-bottom: 0;"> Скоро наша продукция будет доступна в супермаркетах и
                        торговых сетях! Мы обязательно об этом Вас оповестим!</p>
                    <p style="margin-bottom: 0;">С уважением, Ваш СуШЕФ</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="header">
        <div class="header-descktop">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-4">
                        <a href="/">
                            <div class="header-left">
                                <div class="logo">
                                    <img src="{{ asset('/storage/'.setting('site.logo')) }}" alt="">
                                </div>
                                <div class="tagline">
                                    <h1>Готовить - одно удовольствие</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-8">
                        <div class="header-links">
                            {{--                        <a class='link-active' href="">Главная</a>--}}
                            {{--                        <a href="/about">Как это работает?</a>--}}
                            {{--                        <a href="">Меню</a>--}}
                            {{--                        <a href="">Контакты</a>--}}
                            {{--                        <a href="">Аккаунт</a>--}}

                            @foreach(menu('header', '_json') as $menu_item)
                            <a class="{{ 
                                env('APP_URL').$menu_item->link() == url()->current() ? 
                                    'link-active' 
                                        : 
                                    ''
                                }}" @if(strpos($menu_item->link(), 'home'))
                                href="{{ $menu_item->link() }}">
                                @auth {{ $menu_item->title }} @endauth
                                @guest {{ 'ВОЙТИ' }} @endguest
                                @elseif(strpos($menu_item->link(), 'menu'))
                                @php
                                $link = '/week/' . \App\Week::orderBy('created_at', 'desc')->first()->id;
                                @endphp
                                href="{{ $link }}">
                                {{ $menu_item->title }}
                                @else
                                href="{{ $menu_item->link() }}">
                                {{ $menu_item->title }}
                                @endif
                            </a>
                            @endforeach

                            <li class="nav-item">
                                <span
                                    class="basket-count">{{ session()->has('dishes') ? count(session()->get('dishes')) : 0 }}</span>
                                <a class="nav-link icons" href="/cart">
                                    {{ session()->has('price') ? session()->get('price') : 0 }} <span>₸</span>
                                    <i class="fa fa-shopping-basket" aria-hidden="true"></i></a>
                            </li>
                            @auth
                            <div class="logout">
                                <form action="/logout" method='post'>
                                    @csrf
                                    <button>
                                        <i class="fas fa-sign-out-alt"></i>
                                    </button>
                                </form>
                            </div>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-logo">
            <a href=""><img data-src="{{ asset('/storage/'.setting('site.logo')) }}" class="mobile-logotype lazy" alt=""></a>
        </div>
        <div class="basket-mobile">
            <span class="basket-count">0</span>
            <a href=""><i class="fa fa-shopping-basket" aria-hidden="true"></i></a>
        </div>
        <a class="mobile-menu-toggle js-toggle-menu hamburger-menu" href="#">
        </a>
        <nav class="mobile-nav-wrap" role="navigation">
            <ul class="mobile-header-nav">
                @foreach(menu('header', '_json') as $menu_item)
                <li>

                    <a class="{{ env('APP_URL').$menu_item->link() == url()->current() ? 'link-active' : ''}}" {{ 
                    env('APP_URL').$menu_item->link() == url()->current() ? 
                        'link-active' 
                            : 
                        ''
                    }}" @if(strpos($menu_item->link(), 'home'))
                        href="{{ $menu_item->link() }}">
                        @auth {{ $menu_item->title }} @endauth
                        @guest {{ 'ВОЙТИ' }} @endguest
                        @elseif(strpos($menu_item->link(), 'menu'))
                        @php
                        $link = '/week/' . \App\Week::orderBy('created_at', 'desc')->first()->id;
                        @endphp
                        href="{{ $link }}">
                        {{ $menu_item->title }}
                        @else
                        href="{{ $menu_item->link() }}">
                        {{ $menu_item->title }}
                        @endif</a>

                </li>
                @endforeach
                <!-- <li><a href="#">ГЛАВНАЯ</a></li>
            <li><a href="#">КАК ЭТО РАБОТАЕТ?</a></li>
            <li><a href="#">МЕНЮ</a></li>
            <li><a href="#">КОНТАКТЫ</a></li>
            <li><a href="{{--route('logout') --}}">ВЫЙТИ</a></li>
            <li class="account-mob">
                <a href="#" class="ast-menu-toggle">АККАУНТ
                    <button class="ast-menu-btn"></button>
                </a>
                <ul class="sub-menu">
                    <li id="menu-item-1">
                        <a href=""><i class="fas fa-chevron-left"></i>Мой аккаунт</a>
                    </li>
                    <li id="menu-item-2">
                        <a href=""><i class="fas fa-chevron-left"></i>Корзина</a>
                    </li>
                </ul>
            </li> -->
            </ul>
        </nav>
    </div>