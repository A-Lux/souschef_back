<div id="slide-panel">
    <div class="slide-inner">
        <a href="#" class="btn " id="opener"> <img src="/image/modal-basket.JPG" alt=""></a>
        <span class="basket-count">{{ session()->has('dishes') ? count(session()->get('dishes')) : 0 }}</span>
        <div class="trigger">
            <div class="modal-basket">
                <div class="modal-title">
                    <h1>Ваша корзина</h1>
                </div>
                <div class="modal-basket-content">
                    @if(session()->has('dishes'))
                        @foreach(session()->get('dishes') as $dish_id)
                            @php
                                $dish = \App\Dish::where('id', $dish_id)->first();
                            @endphp
                           <div class="dish-counter">
                               <div class="dish-image">
                                   <img src="/image/menu2.jpeg" alt="">
                               </div>
                               <div class="dish-content-item">
                               <h1>{{ $dish->name }} </h1>
                               </div>
                           </div>
                            <br />
                        @endforeach
                    @else
                        <p>Ваша корзина пуста.</p>
                    @endif
                </div>
                <div class="modal-basket-footer">
                    <h2>Предварительный итог: <span>{{ session()->has('price') ? session()->get('price') :  0 }}  ₸</span></h2>
                    <p>Чтобы узнать стоимость доставки, перейдите к оформлению заказа.</p>
                    @php
                        $link = '/week/' . \App\Week::orderBy('created_at', 'desc')->first()->id;
                    @endphp
                    <a href="{{ $link }}">Продолжить покупки</a>
                </div>
            </div>
        </div>

    </div>
</div>