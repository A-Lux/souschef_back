@extends('layout')

@section('content')
    <div class="account">
        <div class="container">
            <div class="account-inner titleDiv">
                <div class="account-form">
                    <div class="account-link">
                        <div class="account-title">
                            <h3>Введите номер вашего телефона</h3>
                        </div>
                        <div class="account-password">
                            <input  id="phone1" type="text" placeholder="+7 (999) 999-99-99">
                            <button class="button-item receive-code">Получить код</button>
                        </div>
                        <div>
                        <div class="enter-code">
                            <h3>Введите код из смс</h3>
                            <div class="account-password">
                                <form method="post" action="/check">
                                    @csrf
                                    <input  type="text" placeholder="Введите код" class='code-input' name="code"Е>
                                    <button type="submit" class="button-item send-btn">Отправить</button>
                                </form>
                            </div>
                        </div>
                        <p>Нажимая «Получить код», вы даете согласие на получение sms и принимаете условия
                            <a href="">пользовательского соглашения</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
