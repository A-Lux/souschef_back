$(document).ready(function () {
  $('.adress-button').click(function () {
    var mapId = this.getAttribute('data-map');
    ymaps.ready(() => init(mapId));
  });


  // AOS.init();

  $('.burger-menu-btn').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('burger-menu-lines-active');
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
    $('body').toggleClass('body-overflow');
  });
 $(window).on('load',function(){
        $('#modal-info').modal('show');
    });

  // $('.send-btn').on('click', function(e){
  //   e.preventDefault();
  //   if($('.code-input').val().length == ""){
  //     $('.send-btn').attr('disabled', true);
  //     $(".send-btn").removeClass("button-item").addClass("disabled");
  //   }
  //   else{
  //     $('.send-btn').attr('disabled', false);
  //     $(".send-btn").removeClass("disabled").addClass("button-item");
  //   }
  //   return;
  // });

  // setTimeout(function() { 
  //   $('.send-btn').attr('disabled', true);
  // }, 1000);

  // $('.send-btn').on('click', function() {
  //   $(this).prop('disabled', true);
  //   $(".send-btn").removeClass("button-item").addClass("disabled");
  //   setTimeout(function() {
  //     $(this).prop('disabled', false);
  //     $(".send-btn").removeClass("disabled").addClass("button-item");
  //   }.bind(this), 30000);
  // });

  $('.receive-code').on('click', function () {
    $.ajax({
      type: 'GET',
      url: '/api/auth/phone',
      data: {
        'phone': $("#phone1").val()
      },
      success: function () {
        $('.enter-code').addClass('enter-code-active');
      },
      error: function () {
        console.log(2)
      }

    });
  });
  // window.onscroll=(function(){
  //     if(document.documentElement.scrollTop>=120){
  //         document.getElementById('choose-head').className='choose-head-active';      
  //     }
  //     else{
  //        document.getElementById('choose-head').className='choose-head';
  //     }
  // })

  $(function () {
    $("#phone1").mask("+  7(999) 999-9999");
  });


  // BURGER-MENU
  $('.js-toggle-menu').click(function (e) {
    e.preventDefault();
    $('.hamburger-menu').toggleClass('hamburger-menu-active');
    $('.mobile-header-nav').slideToggle();
    $(this).toggleClass('open');
  });
  $('.ast-menu-toggle').click(function (e) {
    e.preventDefault();
    $('.ast-menu-btn').toggleClass('ast-menu-btn-active');
    $('.sub-menu').toggleClass('sub-menu-active');
  })
  var $main_nav = $('#main-nav');
  var $toggle = $('.toggle');

  var defaultData = {
    maxWidth: false,
    customToggle: $toggle,
    navTitle: 'Ваша корзина',
    levelTitles: true,
    insertClose: 2,
    closeLevels: false,
    position: 'right',
    insertClose: false,
    labelBack: 'Назад'

  };
  $main_nav.hcOffcanvasNav(defaultData);


  //SLIDERS
  $('.menu').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  });


  $('.orders-slider').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });


  $('.orders-slider-bottom').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });


  $('.slider-nav').slick({
    slidesToShow: 0.999,
    slidesToScroll: 1,
    swipe: false,
    asNavFor: '.slide-content',
  });
  $('.slide-content').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    swipe: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.review-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.dishes-slider-mobile').slick({
    slidesToShow: 1,
    // slidesToScroll:1,
    dots: true
})
  $(document).ready(function () {
    $('.original-slider').slick({
      centerMode: true,
      centerPadding: '60px',
      slidesToShow: 1,
      useTransform: false,
      // swipe: false,
      responsive: [{
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            // slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: false,
            slidesToShow: 1,
            centerPadding: '0px',
          }
        }
      ]
    });
    
});
  $('.choose-prise').hover(
    function () {
      $('.choose-button').show();
    },
    function () {
      $('choose-button').hide();
    }
  );

  // let size = 218;
  // let text = $('.review-p');
  // text.each(function () {
  //   if ($(this).text().length > size) {
  //     $(this).text($(this).text().slice(0, size) + '...')
  //   }
  // });
  // if(document.getElementById('time-sending')) {
  //   document.getElementById('time-sending').innerText = new Date().getHours() + ':' + new Date().getMinutes();
  // }
  
});
$('#opener').on('click', function () {
  var panel = $('#slide-panel');

  if (panel.hasClass("visible")) {
    panel.removeClass('visible').animate({
      'margin-right': '-300px'
    });
    // $('body').css('opacity', '1')
  } else {
    panel.addClass('visible').animate({
      'margin-right': '0px'
    });
    // $('body').css('background', 'black');
    // $('body').css('opacity', '0.7')
  }
  return false;
});

$(document).ready(function () {
  $('.select-adress').select2({
    placeholder: "ВЫБЕРИТЕ АДРЕС",
    minimumResultsForSearch: Infinity,
    tags: "true",
  });
  $('.dishes-quantity').select2({
    minimumResultsForSearch: Infinity
  });

  $('.quantity-person').select2({
    minimumResultsForSearch: Infinity
  });

  $("#openmodal").on("change", function () {
    if ($(this).val() === 'openmodaloption') {
      $('#modal').modal('show');
    }
  });
  $('.delivery-select').select2({
    placeholder: "НЕТ ДОСТАВКИ НА УКАЗАННЫЙ АДРЕС",
    minimumResultsForSearch: Infinity
  });
  $('.time-select').select2({
    placeholder: "НЕТ ИНТЕРВАЛОВ ДОСТАВКИ",
    minimumResultsForSearch: Infinity
  });
  $('.show-dishes').on('click', function () {
    var spolier = $(this).parents('.text-overflow').find('.read-overflow').toggle('slow');
    $('.spoiler-arrow').toggleClass('spoiler-arrow-active');
    if (this.value == 'Посмотреть блюда') {
      this.value = 'Скрыть';
    } else {
      this.value = "Посмотреть блюда";
    };
    return false;
  });

  $('.spoiler-icons').on('click', function () {
    var spolier = $(this).parents('.text-overflow').find('.read-overflow').toggle('slow');
    $(this).children('.spoiler-arrow').toggleClass('spoiler-arrow-active');
    if (this.value == 'Посмотреть блюда') {
      this.value = 'Скрыть';
    } else {
      this.value = "Посмотреть блюда";
    };
    return false;
  });


  $('.close-panel').on('click', function () {
    $('.scissors').css('display', 'none');
  });
});
$('.help-icon').on('click', function () {
  $('.tooltip-card').toggleClass('tooltip-card-active');
});
$('.expand-btn').on('click', function () {
  $('.expand-wrapper').toggleClass('expand-wrapper-active');
});
$('.question-arrow').on('click', function () {
  $('.collapse').toggleClass('show');
  $('.question-arrow ').toggleClass('question-arrow-active');
});


$("#checked-dish").click(function () {
  if ($('#checked-dish').attr("checked") == 'checked') {
    console.log('mjndcjhbs')
  }
});


$("#checkbox").click(function () {
  if (document.getElementById('checkbox').checked) {
    console.log('mjndcjhbs')
    $('#issue-btn').removeAttr("disabled");
    $('#issue-btn').css('background-color', '#59b958')
  } else {
    $('#issue-btn').attr("disabled", true);
    $('#issue-btn').css('background-color', '#999')
    console.log('fdb')
  }
});

$('.question-btn').click(function () {
  $(this).children(".sub-btn").toggleClass('sub-btn-active');
  $(this).children('.arrow-right').toggleClass('arrow-right-active');
})


$('.question').click(function () {
  $(this).children('.question-arrow').toggleClass('question-arrow-active');
  $(this).children('.questions').children('.quest-button').toggleClass('sub-btn-active');
})





// $('.dishes-quantitys').change(function(e){
//   var option = (this).value;
//   var checkbox = document.querySelectorAll('#checked-dish[checked]');
//   // if(option == )
//   console.log(checkbox)
// })


$('.receive-code').on('click', function () {
  $.ajax({
    type: 'GET',
    url: '/api/auth/phone',
    data: {
      'phone': $("#phone1").val()
    },
    success: function () {
      $('.enter-code').toggleClass('enter-code-active');
    },
  });
});

function init(mapId) {
  var myPlacemark,
    myMap = new ymaps.Map(mapId, {
      center: [43.25622887, 76.92696959],
      zoom: 13
    }, {
      searchControlProvider: 'yandex#search'
    });

  var inputId = document.querySelector('#' + mapId).getAttribute('data-input');

  // Слушаем клик на карте.
  myMap.events.add('click', function (e) {
    var coords = e.get('coords');
    console.log(e._sourceEvent._sourceEvent.originalEvent.srcElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement)
    // Если метка уже создана – просто передвигаем ее.
    if (myPlacemark) {
      myPlacemark.geometry.setCoordinates(coords);
    }
    // Если нет – создаем.
    else {
      myPlacemark = createPlacemark(coords, inputId);
      myMap.geoObjects.add(myPlacemark);
      // Слушаем событие окончания перетаскивания на метке.
      myPlacemark.events.add('dragend', function () {
        getAddress(myPlacemark.geometry.getCoordinates());
      });
    }
    getAddress(coords, inputId);
  });

  // Создание метки.
  function createPlacemark(coords) {
    return new ymaps.Placemark(coords, {
      iconCaption: 'поиск...'
    }, {
      preset: 'islands#violetDotIconWithCaption',
      draggable: true
    });
  }

  // Определяем адрес по координатам (обратное геокодирование).
  function getAddress(coords, inputId) {
    myPlacemark.properties.set('iconCaption', 'поиск...');
    ymaps.geocode(coords).then(function (res) {
      var firstGeoObject = res.geoObjects.get(0);

      myPlacemark.properties
        .set({
          // Формируем строку с данными об объекте.
          iconCaption: [
            // Название населенного пункта или вышестоящее административно-территориальное образование.
            firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
            // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
            firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
          ].filter(Boolean).join(', '),
          // В качестве контента балуна задаем строку с адресом объекта.
          balloonContent: firstGeoObject.getAddressLine()

        });
      document.getElementById(inputId).value = firstGeoObject.getAddressLine();
      // var inputs = $('.ymaps-2-1-76-searchbox-input__input').value
    });
  }
}

function changemodal() {
  var e = document.getElementById("openmodal");
  var strUser = e.options[e.selectedIndex].value;
  document.getElementById('switcher').setAttribute('data-target', '#modal' + strUser);
  document.getElementById('switcher').setAttribute('data-map', 'map' + strUser);
  document.getElementById('switcher').style.display = 'block';
}

$('#openmodal').on('select2:select', changemodal);