var categoryPill = {
    template: `        
        <li class="nav-item" @click="sendEvent">
            <a :class="['nav-link', { active: active == category.id }]" href="#">
                {{ category.name }}
            </a>
        </li>`,
    props: ['category', 'active', 'index'],
    methods: {
        sendEvent(e) {
            this.$emit('test', { index: this.index });
        }
    }
};