var category = {
    template: `
        <div class="tab-pane fade show active">
            <div class="dishes-list">
                <div class="container container-tabs">
                        <div class="row">
                            <dish
                                :week="week"
                                @check="check"
                                @uncheck="uncheck"
                                :key="index"
                                v-for="(dish, index) in category.dishes"
                                :dish="dish"
                                :changeflag='changeflag'
                                :category="category.id">
                            </dish>
                        </div>
                    </div>
                </div>
            </div>
        </div>`,
    props: ['category', 'week', 'changeflag'],

    components: {
        'dish': dish
    },
    data() {
        return {

        }
    },
    computed: {
    },
    methods: {
        check(e) {
            this.$emit('dishChecked')
        },
        uncheck(e) {
            this.$emit('dishUnchecked')
        }
    },
    created() {

    }
};