var dish = {
    template: `
        <div class="col-xl-4 col-md-6">
            <div class="item-wrapper">
                <a :href="'/dishes/' + week + '/' + category + '/' + dish.id">
                    <div class="dishes-image">
                        <img :src="
                            dish.images ?
                                dish.images :
                                ''" alt="">
                        <div class="menu-hashtags">
                                <span
                                    :key="index"
                                    v-for="(index, tag) in dish.hashTags"
                                    >#{{ tag.name }}
                                </span>
                        </div>
                    </div>
                </a>
                <div class="about-dishes">
                    <div class="dishes-title">
                        <h1>{{ dish.name }}</h1>
                    </div>
                    <div class="dishes-date">
                        <span></span>
                    <label class="container-check">
                        <input :disabled="!changeflag" type="checkbox" @change="check" v-model="dish.checked" :name="'dishes['+ dish.id+']'" class='checked-dish' >
                        <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="dishes-icon">
                        <div class="dish-icon">
                            <p><img src="/image/cook.svg" alt="">{{ dish.weight }} г</p>
                        </div>
                        <div class="dish-icon">
                            <p><img src="/image/time.svg" alt="">{{ dish.cooking_time }} мин</p>
                        </div>
                        <div class="dish-icon">
                            <p><img src="/image/shef.svg" alt="">{{ dish.cooking_difficulty }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>`,
    props: ['dish', 'category', 'week', 'changeflag'],
    components: {
    },
    data() {
        return {

        }
    },
    computed: {
    },
    methods: {
        check(e) {
            if (e.target.checked) {
                this.$emit('check')
            }
            else {
                this.$emit('uncheck')
            }
        }
    }
}
