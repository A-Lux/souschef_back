var weeks = {
    template: `
        <div class="slider-nav">
            <div class="menu-slide">
                <h1>Меню на {{ weeks[current].name }}</h1>
                <div class="menu-inner-tabs">
                    <ul class="nav nav-pills mb-3 dishes-nav-pills">
                        <li class="nav-item">
                            <a :href="'/choose/'+weeks[current].id" class="choose-link">Что выбрать?</a>
                        </li>
                        <category-pill 
                            :key="index"
                            :active="currentCategory.id"
                            v-for="(category, index) in weeks[current].categories"
                            @test="setActiveCategory"
                            :index="index"
                            :category="category">
                        </category-pill>
                    </ul>                    
                    <div class="menu-tabs-bottom">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-5 col-md-5">
                                    <div class="tabs-left">
                                        <select
                                            @change="checkDishes"
                                            v-model="allowedDishesAmount"
                                            name="dishesAmount" 
                                            class="dishes-quantitys">
                                                <option value="3">
                                                    3 блюда
                                                </option>
                                                <option selected value="5">
                                                    5 блюд
                                                </option>
                                        </select>
                                        <select
                                            v-model="persons"
                                            @change="setPersons"
                                            name="personAmount" 
                                            class="dishes-quantitys">
                                                <option selected value="2">
                                                    2 персоны
                                                </option>
                                                <option value="4">
                                                    4 персоны
                                                </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-7 col-md-7">
                                    <div class="tabs-right">
                                        <div class="total-price">
                                            <span>
                                                {{ sum }} тенге
                                            </span>
                                        </div>
                                        <div class="expand-icon">
                                            <span class="expand-btn"> <img src="/image/expand.svg" alt=""></span>
                                            <div class="expand-wrapper">
                                                <p>Цена по подписке. Вы можете не оформлять подписку. В таком случае скидка по подписке
                                                    не применяется.</p>
                                                <div class="priceInfo">
                                                    <span>Семейное меню</span> <span>3</span> блюда</span>
                                                    <span>0 Т</span>
                                                </div>
                                                <div class="priceInfo-total">
                                                    <span>Итого</span>
                                                    <span>{{ sum * 0.9 }} тенге</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dish-button">
                                            <input type="hidden" v-model="currentCategory.id" name="category">
                                            <input type="hidden" v-model="weeks[current].id" name="week">
                                            <button @click="changeChoose" :class="pressedChange ? 'change-choose' : ''" type="button">
                                                
                                                    ИЗМЕНИТЬ СОСТАВ
                                                
                                            </button>
                                        </div>
                                        <div class="button-loader">
                                            <button 
                                                type="button"
                                                @click="sendOrder" 
                                                :class = "!allowOrder ? 'loader-btn' : '' "
                                                :disabled="!allowOrder">
                                                    ОФОРМИТЬ ЗАКАЗ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-tabs-mobile">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-6 col-12">
                                    <div class="tabs-right">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="expand-icon">
                                                    <div class="button-loader">
                                                    <button 
                                                        type="button"
                                                        @click="sendOrder" 
                                                        :class = "!allowOrder ? 'loader-btn' : '' "
                                                        :disabled="!allowOrder">
                                                            ОФОРМИТЬ ЗАКАЗ
                                                    </button>
                                                    </div>
                                                    <a class="btn" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                        <img src="/image/expand.svg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="dish-button">
                                                    <button @click="changeFlag = !changeFlag"> 
                                                        <span>
                                                            ИЗМЕНИТЬ СОСТАВ
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-12">
                                    <div class="tabs-left">
                                        <select
                                            @change="checkDishes"
                                            v-model="allowedDishesAmount"
                                            name="dishesAmount" 
                                            class="dishes-quantitys">
                                            <option value="2">
                                                2 блюда
                                            </option>
                                            <option value="3">
                                                    3 блюда
                                                </option>
                                                <option value="4">
                                                    4 блюда
                                                </option>
                                                <option selected value="5">
                                                    5 блюд
                                                </option>
                                                <option value="6">
                                                    6 блюд
                                                </option>
                                                <option value="7">
                                                    7 блюд
                                                </option>
                                        </select>
                                        <select
                                            v-model="persons"
                                            @change="setPersons"
                                            name="personAmount" 
                                            class="dishes-quantitys">
                                                <option value="1">
                                                    1 персона
                                                </option>
                                                <option selected value="2">
                                                    2 персоны
                                                </option>
                                                <option value="4">
                                                    4 персоны
                                                </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="collapse" id="collapseExample">
                                        <div class="card card-body">
                                            <div class="expand-wrapper">
                                                <p>Цена по подписке. Вы можете не оформлять подписку. В таком случае скидка по подписке
                                                    не применяется.</p>
                                                <div class="priceInfo">
                                                    <span>Семейное меню 3 блюда</span>
                                                    <span>4 390 ₽</span>
                                                </div>
                                                <div class="scissors">
                                                    <span>Ножницы</span>
                                                    <div class="delete-scissors">
                                                        <span> тенге</span>
                                                        <span class="close-panel"></span>
                                                    </div>
                                                </div>
                                                <div class="priceInfo-total">
                                                    <span>Итого</span>
                                                    <span>{{ sum * 0.9 }} тенге</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-content">
                    <div class="tab-content">
                        <category
                            :week="weeks[current].id"
                            :changeflag="changeFlag"
                            @dishChecked="check"
                            @dishUnchecked="uncheck"
                            :category="currentCategory">
                        </category>
                    </div>
                </div>
            </div>
        </div>`,
    props: ['weeks', 'current'],
    components: {
        'category-pill': categoryPill,
        'category': category
    },
    data() {
        return {
            activeCat: 0,
            checked: [],
            persons: 2,
            allowedDishesAmount: 5,
            changeFlag: false,
            pressedChange: false
        }
    },
    created() {
        if (this.getParameterByName('category')) {
            let e = {};
            e.index = this.weeks[this.current].categories.findIndex(el => {
                return el.id == this.getParameterByName('category')
            });
            this.setActiveCategory(e);
        }
        if (localStorage.getItem('persons')) {
            this.persons = localStorage.getItem('persons');
        }
        if (localStorage.getItem('dishes')) {
            this.allowedDishesAmount = localStorage.getItem('dishes');
        }
        if (!localStorage.getItem('state'))
            this.checkDishes();
    },
    watch: {
        current: function () {
            if (!localStorage.getItem('state'))
                this.checkDishes();
        }
    },
    methods: {
        getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        },
        changeChoose() {
            this.changeFlag = !this.changeFlag;
            this.pressedChange = !this.pressedChange;
        },
        sendOrder() {
            var formData = new FormData();
            formData.append('dishesAmount', this.checkedAmount);
            formData.append('persons', this.persons);
            formData.append('category', this.currentCategory.id);
            formData.append('week', this.weeks[this.current].id);
            // formData.append('_token', document.getElementsByName('_token').value);
            let dish_ids = this.currentCategory.dishes
                .filter(el => el.checked)
                .forEach(element => {
                    formData.append('dishes[]', element.id);
                });
            axios.post('/order', formData)
                .then(() => {
                    window.location.replace('/cart');
                })
                .catch(() => {
                    window.location.replace('/login');
                });
        },
        setActiveCategory(e) {
            this.activeCat = e.index;
            if (!localStorage.getItem('state'))
                this.checkDishes();
        },
        checkDishes(e) {
            for (let index = 0; index < this.currentCategory.dishes.length; index++) {
                this.currentCategory.dishes[index].checked = false;
            }
            for (let index = 0; index < this.allowedDishesAmount; index++) {
                if (this.currentCategory.dishes[index]) {
                    this.currentCategory.dishes[index].checked = true;
                }
            }
            localStorage.setItem('persons', this.persons);
            localStorage.setItem('dishes', this.allowedDishesAmount);
            this.$emit('save');
        },
        setPersons(e) {
            localStorage.setItem('persons', this.persons);
        },
        check(e) {
            this.$emit('save');
        },
        uncheck(e) {
            this.$emit('save');
        },
    },
    computed: {
        active() {
            // if(this.current && this.weeks){
            console.log(this.weeks[this.current].categories)
            return this.weeks[this.current].categories[this.activeCat].id;
            // }
            // return 0;
        },
        currentCategory() {
            return this.weeks[this.current].categories.filter(el => {
                return el.id == this.active
            })[0];
        },
        sum() {
            if (this.persons == 2 && this.allowedDishesAmount == 3)
                return this.currentCategory.price_two_three;
            if (this.persons == 2 && this.allowedDishesAmount == 5)
                return this.currentCategory.price_two_five;
            if (this.persons == 4 && this.allowedDishesAmount == 3)
                return this.currentCategory.price_four_three;
            if (this.persons == 4 && this.allowedDishesAmount == 5)
                return this.currentCategory.price_four_five;
            return 0;
            //return this.currentCategory.price ?
            //this.currentCategory.price * this.persons * this.allowedDishesAmount : 0;
            // dishes
            //     .filter(el => el.checked)
            //     .reduce(
            //         (a, b) => parseInt(a) + (parseInt(b['price']) || 0), 0) * this.persons;
        },
        checkedAmount() {
            return this.currentCategory.dishes
                .filter(el => el.checked)
                .length
        },
        allowOrder() {
            return this.checkedAmount == this.allowedDishesAmount
        }
    }
};