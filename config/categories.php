<?php

return [
    [
        'median_price' => 'Цена за порцию',
        'median_size' => 'Размер порции',
        'median_time' => 'Время приготовления',
        'median_days' => 'Количество дней',
    ],
    [
        'median_pork' => 'Без свинины',
        'median_meet' => 'Без жирного мяса',
        'median_fish' => 'Премиальная рыба',
        'median_vegetables' => 'Больше свежих овощей и фруктов',
        'median_rice' => 'Разнообразные крупы',
        'median_oil' => 'Без сливочного масла',
        'median_milk' => 'Без молочной продукции',
        'median_salt' => 'Без соли и сахара',
    ],
    [
        'median_choose' => 'Выбор блюд',
        'median_fry' => 'Без интенсивной жарки',
        'median_calory' => 'Пониженная калорийность'
    ]
];
